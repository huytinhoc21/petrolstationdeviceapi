package model

type HTTPError400 struct {
	Message string `json:"message" example:"Status bad request"`
}

type HTTPError500 struct {
	Message string `json:"message" example:"Internal server error"`
}
