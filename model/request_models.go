package model

import (
	"petrol-station/database"
	"petrol-station/types"
	"time"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

type ReqLogin struct {
	Username string `json:"username" validate:"required" example:"ndan.itus@gmail.com"`
	Password string `json:"password" validate:"required" example:"123"`
}

type ReqRegister struct {
	Name     string `json:"name" validate:"required" example:"Nguyễn Đăng An"`
	Username string `json:"username" validate:"required" example:"ndan.itus"`
	Email    string `json:"email" validate:"required"  example:"ndan.itus@gmail.com"`
	Password string `json:"password" validate:"required" example:"123"`
}

type ReqPatchUser struct {
	Name         string   `json:"name" example:"Nguyễn Đăng An"`
	Phone        string   `json:"phone" example:"0379823839"`
	Username     string   `json:"username" example:"0379823839"`
	Email        string   `json:"email" example:"ndan.itus@gmail.com"`
	Password     string   `json:"password" example:"123"`
	PermissionID []uint   `json:"permission_id" example:"1,2,3" `
	GasStationID []string `json:"gas_station_id" example:"1,2,3" `
}

// Permission model info
//
//	@Description	Permission information
type ReqPermission struct {
	database.Permission
} //@name Permission

type ReqPatchPermission struct {
	database.Permission
}

// ApiAction model info
//
//	@Description	ApiAction information
type ReqApiAction struct {
	database.ApiAction
} //@name ApiAction

type ReqPatchApiAction struct {
	database.ApiAction
}

// UserPermission model info
//
//	@Description	UserPermission information
type ReqUserPermission struct {
	database.UserPermission
} //@name UserPermission

type ReqPatchUserPermission struct {
	database.UserPermission
}

// ApiActionPermission model info
//
//	@Description	ApiActionPermission information
type ReqApiActionPermission struct {
	database.ApiActionPermission
} //@name ApiActionPermission

type ReqPatchApiActionPermission struct {
	database.ApiActionPermission
}

type ReqPatchApiActionPermissionByApiAction struct {
	ApiActionIDs []uint `json:"api_action_ids" validate:"required"`
}

// GasStation model info
//
//	@Description	GasStation information
type ReqGasStation struct {
	database.GasStation
} //@name GasStation

type ReqPatchGasStation struct {
	database.GasStation
}

// GasPump model info
//
//	@Description	GasPump information
type ReqGasPump struct {
	database.GasPump
} //@name GasPump

type ReqPatchGasPump struct {
	ID   uuid.UUID `json:"id" gorm:"type:uuid;default:uuid_generate_v4()" swaggerignore:"true"`
	Name string    `json:"name" gorm:"not null" example:"Vòi số 1"`

	GasTankID *uuid.UUID       `json:"gas_tank_id" gorm:"type:uuid"`
	GasTank   database.GasTank `json:"-" swaggerignore:"true"`

	DeletedAt  gorm.DeletedAt `json:"-" gorm:"index" swaggertype:"string"`
	UpdatedAt  time.Time      `json:"-"`
	StatusData types.JsonPsql `json:"status_data" gorm:"type:json" swaggertype:"object"`
}

// GasTank model info
//
//	@Description	GasTank information
type ReqGasTank struct {
	database.GasTank
} //@name GasTank

type ReqPatchGasTank struct {
	database.GasTank
}

// Good model info
//
//	@Description	Good information
type ReqGood struct {
	database.Good
} //@name Good

type ReqPatchGood struct {
	database.Good
}

// Provider model info
//
//	@Description	Provider information
type ReqProvider struct {
	database.Provider
} //@name Provider

type ReqPatchProvider struct {
	database.Provider
}

// Customer model info
//
//	@Description	Customer information
type ReqCustomer struct {
	database.Customer
} //@name Customer

type ReqPatchCustomer struct {
	database.Customer
}

// Staff model info
//
//	@Description	Staff information
type ReqStaff struct {
	database.Staff
} //@name Staff

type ReqPatchStaff struct {
	database.Staff
}

// Shift model info
//
//	@Description	Shift information
type ReqShift struct {
	database.Shift
} //@name Shift

type ReqPatchShift struct {
	database.Shift
}

// StaffShift model info
//
//	@Description	StaffShift information
type ReqStaffShift struct {
	database.StaffShift
} //@name StaffShift

type ReqPatchStaffShift struct {
	ID uint `json:"id" gorm:"primaryKey" swaggerignore:"true"`

	StaffID uuid.UUID `json:"staff_id" example:"d4602cc9-1b48-48ea-9d94-35c39c992704" gorm:"type:uuid"`

	ShiftID uint `json:"shift_id" example:"1"`

	StartingVolume float64 `json:"starting_volume"`

	Date time.Time `json:"date" gorm:"not null;type:date" example:"2023-08-17T00:00:00+07:00"`

	// GasPumpID uuid.UUID `json:"gas_pump_id" gorm:"type:uuid" example:"87f2b563-0f58-457c-9bd3-5e260ec6e457"`
}

type ReqCloseShift struct {
	ID uint `json:"id" gorm:"primaryKey" swaggerignore:"true"`

	StartingVolume float64   `json:"starting_volume" swaggerignore:"true"`
	EndingVolume   float64   `json:"ending_volume"`
	GasPumpID      uuid.UUID `json:"gas_pump_id" gorm:"type:uuid" example:"87f2b563-0f58-457c-9bd3-5e260ec6e457"`

	ActualStartTime time.Time `json:"actual_start_time" example:"2023-10-17T04:09:41.613Z"`
	ActualEndTime   time.Time `json:"actual_end_time" example:"2023-10-17T04:12:41.613Z"`

	FuelVolume   float64 `json:"fuel_volume" gorm:"not null" example:"1.9"`
	UnitPrice    float64 `json:"unit_price" gorm:"not null" example:"2050009"`
	TotalRevenue float64 `json:"total_revenue" example:"345009"`

	PendingAmount   float64 `json:"pending_amount" example:"34500999"`
	CompletedAmount float64 `json:"completed_amount" example:"34500999"`

	Date      time.Time      `json:"date" swaggerignore:"true"`
	RecordID  *string        `json:"record_id"`
	DeletedAt gorm.DeletedAt `json:"-" gorm:"index" swaggerignore:"true"`
}

// Transaction model info
//
//	@Description	Transaction information
type ReqTransaction struct {
	database.Transaction
	GasType string `json:"gas_type" gorm:"-"`
} //@name Transaction

type ReqPatchTransaction struct {
	database.Transaction
}

type ReqCreateTransactionInvoice struct {
	UseCurrentDate bool `json:"use_current_date"`
}

// StaffShiftInfo model info
//
//	@Description	StaffShiftInfo information
type ReqStaffShiftInfo struct {
	database.StaffShiftInfo
} //@name StaffShiftInfo

type ReqPatchStaffShiftInfo struct {
	database.StaffShiftInfo
}

type ReqDeleteMany struct {
	ID string `json:"id"`
}

// Deposit model info
//
//	@Description	Deposit information
type ReqDeposit struct {
	ID        uint      `json:"id" gorm:"not null;primaryKey" swaggerignore:"true"`
	CreatedAt time.Time `json:"created_at"`

	CustomerID *uint `json:"customer_id"`

	TransferContent string `json:"transfer_content"`
	Amount          int64  `json:"amount" gorm:"not null"`

	CashierID *uuid.UUID `json:"cashier_id" gorm:"type:uuid"`
} //@name Deposit

type ReqPatchDeposit struct {
	database.Deposit
}

// FuelImport model info
//
//	@Description	FuelImport information
type ReqFuelImport struct {
	ID        uuid.UUID  `json:"id" gorm:"type:uuid;default:uuid_generate_v4()" swaggerignore:"true"`
	Amount    float32    `json:"amount" example:"1.5"`
	GasTankID *uuid.UUID `json:"gas_tank_id"`
	CreatedAt time.Time  `json:"created_at" swaggerignore:"true"`
} //@name FuelImport

type ReqPatchFuelImport struct {
	ID        uuid.UUID  `json:"id" gorm:"type:uuid;default:uuid_generate_v4()" swaggerignore:"true"`
	Amount    float32    `json:"amount" example:"1.5"`
	GasTankID *uuid.UUID `json:"gas_tank_id"`
}

// UserGasStation model info
//
//	@Description	UserGasStation information
type ReqUserGasStation struct {
	database.UserGasStation
} //@name UserGasStation

type ReqPatchUserGasStation struct {
	database.UserGasStation
}

type ManualUpdateStaffShift struct {
	StartingVolume float64 `json:"starting_volume"`
	EndingVolume   float64 `json:"ending_volume"`
	FuelVolume     float64 `json:"fuel_volume" gorm:"not null" example:"1.9"  swaggerignore:"true"`
	UnitPrice      float64 `json:"unit_price" gorm:"not null" example:"2050009"`
	TotalRevenue   float64 `json:"total_revenue" example:"345009"`
}

type ManualAddStaffShift struct {
	StartingVolume  float64    `json:"starting_volume"`
	EndingVolume    float64    `json:"ending_volume"`
	ActualStartTime *time.Time `json:"actual_start_time" example:"2023-10-17T04:09:41.613Z"`
	ActualEndTime   *time.Time `json:"actual_end_time" example:"2023-10-17T04:12:41.613Z"`
	FuelVolume      float64    `json:"fuel_volume" gorm:"not null" example:"1.9"`
	UnitPrice       float64    `json:"unit_price" gorm:"not null" example:"2050009"`
	TotalRevenue    float64    `json:"total_revenue" example:"345009"`
}

// InvoiceSetting model info
//
//	@Description	InvoiceSetting information
type ReqInvoiceSetting struct {
	database.InvoiceSetting
} //@name InvoiceSetting

type ReqPatchInvoiceSetting struct {
	database.InvoiceSetting
}

type InvoiceGood struct {
	Name      string  `json:"name"`
	Unit      string  `json:"unit"`
	Amount    float64 `json:"amount"`
	UnitPrice float64 `json:"unit_price"`
	Revenue   float64 `json:"revenue"`
	TaxRate   float64 `json:"tax_rate"`
	TaxRateID int     `json:"tax_rate_id"`
	TaxAmount float64 `json:"tax_amount"`
}

// Invoice model info
//
//	@Description	Invoice information
type ReqInvoice struct {
	ID            uint      `json:"id" gorm:"primaryKey" swaggerignore:"true"`
	GasStationID  uuid.UUID `json:"gas_station_id" example:"1"`
	CustomerName  string    `json:"customer_name"`
	Unit          string    `json:"unit"`
	Email         string    `json:"email"`
	Address       string    `json:"address"`
	TaxCode       string    `json:"tax_code"`
	VehicleID     string    `json:"vehicle_id"`
	PaymentMethod int       `json:"payment_method"`
	GoodsRevenue  float64   `json:"goods_revenue"` // sum(goods.revenue)
	VatRevenue    float64   `json:"vat_revenue"`   // goods_revenue * vat
	TotalRevenue  float64   `json:"total_revenue"` // goods_revenue + vat_revenue

	TransactionID *uuid.UUID `json:"transaction_id" gorm:"type:uuid;unique"`
	CreatedAt     time.Time  `json:"created_at" swaggerignore:"true"`

	Draft bool          `json:"draft"`
	Date  time.Time     `json:"date"`
	Goods []InvoiceGood `json:"goods" gorm:"-"`
	Type  string        `json:"type"`
} //@name Invoice

type ReqPatchInvoice struct {
	database.Invoice
}
