package model

import (
	"petrol-station/database"
	"petrol-station/types"
	"time"

	"github.com/gofrs/uuid"
	"github.com/lib/pq"
	"gorm.io/gorm"
)

type UserDataRes struct {
	ID           string         `json:"id" example:"123IHJu212ioJIUN" `
	Name         string         `json:"name" example:"Nguyễn Đăng An"`
	Email        string         `json:"email" example:"ndan.itus@gmail.com"`
	Username     string         `json:"username" example:"ndan.itus"`
	Phone        string         `json:"phone" example:"0382939392" `
	Role         string         `json:"role" example:"admin"`
	Password     string         `json:"password" example:"ewadgdytuyvy43"`
	PermissionID pq.StringArray `json:"permission_id" example:"admin" gorm:"type:text[]" swaggertype:"array,string"`
	GasStationID pq.StringArray `json:"gas_station_id" example:"admin" gorm:"type:text[]" swaggertype:"array,string"`
}
type ResLogin struct {
	Code   int         `json:"code" example:"200"`
	Token  string      `json:"token" example:"iuniu32neui3rn38fh784e5yn78f5r57R&FGU*^TU?;.'grteuiHIUN98"`
	Expire string      `json:"expire" example:"2005-08-15T15:52:01+00:00"`
	Data   UserDataRes `json:"data"`
}

type ResRegister struct {
	Id string `json:"id" example:"123IHJu212ioJIUN"`
}

type ResGetUserInfo struct {
	ID   string `json:"id" example:"123IHJu212ioJIUN" `
	Name string `json:"name" example:"Nguyễn Đăng An"`
}

type ResApiAction struct {
	database.ApiAction
}

type ResUserPermission struct {
	database.UserPermission
}

type ResApiActionPermission struct {
	database.ApiActionPermission
	ApiActionName string `json:"api_action_name"`
}

type ResPermission struct {
	database.Permission
	ID uint `json:"id"`
}

type ResGasStation struct {
	database.GasStation
	ID uuid.UUID `json:"id" gorm:"type:uuid;default:uuid_generate_v4()"`
}

type ResGasPump struct {
	database.GasPump
	ID uuid.UUID `json:"id" gorm:"type:uuid"`
}

type ResGasTank struct {
	ID           uuid.UUID      `json:"id" gorm:"type:uuid"`
	Name         string         `json:"name" `
	Capacity     uint           `json:"capacity" `
	StatusData   types.JsonPsql `json:"status_data" gorm:"type:json" swaggertype:"object"`
	GasPumps     types.JsonPsql `json:"gas_pumps" gorm:"type:json" swaggertype:"object"`
	Good         types.JsonPsql `json:"good" gorm:"type:json" swaggertype:"object"`
	GasStationID *uuid.UUID     `json:"gas_station_id" gorm:"type:uuid"`

	DeletedAt gorm.DeletedAt `json:"-" gorm:"index"`
}

type ResGood struct {
	database.Good
	ID uint `json:"id" gorm:"primaryKey"`
}

type ResProvider struct {
	database.Provider
	ID uuid.UUID `json:"id"`
}

type ResCustomer struct {
	database.Customer
	ID uint `json:"id" gorm:"primaryKey"`
}

type ResStaff struct {
	database.Staff
	ID uuid.UUID `json:"id"`
}

type ResShift struct {
	database.Shift
	ID string `json:"id" gorm:"primaryKey"`
}

type StaffShiftStatisticData struct {
	Staff      StaffData       `json:"staff"`
	StaffShift []ResStaffShift `json:"staff_shift"`
}
type ResStaffShiftStatistic struct {
	Shift ShiftInfo                 `json:"shift"`
	Date  string                    `json:"date"`
	Data  []StaffShiftStatisticData `json:"data"`
}

type ResStaffShift struct {
	ID      uint      `json:"id"`
	Staff   StaffData `json:"staff" gorm:"type:json"`
	Shift   ShiftInfo `json:"shift" gorm:"type:json"`
	GasPump NameAndID `json:"gas_pump" gorm:"type:json"`
	Good    string    `json:"good"`
	Date    time.Time `json:"date" gorm:"type:date" example:"2023-08-17T00:00:00+07:00"`

	StartingVolume float64 `json:"starting_volume"`
	EndingVolume   float64 `json:"ending_volume"`

	ActualStartTime time.Time `json:"actual_start_time" example:"08:00"`
	ActualEndTime   time.Time `json:"actual_end_time" example:"09:00"`

	UnitPrice    float32 `json:"unit_price" example:"23000"`
	FuelVolume   float32 `json:"fuel_volume" example:"1.5"`
	TotalRevenue float64 `json:"total_revenue" example:"34500"`
	RecordID     string  `json:"record_id"`
}

type ResGasStationInformation struct {
	GasPumps []GasPumpStatus `json:"gas_pumps"`
	// GasPumps types.JsonPsql `json:"old_version" gorm:"type:json"`
	// ID uint `json:"id" gorm:"primaryKey" swaggerignore:"true"`

	// StaffID string `json:"staff_id"`
	// Staff   Staff  `json:"-" swaggerignore:"true"`

	// ShiftID string `json:"shift_id"`
	// Shift   Shift  `json:"-" swaggerignore:"true"`

	// Time time.Time `json:"time" gorm:"not null"`

	// GasPumpID uuid.UUID `gorm:"type:uuid"`
	// GasPump   GasPump   `json:"-" swaggerignore:"true"`
}

type ResTransaction struct {
	ID             uuid.UUID       `json:"id" gorm:"type:uuid;default:uuid_generate_v4()"`
	CreatedAt      time.Time       `json:"created_at"`
	GasPump        NameAndID       `json:"gas_pump" gorm:"type:json"`
	Good           NameAndIDUint   `json:"good" gorm:"type:json"`
	Customer       CustomerDataRes `json:"customer" gorm:"type:json"`
	Staff          StaffInfo       `json:"staff" gorm:"type:json"`
	PaymentDate    *time.Time      `json:"payment_date"`
	FuelVolume     float32         `json:"fuel_volume" example:"1.5"`
	UnitPrice      float32         `json:"unit_price" example:"23000"`
	TotalRevenue   float64         `json:"total_revenue" example:"34500"`
	PaymentType    string          `json:"payment_type"`
	Status         string          `json:"status"`
	GasStationName string          `json:"gas_station_name" example:"Tên cửa hàng xăng số 1"`
	GasStationID   string          `json:"gas_station_id" example:"5d0f9989-b887-4ccd-8dfd-55773426bb2b"`
	RecordID       string          `json:"record_id" gorm:"uniqueIndex:gas_pump_id_and_record_id_unique"`
	DeletedAt      gorm.DeletedAt  `json:"deleted_at" gorm:"index" swaggerignore:"true"`

	Invoice types.JsonPsql `json:"invoice" gorm:"type:json" swaggertype:"object"`
}

type ResTransactionByPump struct {
	GasPump         NameAndID      `json:"gas_pump" gorm:"type:json"`
	Good            NameAndIDUint  `json:"good" gorm:"type:json"`
	TotalRevenue    float64        `json:"total_revenue" example:"34500"`
	FuelVolume      float32        `json:"fuel_volume" example:"1.5"`
	PendingAmount   float64        `json:"pending_amount" example:"34500"`
	Cash            float64        `json:"cash" example:"34500"`
	CompletedAmount float64        `json:"completed_amount" example:"34500"`
	RecordID        string         `json:"record_id" gorm:"uniqueIndex:gas_pump_id_and_record_id_unique"`
	DeletedAt       gorm.DeletedAt `json:"-" gorm:"index" swaggerignore:"true"`
}

type ResTransactionByGood struct {
	Good            NameAndIDUint  `json:"good" gorm:"type:json"`
	TotalRevenue    float64        `json:"total_revenue" example:"34500"`
	FuelVolume      float32        `json:"fuel_volume" example:"1.5"`
	PendingAmount   float64        `json:"pending_amount" example:"34500"`
	Cash            float64        `json:"cash" example:"34500"`
	CompletedAmount float64        `json:"completed_amount" example:"34500"`
	RecordID        string         `json:"record_id" gorm:"uniqueIndex:gas_pump_id_and_record_id_unique"`
	DeletedAt       gorm.DeletedAt `json:"-" gorm:"index" swaggerignore:"true"`
}

type ResStaffShiftRevenue struct {
	Shift ShiftInfo
	Data  []struct {
		Staff NameAndID
		Data  []struct {
			GasPump NameAndID
		}
	}
}

type ResStaffShiftInfo struct {
	database.StaffShiftInfo
}

type ResDeposit struct {
	database.Deposit
}

type ResFuelImport struct {
	database.FuelImport
	Good GoodInfo `json:"good" gorm:"type:json"`
}

type ResUserGasStation struct {
	database.UserGasStation
}

type ResInvoiceSetting struct {
	database.InvoiceSetting
}

type ResInvoice struct {
	database.Invoice
}
