package model

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/gofrs/uuid"
)

// GasPumpStatus model info
//
//	@Description	GasPumpStatus information
type GasPumpStatus struct {
	Status     string  `json:"status" example:"Đang bơm"`
	UnitPrice  float32 `json:"unit_price" example:"23000"`
	FuelVolume float32 `json:"fuel_volume" example:"1.5"`
	TotalCost  float32 `json:"total_cost" example:"34500"`
} //@name GasPumpStatus

// GasTankStatus model info
//
//	@Description	GasTankStatus information
type GasTankStatus struct {
	MeasuringStick uint      `json:"measuring_stick" example:"1"`
	Status         string    `json:"status" example:"TANK có mức nhiên liệu thấp hơn Low Alarm"`
	LastConnection time.Time `json:"last_connection" example:"2023-08-17T15:40:58.131023+07:00"`
	Temperature    float32   `json:"temperature" example:"25.4"`
	Remaining      float32   `json:"remaining" example:"50"`
	Fuel           struct {
		H float32 `json:"h"  example:"1720.39"`
		V float32 `json:"v"  example:"7459"`
	} `json:"fuel"`
	Water struct {
		H float32 `json:"h"  example:"1720.39"`
		V float32 `json:"v"  example:"7459"`
	} `json:"water"`
} //@name GasTankStatus

type NameAndID struct {
	Name string `json:"name"`
	ID   string `json:"id"`
}

type StaffData struct {
	Name string    `json:"name"`
	ID   uuid.UUID `json:"id" gorm:"type:uuid"`
	Code string    `json:"code"`
}

type CustomerDataRes struct {
	Name string `json:"name"`
	ID   uint   `json:"id"`
	Type string `json:"type"`
	Code string `json:"code"`
}

func (nameAndID CustomerDataRes) Value() (driver.Value, error) {
	return json.Marshal(nameAndID)
}

func (nameAndID *CustomerDataRes) Scan(value interface{}) error {
	bytes, ok := value.([]byte)
	if !ok {
		return errors.New(fmt.Sprint("Failed to unmarshal JSONB value:", value))
	}
	return json.Unmarshal(bytes, &nameAndID)
}

func (nameAndID StaffData) Value() (driver.Value, error) {
	return json.Marshal(nameAndID)
}

func (nameAndID *StaffData) Scan(value interface{}) error {
	bytes, ok := value.([]byte)
	if !ok {
		return errors.New(fmt.Sprint("Failed to unmarshal JSONB value:", value))
	}
	return json.Unmarshal(bytes, &nameAndID)
}

type StaffInfo struct {
	Name string `json:"name"`
	ID   string `json:"id"`
	Type string `json:"type"`
}

func (v StaffInfo) Value() (driver.Value, error) {
	return json.Marshal(v)
}

func (v *StaffInfo) Scan(value interface{}) error {
	bytes, ok := value.([]byte)
	if !ok {
		return errors.New(fmt.Sprint("Failed to unmarshal JSONB value:", value))
	}
	return json.Unmarshal(bytes, &v)
}

func (nameAndID NameAndID) Value() (driver.Value, error) {
	return json.Marshal(nameAndID)
}

func (nameAndID *NameAndID) Scan(value interface{}) error {
	bytes, ok := value.([]byte)
	if !ok {
		return errors.New(fmt.Sprint("Failed to unmarshal JSONB value:", value))
	}
	return json.Unmarshal(bytes, &nameAndID)
}

type NameAndIDUint struct {
	Name string `json:"name"`
	ID   uint   `json:"id"`
}

func (nameAndID NameAndIDUint) Value() (driver.Value, error) {
	return json.Marshal(nameAndID)
}

func (nameAndID *NameAndIDUint) Scan(value interface{}) error {
	bytes, ok := value.([]byte)
	if !ok {
		return errors.New(fmt.Sprint("Failed to unmarshal JSONB value:", value))
	}
	return json.Unmarshal(bytes, &nameAndID)
}

type ShiftInfo struct {
	ID        uint   `json:"id"`
	Name      string `json:"name" example:"Ca 1"`
	StartTime string `json:"start_time" example:"08:00"`
	EndTime   string `json:"end_time" example:"09:00"`
}

func (shiftInfo ShiftInfo) Value() (driver.Value, error) {
	return json.Marshal(shiftInfo)
}

func (shiftInfo *ShiftInfo) Scan(value interface{}) error {
	bytes, ok := value.([]byte)
	if !ok {
		return errors.New(fmt.Sprint("Failed to unmarshal JSONB value:", value))
	}
	return json.Unmarshal(bytes, &shiftInfo)
}

type StaffShiftCash struct {
	Num500 uint   `json:"num_500"`
	Num200 string `json:"num_200" example:"1"`
	Num100 string `json:"num_100" example:"1"`
	Num50  string `json:"num_50" example:"3"`
	Num20  string `json:"num_20" example:"1"`
	Num10  string `json:"num_10" example:"0"`
	Num5   string `json:"num_5" example:"1"`
	Num2   string `json:"num_2" example:"1"`
	Num1   string `json:"num_1" example:"1"`
}

type GoodInfo struct {
	ID   uint   `json:"id"`
	Name string `json:"name" example:"Xăng A95"`
	Unit string `json:"unit" example:"Lít"`
}

func (shiftInfo GoodInfo) Value() (driver.Value, error) {
	return json.Marshal(shiftInfo)
}

func (shiftInfo *GoodInfo) Scan(value interface{}) error {
	bytes, ok := value.([]byte)
	if !ok {
		return errors.New(fmt.Sprint("Failed to unmarshal JSONB value:", value))
	}
	return json.Unmarshal(bytes, &shiftInfo)
}
