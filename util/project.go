package util

import (
	"fmt"
	"math"
	"petrol-station/database"
	"petrol-station/model"
	"reflect"
	"time"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

type TransactionStatisticsData struct {
	UnitPrice    float32 `json:"unit_price" example:"23000"`
	FuelVolume   float32 `json:"fuel_volume" example:"1.5"`
	TotalRevenue float64 `json:"total_revenue" example:"34500"`
}

func AnalyzeTransactionData(from, to, staffID string) *TransactionStatisticsData {
	var res TransactionStatisticsData
	db := database.DB.Table("transactions").Where("staff_id = ?", staffID)
	if from != "" && IsPostgreSQLDateFormat(from) {
		db = db.Where("created_at >= ?", from)
	}

	if to != "" && IsPostgreSQLDateFormat(to) {
		db = db.Where("created_at <= ?", to)
	}

	db.Select("sum(fuel_volume) as fuel_volume, sum(fuel_volume*unit_price) as total_revenue, avg(unit_price) as unit_price").Scan(&res)
	return &res
}

func MapToStruct(inputMap map[string]interface{}, outputStruct interface{}) error {
	structValue := reflect.ValueOf(outputStruct).Elem()

	for k, v := range inputMap {
		structFieldValue := structValue.FieldByName(k)
		if !structFieldValue.IsValid() {
			return fmt.Errorf("No such field: %s in struct", k)
		}

		if !structFieldValue.CanSet() {
			return fmt.Errorf("Cannot set %s field value", k)
		}

		value := reflect.ValueOf(v)
		if structFieldValue.Type() != value.Type() {
			return fmt.Errorf("Provided value type didn't match field type")
		}

		structFieldValue.Set(value)
	}

	return nil
}

func CheatStaffShift(shift *database.StaffShift, db *gorm.DB) (numAdd, totalLess, totalMore, numUpdate, totalLessEqualVol, totalMoreEqualVol, numUpdateLessEqualVol, numUpdateMoreEqualVol int64) {
	a := time.Time{}
	if shift.ActualStartTime.Equal(a) {
		return
	}
	if shift.ActualEndTime.Sub(shift.ActualStartTime) > 10*24*time.Hour {
		return
	}

	// if shift.ID == 538351 {
	// 	fmt.Println(shift)
	// 	continue
	// }
	// if shift.ActualEndTime.Sub(shift.ActualStartTime) > 10*24*time.Hour {
	// 	fmt.Println("Shitttt2222222")
	// 	continue
	// }

	var sumRev struct {
		SumRev float64 `json:"sum_rev"`
		SumVol float64 `json:"sum_vol"`
	}

	db.Table("transactions").Select("sum(fuel_volume) as sum_vol, sum(total_revenue) as sum_rev").
		Where("created_at >= ? and created_at <= ? and gas_pump_id = ? and deleted_at is null", shift.ActualStartTime, shift.ActualEndTime, shift.GasPumpID).Scan(&sumRev)

	if shift.FuelVolume == 0 {
		invoices := []database.Invoice{}
		db.Table("invoices i").
			Select("i.*").
			Joins("join transactions t on i.transaction_id = t.id").
			Where(`t.customer_id is null
		and t.created_at >= ? and t.created_at <= ? and t.gas_pump_id = ?`,
				shift.ActualStartTime, shift.ActualEndTime, shift.GasPumpID).Find(&invoices)
		for _, invoice := range invoices {
			command := []database.CommandObject301{{
				PartnerInvoiceStringID: fmt.Sprintf("%d", invoice.ID),
			}}
			type InputGood struct {
				GoodName     string    `json:"good_name"`
				Vat          int       `json:"vat"`
				GasStationID uuid.UUID `json:"gas_station_id" gorm:"type:uuid;unique"`
			}
			var good InputGood
			db.Table("transactions").
				Select(`goods.name as good_name, goods.vat as vat, gas_tanks.gas_station_id`).
				Joins("left join gas_pumps on transactions.gas_pump_id = gas_pumps.id").
				Joins("left join gas_tanks on gas_pumps.gas_tank_id = gas_tanks.id").
				Joins("left join goods on gas_tanks.good_id = goods.id").
				Where("transactions.id = ?", invoice.TransactionID).
				Find(&good)
			fmt.Println("Cheating then DeleteBkavInvoice")
			_, err := BKAV_Request(301, command, good.GasStationID)
			if err != nil {
				db.Table("invoices").Where("id = ?", invoice.ID).Update("invoice_msg", err.Error())
			}
		}
		db.Table("transactions").Where(`customer_id is null 
		and created_at >= ? and created_at <= ? and gas_pump_id = ?`,
			shift.ActualStartTime, shift.ActualEndTime, shift.GasPumpID).Update("deleted_at", time.Now())
	}
	if shift.FuelVolume-sumRev.SumVol > 0 {

		transactions := []database.Transaction{}
		db.Table("transactions").
			Where(`
			customer_id is null and
			created_at >= ? and created_at <= ? and 
			gas_pump_id = ? and deleted_at is null and 
			record_id not like 'cheat%' and
			not exists (
				select 1 from invoices 
				where transaction_id = transactions.id
			)`,
				shift.ActualStartTime, shift.ActualEndTime, shift.GasPumpID).Limit(100). // should limit because of bkav request rate limit
			Find(&transactions)
		for _, transaction := range transactions {
			delta := math.Min(shift.FuelVolume-sumRev.SumVol, 0.004)
			transaction.FuelVolume += delta
			transaction.RecordID = "cheat-c1-update-vol-" + transaction.RecordID
			db.Table("transactions").Where("id = ?", transaction.ID).Updates(&transaction)
		}

		totalLess += 1
		fuelVolume := shift.FuelVolume - sumRev.SumVol
		createdAt := shift.ActualEndTime.Add(-15 * time.Minute)
		tmp := database.Transaction{}
		db.Where("created_at = ? and gas_pump_id = ?", createdAt, shift.GasPumpID).First(&tmp)
		if tmp.ID != uuid.Nil {
			createdAt = createdAt.Add(-1 * time.Minute)
		}
		db.Unscoped().Where("record_id = ? and deleted_at is not null", "cheat-"+createdAt.Format("2006-01-02 15:04:05")).Delete(&database.Transaction{})
		fmt.Println("Nhỏ hơn nè:", "Tổng volume:", sumRev.SumVol, "Voume kết ca:", shift.FuelVolume, "Chênh lệch:", shift.FuelVolume-sumRev.SumVol)
		fmt.Println("Nhỏ hơn nè:", "Tổng revunue:", sumRev.SumRev, "Revenur kết ca:", shift.TotalRevenue, "Chênh lệch:", shift.TotalRevenue-sumRev.SumRev)
		fmt.Println("Staff_Shift ID", shift.ID)

		tx := model.ReqTransaction{
			Transaction: database.Transaction{
				GasPumpID:    shift.GasPumpID,
				FuelVolume:   fuelVolume,
				UnitPrice:    shift.UnitPrice,
				TotalRevenue: shift.TotalRevenue - sumRev.SumRev,
				CreatedAt:    createdAt,
				RecordID:     "cheat-c1-create" + createdAt.Format("2006-01-02 15:04:05"),
			},
		}
		result := db.Table("transactions").Create(&tx)
		fmt.Println(tx.ID)
		fmt.Println(tx.FuelVolume, tx.TotalRevenue)
		numAdd += result.RowsAffected
	} else if shift.FuelVolume-sumRev.SumVol < 0 {
		totalMore += 1

		fmt.Println("Lớn hơn nè:", "Tổng volume:", sumRev.SumVol, "Voume kết ca:", shift.FuelVolume, "Chênh lệch:", sumRev.SumVol-shift.FuelVolume)
		fmt.Println("Lớn hơn nè:", "Doanh thu tổng:", sumRev.SumRev, "Doanh thu kết ca:", shift.TotalRevenue, "Chênh lệch:", sumRev.SumRev-shift.TotalRevenue)
		cheatTx := database.Transaction{}

		for {
			cheatTx = database.Transaction{}
			db.Debug().First(&cheatTx, `not exists (
				select 1 from invoices 
				where transaction_id = transactions.id
			) and customer_id is null
			and total_revenue % 1000 <> 0 
			and created_at >= ? and created_at <= ? and gas_pump_id = ? 
			  and deleted_at is null and fuel_volume > ? and total_revenue > ?`,
				shift.ActualStartTime, shift.ActualEndTime, shift.GasPumpID, sumRev.SumVol-shift.FuelVolume, sumRev.SumRev-shift.TotalRevenue)
			if cheatTx.ID != uuid.Nil {
				cheatTx.FuelVolume -= (sumRev.SumVol - shift.FuelVolume)
				cheatTx.TotalRevenue -= (sumRev.SumRev - shift.TotalRevenue)
				cheatTx.RecordID = "cheat-c2-update-" + cheatTx.RecordID
				result := db.Save(&cheatTx)
				fmt.Println("Cheating then CreateOrUpdateBkavInvoice")
				numUpdate += result.RowsAffected
				break
			} else {
				cheatTx = database.Transaction{}
				db.Debug().First(&cheatTx, `created_at >= ? and created_at <= ? and gas_pump_id = ? and deleted_at is null`,
					shift.ActualStartTime, shift.ActualEndTime, shift.GasPumpID).
					Order("total_revenue DESC, fuel_volume DESC")
				if cheatTx.ID == uuid.Nil {
					break
				}
				invoice := database.Invoice{}
				db.First(&invoice, "transaction_id = ?", cheatTx.ID)
				if invoice.InvoiceGUID != "" {
					command := []database.CommandObject301{{
						PartnerInvoiceStringID: fmt.Sprintf("%d", invoice.ID),
					}}
					type InputGood struct {
						GoodName     string    `json:"good_name"`
						Vat          int       `json:"vat"`
						GasStationID uuid.UUID `json:"gas_station_id" gorm:"type:uuid;unique"`
					}
					var good InputGood
					db.Table("transactions").
						Select(`goods.name as good_name, goods.vat as vat, gas_tanks.gas_station_id`).
						Joins("left join gas_pumps on transactions.gas_pump_id = gas_pumps.id").
						Joins("left join gas_tanks on gas_pumps.gas_tank_id = gas_tanks.id").
						Joins("left join goods on gas_tanks.good_id = goods.id").
						Where("transactions.id = ?", invoice.TransactionID).
						Find(&good)
					fmt.Println("Cheating then DeleteBkavInvoice")
					BKAV_Request(301, command, good.GasStationID)
				}
				db.Debug().Where("id = ?", cheatTx.ID).Update("record_id", "cheat-c2-deleted-"+cheatTx.RecordID)
				db.Debug().Where("id = ?", cheatTx.ID).Delete(&database.Transaction{})

				fmt.Println("Xoá nè")
			}

		}

	} else if shift.TotalRevenue-sumRev.SumRev > 0 {
		totalLessEqualVol += 1
		fmt.Println("Nhỏ hơn", "Sum:", sumRev.SumRev, "Data kết ca:", shift.TotalRevenue)
		fmt.Println("ID staff shift", shift.ID)
		cheatTx := database.Transaction{}
		db.First(&cheatTx, `not exists (
			select 1 from invoices 
			where transaction_id = transactions.id
		) and customer_id is null and total_revenue % 1000 <> 0 
		and created_at >= ? and created_at <= ? and gas_pump_id = ? and deleted_at is null`, shift.ActualStartTime, shift.ActualEndTime, shift.GasPumpID)
		if cheatTx.ID != uuid.Nil {
			fmt.Println("Data trước:", cheatTx.TotalRevenue)
			cheatTx.TotalRevenue += shift.TotalRevenue - sumRev.SumRev
			cheatTx.RecordID = "cheat-c3-update-" + cheatTx.RecordID
			fmt.Println("Data sau:", cheatTx.TotalRevenue)
			result := db.Save(&cheatTx)
			fmt.Println("Cheating then CreateOrUpdateBkavInvoice")
			numUpdateLessEqualVol += result.RowsAffected
		}
	} else if shift.TotalRevenue-sumRev.SumRev < 0 {
		totalMoreEqualVol += 1
		fmt.Println("Lớn hơn", "Sum:", sumRev.SumRev, "Data kết ca:", shift.TotalRevenue)
		cheatTx := database.Transaction{}
		db.First(&cheatTx, `not exists (
			select 1 from invoices 
			where transaction_id = transactions.id
		) and customer_id is null and total_revenue % 1000 <> 0 
		and created_at >= ? and created_at <= ? and gas_pump_id = ? and deleted_at is null and total_revenue > ?`, shift.ActualStartTime, shift.ActualEndTime, shift.GasPumpID, sumRev.SumRev-shift.TotalRevenue)
		if cheatTx.ID != uuid.Nil {
			fmt.Println("Data trước:", cheatTx.TotalRevenue)
			cheatTx.TotalRevenue -= sumRev.SumRev - shift.TotalRevenue
			cheatTx.RecordID = "cheat-c3-update-" + cheatTx.RecordID
			fmt.Println("Data sau:", cheatTx.TotalRevenue)
			result := db.Save(&cheatTx)
			fmt.Println("Cheating then CreateOrUpdateBkavInvoice")
			numUpdateMoreEqualVol += result.RowsAffected
		}
	}

	return numAdd, totalLess, totalMore, numUpdate, totalLessEqualVol, totalMoreEqualVol, numUpdateLessEqualVol, numUpdateMoreEqualVol
}
