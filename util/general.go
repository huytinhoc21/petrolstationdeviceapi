package util

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net"
	"net/mail"
	"net/url"
	"petrol-station/database"
	"reflect"
	"regexp"
	"strings"
	"time"
	"unicode"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/inflection"
	"gorm.io/gorm"
)

func MakeTimestamp() string {
	return fmt.Sprint(time.Now().UnixMicro())
}

func CtxTimeout(d time.Duration) context.Context {
	ctx, _ := context.WithTimeout(context.Background(), d)
	return ctx
}

// Dang An: Check if email is a valid email address
func IsValidEmail(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}

// Dang An: Check if s is a valid phone number
func IsPhone(s string) bool {
	if len(s) < 10 || len(s) > 13 {
		return false
	}
	for _, c := range s {
		if !unicode.IsDigit(c) {
			return false
		}
	}
	return true
}

func Convert[T any](v interface{}) T {
	var res T
	v2, err := json.Marshal(v)
	if err != nil {
		return res
	}
	json.Unmarshal(v2, &res)
	return res
}

// Get preferred outbound ip of this machine
func GetOutboundIP() string {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP.String()
}

func IsRunningMode() bool {
	return RUNNING_MODE == gin.ReleaseMode
	// return true
}

func GetFilteredDB(db *gorm.DB, c *gin.Context, columnName any, tableNameArr ...string) *gorm.DB {
	v := reflect.ValueOf(columnName)

	tableName := ""
	if len(tableNameArr) > 0 {
		tableName = tableNameArr[0]
	}

	if tableName == "" {
		tmp := SnakeCase(strings.Replace(reflect.TypeOf(columnName).String(), "database.", "", -1))
		tableName = inflection.Plural(tmp)
	}
	for i := 0; i < v.NumField(); i++ {
		name := SnakeCase(v.Type().Field(i).Name)
		query, _ := url.ParseQuery(c.Request.URL.RawQuery)
		columnFilters := query[name]
		statement := tableName
		if statement != "" {
			statement = statement + "."
		}
		statement = statement + name + " in (?)"
		if len(columnFilters) > 0 {
			db = db.Where(statement, columnFilters)
		}
	}
	return db
}

func SnakeCase(s string) string {
	// Match any sequence of one or more uppercase letters or digits
	re := regexp.MustCompile(`([A-Z0-9]+)`)
	// Replace the matches with the lowercase version of the match followed by an underscore
	snake := re.ReplaceAllStringFunc(s, func(match string) string {
		return "_" + strings.ToLower(match)
	})
	// Remove any leading or trailing underscores
	snake = strings.Trim(snake, "_")
	return snake
}

func IsPostgreSQLDateFormat(str string) bool {
	return true
	layout := "2006-01-02"
	_, err := time.Parse(layout, str)

	if err == nil {
		return true
	}
	layout = "2006-01-02 15:04:05-07:00"
	_, err = time.Parse(layout, str)
	return err == nil
}

func SetQueryArray(c *gin.Context, paramName string, paramValues []string) {
	values := c.Request.URL.Query()
	values[paramName] = paramValues
	c.Request.URL.RawQuery = values.Encode()
}

func RemoveQueryParameter(c *gin.Context, paramName string) {
	values := c.Request.URL.Query()
	values.Del(paramName)
	c.Request.URL.RawQuery = values.Encode()

	query, _ := url.ParseQuery(c.Request.URL.RawQuery)
	columnFilters := query[paramName]
	fmt.Println(columnFilters)

	// Create a new url.Values object from the modified query parameters
	// modifiedQueryValues, _ := url.ParseQuery(c.Request.URL.RawQuery)
	// fmt.Println("**************")
	// fmt.Println(modifiedQueryValues["deposit_id"])
	// fmt.Println(modifiedQueryValues.Get("deposit_id"))
}

func FindElementsWithSum(nums []database.Transaction, targetSum float64, backup *database.MinBackup) []database.Transaction {
	return FindElements(nums, targetSum, 0, []database.Transaction{}, backup)
}

func FindElements(nums []database.Transaction, targetSum float64, currentIndex int, currentSubset []database.Transaction, backup *database.MinBackup) []database.Transaction {
	fmt.Println("targetSum", targetSum)
	if targetSum == 0 {
		// Found a subset with the desired sum
		return currentSubset
	}
	if targetSum < backup.Gap && targetSum >= 0 {
		backup.Arr = currentSubset
		backup.Gap = targetSum
	}

	if targetSum < 0 || currentIndex >= len(nums) {
		// Reached the end of the array or targetSum became negative,
		// indicating that the current subset is not valid
		return nil
	}

	// Include the current element in the subset
	withCurrent := append(currentSubset, nums[currentIndex])
	result := FindElements(nums, targetSum-nums[currentIndex].FuelVolume, currentIndex+1, withCurrent, backup)
	if result != nil {
		// Found a valid subset, return it
		return result
	}

	// Exclude the current element from the subset
	withoutCurrent := FindElements(nums, targetSum, currentIndex+1, currentSubset, backup)
	return withoutCurrent
}

func RoundFloat(val float64, precision uint) float64 {
	ratio := math.Pow(10, float64(precision))
	return math.Round(val*ratio) / ratio
}
