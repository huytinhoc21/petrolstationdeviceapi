package util

import (
	"net/http"

	ginJWT "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func CheckAdminPermission(c *gin.Context) {
	if !IsRunningMode() {
		c.Next()
		return
	}

	tmp, _ := c.Get("JWT_PAYLOAD")
	payload := tmp.(ginJWT.MapClaims)
	role := payload["Role"].(string)

	if role == "admin" {
		c.Next()
	} else {
		c.AbortWithStatus(http.StatusForbidden)
	}
}

func CheckPermission(db *gorm.DB, apiAction string) gin.HandlerFunc {
	return func(c *gin.Context) {
		if !IsRunningMode() {
			c.Next()
			return
		}

		tmp, _ := c.Get("JWT_PAYLOAD")
		payload := tmp.(ginJWT.MapClaims)
		id := payload["ID"].(float64)
		userId := uint(id)

		count := struct {
			Count int64
		}{}
		db.Raw(`select count(*) from users 
		join user_permissions on users.id = user_permissions.user_id
		join permissions on permissions.id = user_permissions.permission_id
		join api_action_permissions on api_action_permissions.permission_id = permissions.id
		join api_actions on api_actions.id = api_action_permissions.api_action_id
		where users.id = ? and api_actions.name = ?`, userId, apiAction).Scan(&count)

		if count.Count == 1 {
			c.Next()
		} else {
			c.AbortWithStatus(http.StatusForbidden)
		}
	}
}
