package util

import (
	"errors"
	"fmt"
	"math"
	"os"
	"petrol-station/database"
	"petrol-station/model"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type InputGood struct {
	GoodName     string    `json:"good_name"`
	Vat          int       `json:"vat"`
	GasStationID uuid.UUID `json:"gas_station_id" gorm:"type:uuid;unique"`
}

func CreateInvoiceOnStaffShift(shift *database.StaffShift, delay time.Duration, db *gorm.DB) {
	transactions := []database.Transaction{}
	db.Table("transactions").
		Where(`
			created_at >= ? and created_at <= ? and 
			gas_pump_id = ? and deleted_at is null and 
			not exists (
				select 1 from invoices 
				where transaction_id = transactions.id
			)`,
			shift.ActualStartTime, shift.ActualEndTime, shift.GasPumpID).
		Order("created_at asc").
		Find(&transactions)
	fmt.Println("************** Create Invoice on Staff shift *****************")
	fmt.Println("len(transactions)", len(transactions))
	fmt.Println("*************************************************************")
	for _, transaction := range transactions {
		for i := 0; i < 10; i++ { // Retry 10 times
			_, err := CreateInvoiceFromTransaction(transaction, false, false, db)
			if err == nil {
				break
			}
			time.Sleep(delay * time.Millisecond)
		}
		time.Sleep(delay * time.Millisecond)
	}
}

func CreateOrUpdateBkavInvoiceOnStart(db *gorm.DB, gasStationID uuid.UUID) {
	var transactions []database.Transaction
	// lastDay := time.Now().Add(-24 * 7 * time.Hour)
	startInvoiceDate := time.Date(2024, 3, 21, 0, 0, 0, 0, time.Local) // Huy's birthday :)
	fmt.Println("startInvoiceDate", startInvoiceDate)

	db.Raw(`select t.*	
	from invoices i
	join transactions t on i.transaction_id = t.id
	join gas_pumps on t.gas_pump_id = gas_pumps.id
	join gas_tanks on gas_pumps.gas_tank_id = gas_tanks.id
	where 
		
		 i.invoice_guid is null
		and t.created_at >= '2024-03-20 17:00:00' 
		and gas_tanks.gas_station_id = ?`, gasStationID).
		Find(&transactions).Order("t.created_at desc")
	fmt.Println("************** Create or Update Invoice *****************")
	fmt.Println("len(transactions)", len(transactions))
	fmt.Println("*********************************************************")
	for _, transaction := range transactions {
		CreateOrUpdateBkavInvoice(transaction, db)
		time.Sleep(100 * time.Millisecond)
	}
}

func CreateOrUpdateBkavInvoice(req database.Transaction, db *gorm.DB) {
	fmt.Println("CreateOrUpdateBkavInvoice Transaction", req.ID)

	good := GetInputGood(req, db)

	// create or update if exist
	CreateOrUpdateBkavInvoiceTimeout := func() {

		// cancel if transaction deleted
		transaction := database.Transaction{}
		db.Debug().Table("transactions").First(&transaction, "id = ? and deleted_at is null", req.ID)

		invoiceSetting := database.InvoiceSetting{}
		db.Table("invoice_settings").First(&invoiceSetting, "gas_station_id = ?", good.GasStationID)
		fmt.Println("invoiceSetting.ID, invoiceSetting.AutoExport", invoiceSetting.ID, invoiceSetting.AutoExport)

		// if invoiceSetting.ID == 0 || !invoiceSetting.AutoExport {
		// 	return
		// }

		fmt.Println("transaction.ID", transaction.ID, transaction.ID == uuid.Nil, req.ID)
		if transaction.ID == uuid.Nil {
			return
		}

		invoice := database.Invoice{
			CustomerName:  "Khách lẻ",
			Email:         "",
			PaymentMethod: 1,

			GoodsRevenue: math.Round(transaction.TotalRevenue / (TaxRate[good.Vat] + 1)),
			VatRevenue:   math.Round(transaction.TotalRevenue / (TaxRate[good.Vat] + 1) * TaxRate[good.Vat]),
			TotalRevenue: transaction.TotalRevenue,

			TransactionID: &transaction.ID,
			CreatedAt:     time.Now(),

			Draft: true,
			Date:  transaction.CreatedAt,
			Type:  "transaction-auto",
		}

		existInvoice := database.Invoice{}
		db.Table("invoices").First(&existInvoice, "transaction_id = ?", transaction.ID)
		isExist := false

		if existInvoice.ID == 0 {

			err := db.Table("invoices").Omit("gas_station_id, invoice_guid").Create(&invoice).Error
			if err != nil {
				fmt.Println("err", err.Error())
			}
		} else {
			if existInvoice.Type == "transaction-manual" {
				return
			}
			if existInvoice.InvoiceGUID != "" {
				isExist = true
			}
			err := db.Table("invoices").Omit("gas_station_id, date, type").
				Where("id = ?", existInvoice.ID).Updates(&invoice).Error
			invoice.ID = existInvoice.ID
			invoice.Date = existInvoice.Date
			invoice.Type = existInvoice.Type

			if err != nil {
				fmt.Println("err", err.Error())
			}
		}
		fmt.Println("isExist", isExist)
		fmt.Println("invoice.ID", invoice.ID)

		fmt.Println("invoice.Date", invoice.Date)
		buyerName := "Khách lẻ"
		if invoice.CustomerName != "" {
			buyerName = invoice.CustomerName
		}

		command := []database.CommandObject100{{
			Invoice: database.BKAVInvoice{
				Note:          "Thời gian giao dịch: " + transaction.CreatedAt.Format("2006-01-02 15:04:05"),
				InvoiceTypeID: 1,
				InvoiceDate:   time.Date(invoice.Date.Year(), invoice.Date.Month(), invoice.Date.Day(), 12, 0, 0, 0, time.Local),
				PayMethodID:   1,     //1: tiền mặt 2 chuyển khoản 3: TM/CK
				ReceiveTypeID: 1,     //           int       `json:"receive_type_id"`           //3,//Hình thức nhận Hoá đơn, 1:Email, 2: SMS, 3: Email & SMS, 4: Chuyển phát nhanh
				CurrencyID:    "VND", //              string    `json:"currency_id"`               // "VND",					//Đơn vị tiền tệ, mặc định VND
				ExchangeRate:  1.0,   //            float32   `json:"exchange_rate"`             //": 1.0,					//Tỷ giá, mặc định là 1
				BuyerName:     buyerName,
				BuyerTaxCode:  invoice.TaxCode, //            string    `json:"buyer_tax_code"`            // "0123456789",				//Mã số Thuế
				BuyerUnitName: invoice.Unit,    //           string    `json:"buyer_unit_name"`           // "CONG TY ABC",			//Đơn vị mua hàng
				BuyerAddress:  invoice.Address,
			},
			PartnerInvoiceStringID: strconv.Itoa(int(invoice.ID)),
		}}

		command[0].ListInvoiceDetailsWS = append(command[0].ListInvoiceDetailsWS, database.InvoiceDetailsWS{
			ItemName: good.GoodName,
			UnitName: "Lít",
			Qty:      RoundFloat(transaction.FuelVolume, 2),
			Price:    RoundFloat(transaction.UnitPrice/(1+TaxRate[good.Vat]), 2),
			Amount:   math.Round(transaction.TotalRevenue / (1 + TaxRate[good.Vat])),

			// // TODO
			TaxRateID: good.Vat,          // 1: 0%, 2: 5%, 3: 10%
			TaxRate:   TaxRate[good.Vat], //           float64 // 10.0,
			// // TODO
			TaxAmount: math.Round(transaction.TotalRevenue - transaction.TotalRevenue/(1+TaxRate[good.Vat])), // 100.0, 				//Tiền thuế

			// OtherAmount       float64 //2000,                                       // Thuế, phí khác theo chi tiết mặt hàng
			// DiscountRate      float64 // 0.0,				//Tỷ lệ chiết khấu (xem tại đây)
			// DiscountAmount    float64 //"",			//Số tiền chiết khấu (xem tại đây)
			// IsDiscount        bool    // false,				// true - chiết khấu, false - Hàng hoá dịch vụ  (xem tại đây)
			// UserDefineDetails string  // "",		           // Thông tin đặc biệt trên Hoá đơn (xem tại đây)
			// ItemTypeID        int     // 0,				// Loại Item (xem tại đây)
			// IsIncrease        *bool
		})

		var cmdType int64 = 101 // 100: Tạo nháp, 101: Tạo chờ ký (có số hđ)
		if isExist {
			cmdType = 200 // Update
		}
		bkavRes, err := BKAV_Request(cmdType, command, good.GasStationID)

		if err != nil {
			file, err := os.OpenFile("error.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
			if err != nil {
				fmt.Println("Failed to open log file:", err)
			}
			logger := logrus.New()
			logger.SetOutput(file)
			logger.Error("Add invoice BKAV error from creating transaction - Request Err" + err.Error())
			defer file.Close()
			fmt.Println("Add invoice BKAV error from creating transaction - Request Err" + err.Error())
		}

		objectJSON, _ := bkavRes["Object"].(string)
		objects, err := ParseBKAVObjectRes100(objectJSON)

		if err == nil && len(objects) > 0 {
			if objects[0].Status != 0 {
				file, err := os.OpenFile("error.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
				if err != nil {
					fmt.Println("Failed to open log file:", err)
				}
				logger := logrus.New()
				logger.SetOutput(file)
				logger.Error("Add invoice BKAV error from creating transaction Status <> 0 - Mess log:" + objects[0].MessLog)
				defer file.Close()
				fmt.Println("Add invoice BKAV error from creating transaction Status <> 0 - Mess log:" + objects[0].MessLog)
			} else {
				fmt.Println("CreateOrUpdateBkavInvoice Successful! Transaction", req.ID)
				db.Table("invoices").Debug().Where("id = ?", invoice.ID).Updates(map[string]any{
					"invoice_guid": objects[0].InvoiceGUID,
					"invoice_no":   objects[0].InvoiceNo,
				})
			}
		}
	}
	//// end of func

	var invoice_setting struct {
		WaitingTime uint `json:"waiting_time"`
	}
	db.Table("invoice_settings").First(&invoice_setting, "gas_station_id = ?", good.GasStationID)

	waitingTime := invoice_setting.WaitingTime
	if RUNNING_MODE == gin.DebugMode {
		waitingTime -= 1 // Create invoice earlier when debug
	}

	delayDuration := time.Until(req.CreatedAt.Add(time.Duration(waitingTime) * time.Minute))
	if req.CreatedAt.Add(time.Duration(waitingTime)*time.Minute).Compare(time.Now()) == -1 {
		delayDuration = 0
	}
	fmt.Println("invoice_setting.WaitingTime", invoice_setting.WaitingTime)
	fmt.Println("req.CreatedAt", req.CreatedAt, req.CreatedAt.Add(time.Duration(waitingTime)*time.Minute), req.CreatedAt.Add(time.Duration(waitingTime)*time.Minute).Compare(time.Now()))
	fmt.Println("delayDuration", delayDuration)

	// fmt.Println(delayDuration)

	time.AfterFunc(0, func() {
		CreateOrUpdateBkavInvoiceTimeout()
	})

}

// DO NOT WRITE TO DB HERE - ONLY READ AND CALL TO BKAV
func CreateOrUpdateInvoice(invoice model.ReqInvoice, isExist bool, gasStationID *uuid.UUID, db *gorm.DB) ([]BKAV100ObjRes, error) {
	manualNote := ""
	invoiceDate := invoice.Date
	transaction := database.Transaction{}
	db.Table("transactions").First(&transaction, "id = ?", invoice.TransactionID)
	if invoice.TransactionID == &uuid.Nil {
		manualNote = "Hoá đơn tạo thủ công"
	} else {
		invoiceDate = transaction.CreatedAt
	}

	command := []database.CommandObject100{{
		Invoice: database.BKAVInvoice{
			InvoiceTypeID: 1,
			InvoiceDate:   time.Date(invoiceDate.Year(), invoiceDate.Month(), invoiceDate.Day(), 12, 0, 0, 0, time.Local),
			BuyerName:     invoice.CustomerName,
			BuyerTaxCode:  invoice.TaxCode,       //            string    `json:"buyer_tax_code"`            // "0123456789",				//Mã số Thuế
			BuyerUnitName: invoice.Unit,          //           string    `json:"buyer_unit_name"`           // "CONG TY ABC",			//Đơn vị mua hàng
			BuyerAddress:  invoice.Address,       //         string    `json:"buyer_address"`             // "So 632, Duong A, Q. Hai Ba Trung",// Địa chỉ người/đơn vị mua hàng
			PayMethodID:   invoice.PaymentMethod, //1: tiền mặt 2 chuyển khoản
			ReceiveTypeID: 1,                     //           int       `json:"receive_type_id"`           //3,//Hình thức nhận Hoá đơn, 1:Email, 2: SMS, 3: Email & SMS, 4: Chuyển phát nhanh
			ReceiverEmail: invoice.Email,         //           string    `json:"receiver_email"`            // "", 					//Email nhận thông báo tra cứu Hoá đơn
			Note: fmt.Sprintf("%s\nBiển số xe: %s\nThời gian: %s",
				manualNote,
				invoice.VehicleID,
				invoiceDate.Format("2006-01-02 15:04:05")), //                    string    `json:"note"`                      // "Ghi Chú",					 //Ghi chú trên Hoá đơn
			CurrencyID:   "VND",                              //              string    `json:"currency_id"`               // "VND",					//Đơn vị tiền tệ, mặc định VND
			ExchangeRate: 1.0,                                //            float32   `json:"exchange_rate"`             //": 1.0,					//Tỷ giá, mặc định là 1
			UserDefine:   "Biển số xe: " + invoice.VehicleID, //              string    `json:"user_define"`               // "",					//Thông tin đặc biệt trên Hoá đơn, tham khảo tại đây
		},
		PartnerInvoiceStringID: strconv.Itoa(int(invoice.ID)),
	}}
	for _, v := range invoice.Goods {
		command[0].ListInvoiceDetailsWS = append(command[0].ListInvoiceDetailsWS, database.InvoiceDetailsWS{
			ItemName:  v.Name,
			UnitName:  v.Unit,
			Qty:       v.Amount,
			Price:     v.UnitPrice,
			Amount:    math.Round(v.Revenue),
			TaxRateID: v.TaxRateID,             // 1: 0%, 2: 5%, 3: 10%
			TaxRate:   v.TaxRate,               //           float64 // 10.0,
			TaxAmount: math.Round(v.TaxAmount), // 100.0, 				//Tiền thuế
		})
	}

	var cmdType int64 = 101 // 100: Tạo nháp, 101: Tạo chờ ký (có số hđ)
	if isExist {
		cmdType = 200 // Update
	}

	if gasStationID == nil || gasStationID == &uuid.Nil {
		good := GetInputGood(transaction, db)
		gasStationID = &(good.GasStationID)
	}
	bkavRes, err := BKAV_Request(cmdType, command, *gasStationID)
	if err != nil {
		return nil, err
	}
	fmt.Println("bkavRes", bkavRes)
	fmt.Println("gasStationID", gasStationID)

	objectJSON, _ := bkavRes["Object"].(string)
	objects, err := ParseBKAVObjectRes100(objectJSON)

	if err != nil {
		return nil, err
	}

	if len(objects) == 0 {
		return nil, errors.New("BKAV object is empty")
	}

	if objects[0].Status == 0 {
		return objects, nil
	}

	if !strings.Contains(objects[0].MessLog, "Đã tồn tại") || strings.Contains(objects[0].MessLog, "lớn hơn") {
		return nil, errors.New(objects[0].MessLog)
	}
	msgLog := objects[0].MessLog

	cmdType = 200

	bkavRes, err = BKAV_Request(cmdType, command, *gasStationID)
	if err != nil {
		return nil, err
	}

	objectJSON, _ = bkavRes["Object"].(string)
	objects, err = ParseBKAVObjectRes100(objectJSON)

	if err != nil {
		return nil, errors.New(msgLog + " & " + err.Error())
	}

	if len(objects) == 0 {
		return nil, errors.New("BKAV object is empty")
	}

	if objects[0].Status != 0 {
		return nil, errors.New(msgLog + " & " + objects[0].MessLog)
	}

	return objects, nil

}

func CreateInvoiceFromTransaction(transaction database.Transaction, allowUpdate bool, useCurrentDate bool, db *gorm.DB) (*database.Invoice, error) {

	fmt.Println("CreateInvoiceFromTransaction -- transaction.RecordID:", transaction.RecordID)

	existInvoice := database.Invoice{}
	db.First(&existInvoice, "transaction_id = ?", transaction.ID)
	if existInvoice.InvoiceGUID != "" && !allowUpdate {
		return nil, fmt.Errorf("không thể cập nhật! Hoá đơn đã được tạo với mã số: %v", existInvoice.InvoiceNo)
	}

	good := GetInputGood(transaction, db)
	invoice := GetInitialInvoice(transaction, &good, db)
	invoiceSetting := database.InvoiceSetting{}
	db.Table("invoice_settings").First(&invoiceSetting, "gas_station_id = ?", good.GasStationID)

	if useCurrentDate {
		invoice.Date = time.Now()
	}
	var err error = nil
	if existInvoice.ID == 0 {
		err = db.Table("invoices").Omit("gas_station_id, invoice_guid").Create(&invoice).Error
	} else {
		if !existInvoice.Draft {
			return &existInvoice, errors.New("không thể chỉnh sửa hoá đơn đã được ký phát hành")
		}
		err = db.Table("invoices").Omit("gas_station_id, invoice_guid").
			Where("id = ?", existInvoice.ID).
			Updates(&invoice).Error
		invoice.ID = existInvoice.ID
	}
	if err != nil {
		return nil, err
	}

	bkavInvoice := database.BKAVInvoice{
		Note:          "Thời gian: " + transaction.CreatedAt.Format("2006-01-02 15:04:05"),
		InvoiceTypeID: 1,
		InvoiceDate:   time.Date(invoice.Date.Year(), invoice.Date.Month(), invoice.Date.Day(), 12, 0, 0, 0, time.Local),
		PayMethodID:   1,     //1: tiền mặt 2 chuyển khoản 3: TM/CK
		ReceiveTypeID: 1,     //           int       `json:"receive_type_id"`           //3,//Hình thức nhận Hoá đơn, 1:Email, 2: SMS, 3: Email & SMS, 4: Chuyển phát nhanh
		CurrencyID:    "VND", //              string    `json:"currency_id"`               // "VND",					//Đơn vị tiền tệ, mặc định VND
		ExchangeRate:  1.0,   //            float32   `json:"exchange_rate"`             //": 1.0,					//Tỷ giá, mặc định là 1
		BuyerName:     "Khách lẻ",
	}

	if existInvoice.CustomerName != "" {
		bkavInvoice.BuyerName = existInvoice.CustomerName
	}
	if existInvoice.TaxCode != "" {
		bkavInvoice.BuyerTaxCode = existInvoice.TaxCode
	}
	if existInvoice.Unit != "" {
		bkavInvoice.BuyerUnitName = existInvoice.Unit
	}
	if existInvoice.Address != "" {
		bkavInvoice.BuyerAddress = existInvoice.Address
	}
	if existInvoice.PaymentMethod != 0 {
		bkavInvoice.PayMethodID = existInvoice.PaymentMethod
	}
	if existInvoice.VehicleID != "" {
		bkavInvoice.Note = fmt.Sprintf("Biển số xe: %s\nThời gian: %s",
			invoice.VehicleID,
			invoice.Date.Format("2006-01-02 15:04:05"))
	}

	command := []database.CommandObject100{{
		Invoice:                bkavInvoice,
		PartnerInvoiceStringID: strconv.Itoa(int(invoice.ID)),
	}}

	command[0].ListInvoiceDetailsWS = append(command[0].ListInvoiceDetailsWS, database.InvoiceDetailsWS{
		ItemName: good.GoodName,
		UnitName: "Lít",
		Qty:      RoundFloat(transaction.FuelVolume, 2),
		Price:    RoundFloat(transaction.UnitPrice/(1+TaxRate[good.Vat]), 2),
		Amount:   math.Round(transaction.TotalRevenue / (1 + TaxRate[good.Vat])),

		TaxRateID: good.Vat,                                                                              // 1: 0%, 2: 5%, 3: 10%
		TaxRate:   TaxRate[good.Vat],                                                                     //           float64 // 10.0,
		TaxAmount: math.Round(transaction.TotalRevenue - transaction.TotalRevenue/(1+TaxRate[good.Vat])), // 100.0, 				//Tiền thuế
	})

	var cmdType int64 = 101 // 100: Tạo nháp, 101: Tạo chờ ký (có số hđ)
	if existInvoice.InvoiceGUID != "" {
		cmdType = 200 // Update
	}
	bkavRes, err := BKAV_Request(cmdType, command, good.GasStationID)

	if err != nil {
		errMsg := "Add invoice BKAV error from creating transaction - Request Err" + err.Error()
		LogError(errMsg)
		invoice.InvoiceMsg = errMsg
		db.Table("invoices").Where("id = ?", invoice.ID).Updates(&invoice)
		return &invoice, errors.New(errMsg)
	}

	objectJSON, _ := bkavRes["Object"].(string)
	objects, err := ParseBKAVObjectRes100(objectJSON)

	if err != nil {
		invoice.InvoiceMsg = err.Error()
	} else if len(objects) == 0 {
		invoice.InvoiceMsg = "BKAV object is empty"
	} else if objects[0].Status != 0 {
		invoice.InvoiceMsg = objects[0].MessLog
	} else {
		scheduled := transaction.CreatedAt.Add(time.Duration(invoiceSetting.WaitingTime) * time.Minute)
		invoice.Scheduled = &scheduled
		invoice.InvoiceGUID = objects[0].InvoiceGUID
		invoice.InvoiceNo = objects[0].InvoiceNo
	}
	db.Table("invoices").Where("id = ?", invoice.ID).Updates(&invoice)

	if invoice.InvoiceMsg != "" {
		return &invoice, errors.New(invoice.InvoiceMsg)
	}
	return &invoice, nil

}

func LogError(msg string) {
	file, err := os.OpenFile("error.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		fmt.Println("Failed to open log file:", err)
	}
	logger := logrus.New()
	logger.SetOutput(file)
	logger.Error(msg)
	defer file.Close()
	fmt.Println(msg)
}

func GetInitialInvoice(transaction database.Transaction, good *InputGood, db *gorm.DB) database.Invoice {
	if good == nil {
		_good := GetInputGood(transaction, db)
		good = &_good
	}
	invoice := database.Invoice{
		CustomerName:  "Khách lẻ",
		Email:         "",
		PaymentMethod: 1,

		GoodsRevenue: math.Round(transaction.TotalRevenue / (TaxRate[good.Vat] + 1)),
		VatRevenue:   math.Round(transaction.TotalRevenue / (TaxRate[good.Vat] + 1) * TaxRate[good.Vat]),
		TotalRevenue: transaction.TotalRevenue,

		TransactionID: &transaction.ID,
		CreatedAt:     time.Now(),

		Draft: true,
		Date:  transaction.CreatedAt,
		Type:  "transaction-auto",
	}
	return invoice
}

func GetInputGood(transaction database.Transaction, db *gorm.DB) InputGood {
	var good InputGood
	// Get all records

	db.Table("gas_pumps").
		Select(`goods.name as good_name, goods.vat as vat, gas_tanks.gas_station_id`).
		Joins("left join gas_tanks on gas_pumps.gas_tank_id = gas_tanks.id").
		Joins("left join goods on gas_tanks.good_id = goods.id").
		Where("gas_pumps.id = ?", transaction.GasPumpID).
		Find(&good)
	return good
}
