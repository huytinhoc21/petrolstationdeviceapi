package util

import (
	"os"

	"github.com/gin-gonic/gin"
)

func getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

var JWT_SECRET = []byte(getenv("JWT_SECRET", "asdasdiu_awsisuadhome_sasiduhtaruitup"))

var PORT = getenv("PORT", "4000")
var RUNNING_MODE = getenv("RUNNING_MODE", gin.DebugMode) // Dang An: Set mode to debug when coding and for clearer management

var DOMAIN = getenv("DOMAIN", "https://petrol-station.vn")
