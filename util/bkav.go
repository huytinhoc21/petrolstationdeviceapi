package util

import (
	"bytes"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"petrol-station/database"

	"github.com/gofrs/uuid"
)

var PARTNER_TOKEN = "8irsn2Otjozj860yMesmVXLFcXupmnNUUq7aT2slXCI=:bkHPKndQn7j7k2pgzei8Vw=="
var PARTNER_GUID = "462904a4-7a93-4013-987f-166388065408"
var BKAV_URL = "https://ws.ehoadon.vn/WSPublicEhoadon.asmx/ExecCommand"

type PartnerInfo struct {
	Token string
	GUID  string
}

var stationInfo = map[uuid.UUID]PartnerInfo{
	uuid.Must(uuid.FromString("6c69964a-0b98-4ccb-a7d4-6f2a0d681e3e")): { //chxd.27
		Token: "3aBdpzrNmTwS3O413V+8cMRsAdEFNxO2jmrjA2E/oM8=:7QjzGOwcMqDm6HO/OOvdaA==",
		GUID:  "f9df9144-84ab-4181-83e5-ab4bce2b1ff4",
	},
	uuid.Must(uuid.FromString("7962b3bf-7916-4256-a4e6-53e93344d87d")): { //chxd.28
		Token: "e7czD90tsp2nPTfINyYxYdYae/RN1x9+9REWvC1OBPQ=:YYfoWU6aZgKYZh46uw0/Uw==",
		GUID:  "a5c2c8b7-d08b-4dda-b1b5-948f5c636ba1",
	},
	uuid.Must(uuid.FromString("3b906569-0721-4066-af3e-9a41e5ce66f9")): { //chxd.mp
		Token: "JWx8p1BAQaOrVtu/qgdt91fzLIBXhchUTZvTkKnIqXA=:jm83lc8eSh4uHVHLVO1kSw==",
		GUID:  "297970a6-efe0-4084-914e-a2ff0020632c",
	},
}

func Base64ToJSON(base64String string) (map[string]interface{}, error) {
	// Decode base64 string to binary data
	decodedBytes, err := base64.StdEncoding.DecodeString(base64String)
	if err != nil {
		return nil, err
	}

	// Parse binary data into JSON
	var jsonData map[string]interface{}
	err = json.Unmarshal(decodedBytes, &jsonData)
	if err != nil {
		return nil, err
	}

	return jsonData, nil
}

func StructToBase64(person database.CommandData) (string, error) {
	// Convert struct to JSON
	jsonData, err := json.Marshal(person)
	if err != nil {
		return "", err
	}

	// fmt.Println(string(jsonData))
	// Encode JSON to base64 with padding
	encoder := base64.StdEncoding.WithPadding(base64.StdPadding)
	base64String := encoder.EncodeToString(jsonData)
	return base64String, nil
}

func BKAV_Request(cmdType int64, commandObject interface{}, stationId uuid.UUID) (map[string]interface{}, error) {
	// commandObject := database.CommandObject{
	// 	Invoice: database.Invoice{
	// 		InvoiceTypeID:           1,                                                                                 /// Loại Hoá đơn (Xem chi tiết mục 4.1)
	// 		InvoiceDate:             time.Now(),                                                                        /// Ngày trên Hoá đơn
	// 		BuyerName:               "Nguyễn Văn A",                                                                    /// Tên người mua hàng
	// 		BuyerTaxCode:            "0104746603",                                                                      /// Mã số thuế Người mua hàng
	// 		BuyerUnitName:           "Công Ty Luật TNHH ABC",                                                           /// Tên đơn vị mua hàng
	// 		BuyerAddress:            "Nhà N2D Khu ĐT Trung Hoà-Nhân Chính, Phường Nhân Chính, Quận Thanh Xuân, Hà Nội", /// Địa chỉ đơn vị mua hàng
	// 		BuyerBankAccount:        "",                                                                                /// Thông tin tài khoản ngân hàng người mua ví dụ: 11111111111 - BIDV chi nhánh Tây Hồ
	// 		PayMethodID:             1,                                                                                 /// Hình thức thanh toán (Xem chi tiết mục 4.3)
	// 		ReceiveTypeID:           3,                                                                                 /// Hình thức nhận Hoá đơn: 1	Email , 2	Tin nhắn, 3	Email và tin nhắn, 4	Chuyển phát nhanh
	// 		ReceiverEmail:           "testABC@gmail.com",                                                               /// eMail nhận Hoá đơn
	// 		ReceiverMobile:          "0379823839",                                                                      /// Số điện thoại nhận Hoá đơn
	// 		ReceiverAddress:         "Nhà N2D Khu ĐT Trung Hoà-Nhân Chính, Phường Nhân Chính, Quận Thanh Xuân, Hà Nội", /// Địa chỉ nhận Hoá đơn (Hoá đơn in chuyển đổi)
	// 		ReceiverName:            "Nguyễn Văn A",                                                                    /// Tên người nhận Hoá đơn (Hoá đơn in chuyển đổi)
	// 		Note:                    "Test eHoaDon",                                                                    /// Ghi chú trên Hoá đơn
	// 		BillCode:                "",                                                                                /// Mã ID chứng từ kế toán hoặc số Bill code của Hoá đơn Bán hàng
	// 		CurrencyID:              "VND",                                                                             /// ID tiền tệ: VND - Việt Nam đồng (mặc định), USD - Đô la Mỹ, EUR - Đồng Euro, GBP - Bảng Anh, CNY - Nhân dân tệ,CHF - Phơ răng Thuỵ Sĩ ...
	// 		ExchangeRate:            1.0,                                                                               /// Tỷ giá ngoại tệ so với VND: mặc định là 1
	// 		InvoiceForm:             "",                                                                                /// Mẫu số Hóa đơn, Phải truyền khi dùng mã lệnh 110, 111 và 112
	// 		InvoiceSerial:           "",                                                                                /// Ký hiệu Hóa đơn, Phải truyền khi dùng mã lệnh 110, 111 và 112
	// 		InvoiceNo:               0,                                                                                 /// Số hóa đơn, Phải truyền khi dùng mã lệnh 111
	// 		OriginalInvoiceIdentify: "",                                                                                /// Thông tin Hoá đơn gốc dùng trong trường hợp thay thế, điều chỉnh. Định dạng như sau: [Mẫu Số]_[Ký hiệu]_[Số Hoá đơn], ví dụ: [01GTKT0/001]_[AA/17E]_[0000001]
	// 	},
	// 	ListInvoiceDetailsWS: []datatype.InvoiceDetailsWS{
	// 		{
	// 			ItemName:   "Chữ ký số Bkav CA ENT BN (bao gồm Thiết bị USB Token) update", /// Tên hàng hóa, dịch vụ hoặc nội dung giảm giá chiết khấu (IsDiscount = 1)
	// 			UnitName:   "Gói",                                                          /// Đơn vị tính hàng hóa, dịch vụ
	// 			Qty:        1.0,                                                            /// Số lượng hàng hóa dịch vụ
	// 			Price:      600000.0,                                                       /// Giá của hàng hóa
	// 			Amount:     600000.0,                                                       /// Thành tiền hàng hóa dịch vụ hoặc số tiền chiết khấu
	// 			TaxRateID:  3,                                                              /// ID thuế suất:    1 - 0%  | 2- 5% |  3 - 10% |  4 - Không chịu thuế|  5 - Không kê khai thuế | 6 - Thuế suất khác
	// 			TaxRate:    10,                                                             /// Chi tiết thuế suất:    0 – 0% | 5 -  5% | 10 -10% | -1 – Không chịu thuế | -2 – Không kê khai thuế | -4 -  xxx
	// 			TaxAmount:  60000.0,                                                        /// Thành tiền thuế
	// 			IsDiscount: false,                                                          /// Là chiết khấu ghi trên Hoá đơn: 1 - là chiết khấu, mặc định là 0 (Để số dương, hệ thống Bkav sẽ tự trừ khi hiển thị trên Web ehoadon)
	// 			IsIncrease: nil,                                                            ///Hóa đơn bình thường hoặc Hoá đơn điều chỉnh thông tin: có giá trị là Null, Hóa đơn điều chỉnh: False - dòng hàng hóa dịch vụ bị điều chỉnh giảm, True - dòng hàng hóa dịch vụ bị điều chỉnh tăng
	// 			ItemTypeID: 0,                                                              ///Loại hàng hoá (Xem chi tiết tại mục 4.2)
	// 		},
	// 	},
	// 	ListInvoiceAttachFileWS: []string{},
	// 	PartnerInvoiceID:        0,
	// 	PartnerInvoiceStringID:  "aaaaaa",
	// }
	commandData := database.CommandData{
		CmdType:       cmdType,
		CommandObject: commandObject,
		// CommandObject: []database.CommandObject{commandObject},
	}

	fmt.Println("commandData", commandData)

	data, _ := StructToBase64(commandData)

	partnerGuid, ok := stationInfo[stationId]
	if !ok {
		return nil, errors.New("station id không hợp lệ " + stationId.String())
	}

	// Create the request body
	requestBody := []byte(fmt.Sprintf(`{"partnerGUID": "%s", "CommandData": "%s"}`, partnerGuid.GUID, data))

	fmt.Println(fmt.Sprintf(`{"partnerGUID": "%s", "CommandData": "%s"}`, partnerGuid.GUID, data))
	// Create a new HTTP POST request
	req, err := http.NewRequest("POST", BKAV_URL, bytes.NewBuffer(requestBody))
	if err != nil {
		fmt.Println("Error creating HTTP request:", err)
		return nil, err
	}

	// Set the Content-Type header
	req.Header.Set("Content-Type", "application/json")

	//Create an HTTP client with custom settings (e.g., to disable SSL verification)
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	//Send the HTTP request
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error sending HTTP request:", err)
		return nil, err
	}
	defer resp.Body.Close()

	// Read the response body
	body, err := io.ReadAll(resp.Body)

	if err != nil {
		fmt.Println("Error reading response body:", err)
		return nil, err
	}

	var res struct {
		D string `json:"d"`
	}
	// Print the response body
	err = json.Unmarshal(body, &res)
	if err != nil {
		errMsg := fmt.Sprintf("Error parsing response: %s | %s", err.Error(), res.D)
		return nil, errors.New(errMsg)
	}

	decodeRes, err := Base64ToJSON(res.D)
	if err != nil {
		errMsg := fmt.Sprintf("Error parsing response: %s | %s", err.Error(), res.D)
		return nil, errors.New(errMsg)
	}

	return decodeRes, nil
}

type BKAVRes struct {
	Status  int
	Object  string
	IsOK    bool `json:"isOk"`
	IsError bool `json:"isError"`
}

type BKAV800ObjRes struct {
	Invoice struct {
		InvoiceStatusID int
		InvoiceNo       int
		InvoiceGUID     string
	}
}

func ParseBKAVObjectRes800(objectJSON string) (BKAV800ObjRes, error) {
	var objects BKAV800ObjRes

	err := json.Unmarshal([]byte(objectJSON), &objects)
	if err != nil {
		return BKAV800ObjRes{}, fmt.Errorf("error parsing BKAVObjectRes800: %w | %s", err, objectJSON)
	}

	return objects, nil
}

type BKAV100ObjRes struct {
	Status      int
	MessLog     string
	InvoiceGUID string
	InvoiceNo   uint64
}

func ParseBKAVObjectRes100(objectJSON string) ([]BKAV100ObjRes, error) {
	var objects []BKAV100ObjRes

	err := json.Unmarshal([]byte(objectJSON), &objects)
	if err != nil {
		return nil, fmt.Errorf("error parsing BKAVObjectRes100: %w | %s", err, objectJSON)
	}

	return objects, nil
}

var TaxRate = map[int]float64{
	1: 0,
	2: 5.0 / 100,
	3: 10.0 / 100,
	4: 0,
	5: 0,
	6: 0,
	7: (5.0 / 100) * (70.0 / 100),
	8: (10.0 / 100) * (70.0 / 100),
	9: 8.0 / 100,
}
