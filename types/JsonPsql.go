package types

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
)

// // util.JsonPsql use for db.Raw and db.Exec type json
type JsonPsql struct {
	Val interface{}
}

// Value to write to database
// Called by db.Table().Create()
func (jsonPsql JsonPsql) Value() (driver.Value, error) {
	return json.Marshal(jsonPsql.Val)
}

// Scan to read from database
// Called by db.Table().Find
func (jsonPsql *JsonPsql) Scan(src interface{}) error {

	if src == nil {
		jsonPsql.Val = nil
		return nil
	}
	var source []byte
	switch v := src.(type) {
	case string:
		source = []byte(v)
	case []byte:
		source = v
	default:
		return errors.New("need type string or []byte for json")
	}
	return json.Unmarshal(source, &jsonPsql.Val)
}

func (jsonPsql JsonPsql) MarshalJSON() ([]byte, error) {
	return json.Marshal(jsonPsql.Val)
}

func (jsonPsql *JsonPsql) UnmarshalJSON(bytes []byte) error {
	return json.Unmarshal(bytes, &jsonPsql.Val)
}

// func (jsonPsql *JsonPsql) Copy(obj JsonPsql) error {
// 	bytes, err := obj.MarshalJSON()
// 	if err != nil {
// 		return err
// 	}
// 	return json.Unmarshal(bytes, &jsonPsql.Val)
// }

// util.NewJsonPsql create util.JsonPsql only from map[string]interface{}
// func NewJsonPsql(src interface{}) []byte {
// 	if src == nil {
// 		return nil
// 	}
// 	res, err := json.Marshal(src)
// 	if err != nil {
// 		panic(fmt.Sprintf("util.NewJsonPsql marshal error: %v", err))
// 	}
// 	return res
// }
