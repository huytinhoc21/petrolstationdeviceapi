const net = require('net');

// Define the port on which the server will listen
const serverPort = 8081;

// Create a new TCP server
const server = net.createServer((socket) => {
    console.log('Client connected');

    // Handle data received from the client
    socket.on('data', (data) => {
        console.log('Received from client:', data.toString());

        // Here you can process the received data as needed

        // Example: Send a response back to the client
        socket.write('Hello, client! Your message was received.');
    });

    // Handle client disconnection
    socket.on('end', () => {
        console.log('Client disconnected');
    });
});

// Start listening on the specified port
server.listen(serverPort, () => {
    console.log(`Server listening on port ${serverPort}`);
});
