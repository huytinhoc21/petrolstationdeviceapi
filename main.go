package main

import (
	"bufio"
	"fmt"
	"net"
)

func main() {
	fmt.Println("????")

	ln, err := net.Listen("tcp", ":8081")
	if err != nil {
		fmt.Println("err", err)
		return
	}
	defer ln.Close()
	for {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println("err", err)
			return
		}
		reader := bufio.NewReader(conn)
		message, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println("err", err)
			return
		}
		fmt.Println("Received:", message)
		// ... process message
		conn.Close()
	}
}
