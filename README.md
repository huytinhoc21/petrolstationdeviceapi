Run the project: ./run.sh

Some useful commands:

- Switch the current user to postgres: sudo -i -u postgres
- Create psql db qlead: createdb qlead
- To connect to qlead database: psql qlead
- To install uuid-ossp extension to use the uuid_generate_v4() function: CREATE EXTENSION IF NOT EXISTS "uuid-ossp"; (after connecting to the desired database)
- List all users in database: \du
- Create a psql user: createuser --interactive
- Change the password of user nguyendangan: ALTER USER nguyendangan WITH PASSWORD 'ahihi'
- Show the current psql connection: \conninfo
- List the database privileges: \l
- Grant all privileges on the psql database: GRANT ALL PRIVILEGES ON DATABASE database_name TO username;
- To dump a psql database: pg_dump -h <your_host> -U <your_username> -p 25060 -Fc <your_database> > <path/to/dump_file.pgsql>
- To import a psql database: sudo -i -u postgres psql -d qlead < <path/to/dump_file.pgsql>
  Swagger:
- Run command: swag init - To update the document
- Run command: swag fmt - To format the swag comments

Auto gen code:

- For tables with ID primary key, run command: ts-node ./new-table src=../BE_Code_Gen/src/base des=./ ntb=NEW_TABLE_NAME
- For tables with composite primary key, run command: ts-node ./new-table src=../BE_Code_Gen/src/base des=./ ntb=NEW_TABLE_NAME tb1=NEW_TABLE1_NAME tb2=NEW_TABLE2_NAME

exmample: ts-node ./new-table ntb=camera
exmample: ts-node ./new-table ntb=product pk2=station_id
exmample: ts-node ./new-table ntb=user_permission tb1=user tb2=permission

For development
Token: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJJRCI6IjFhZjU3OTNmLWQzYmEtNDIyOS1hOTJjLWIzZmI4N2ZiNTVlMyIsIk5hbWUiOiJOZ3V54buFbiDEkMSDbmcgQW4iLCJSb2xlIjoidXNlciIsImV4cCI6MTcwOTQ1MDE2Mywib3JpZ19pYXQiOjE2OTM4OTgxNjN9.ku7DbUVbz87Wm6CVYaJSm7y8BVFnF339BcuEEY05d7U

delete pump_id, staff_id trong staff_shift_infos

UPDATE transactions
SET deleted_at = NOW()
WHERE id NOT IN (
SELECT id
FROM (
SELECT id,
ROW_NUMBER() OVER (PARTITION BY gas_pump_id, created_at ORDER BY id) AS row_num
FROM transactions
) subquery
WHERE row_num = 1
)

UPDATE staff_shifts AS s1
SET starting_volume = s2.prev_ending_volume
FROM (
SELECT id, gas_pump_id, ending_volume,
LAG(ending_volume) OVER (PARTITION BY gas_pump_id ORDER BY id) AS prev_ending_volume
FROM staff_shifts
) AS s2
WHERE s1.id = s2.id;

DELETE FROM staff_shifts
WHERE id IN (
SELECT id
FROM (
SELECT id, gas_pump_id, actual_end_time,
ROW_NUMBER() OVER (PARTITION BY gas_pump_id, actual_end_time ORDER BY id) AS row_num
FROM staff_shifts
) AS subquery
WHERE row_num > 1
);

1a919220-a69c-4784-8972-b433f7a4ae41

DELETE FROM transactions
WHERE id IN (
SELECT id
FROM (
SELECT id,
ROW_NUMBER() OVER (PARTITION BY gas_pump_id, created_at ORDER BY id) AS row_num
FROM transactions) AS subquery
WHERE row_num > 1
);

SELECT \*
from transactions
where gas_pump_id = '3553adf7-ba0d-42c5-9965-7d57d4330db1' and created_at = '2023-12-20 23:12:10+00'

SET TIME ZONE INTERVAL '+7:00' HOUR TO MINUTE;

DELETE FROM transactions
USING gas_pumps
JOIN gas_tanks ON gas_tanks.id = gas_pumps.gas_tank_id
WHERE transactions.gas_pump_id = gas_pumps.id
AND gas_tanks.gas_station_id = '7962b3bf-7916-4256-a4e6-53e93344d87d'
AND transactions.created_at >= '2023-12-26 00:00:00'::timestamp
AND transactions.created_at < '2023-12-27 00:00:00'::timestamp;

select count(\*)
from transactions
join gas_pumps on transactions.gas_pump_id = gas_pumps.id
JOIN gas_tanks ON gas_tanks.id = gas_pumps.gas_tank_id
WHERE gas_tanks.gas_station_id = '7962b3bf-7916-4256-a4e6-53e93344d87d'
AND transactions.created_at >= '2023-12-26 00:00:00'::timestamp
AND transactions.created_at < '2023-12-27 00:00:00'::timestamp;

SELECT
staff_shift_noises.actual_start_time + INTERVAL '7 hours',
staff_shift_noises.actual_end_time + INTERVAL '7 hours',
staff_shift_noises.starting_volume,
staff_shift_noises.ending_volume,
staff_shift_noises.total_revenue,
staff_shift_noises.fuel_volume,
gas_stations.name, gas_pumps.name
FROM staff_shift_noises
JOIN gas_pumps ON gas_pumps.id = CAST(staff_shift_noises.gas_pump_id AS UUID)
join gas_tanks on gas_tanks.id = gas_pumps.gas_tank_id
join gas_stations on gas_stations.id = gas_tanks.gas_station_id
where gas_stations.name = 'Cửa hàng xăng dầu số 28';

SELECT
staff_shift_noises.actual_start_time + INTERVAL '7 hours' as start_time,
staff_shift_noises.actual_end_time + INTERVAL '7 hours' as end_time,
staff_shift_noises.starting_volume,
staff_shift_noises.ending_volume,
staff_shift_noises.total_revenue,
staff_shift_noises.fuel_volume,
staff_shift_noises.record_id,
staff_shift_noises.fuel_volume,
gas_stations.name, gas_pumps.name
FROM staff_shift_noises
JOIN gas_pumps ON gas_pumps.id = CAST(staff_shift_noises.gas_pump_id AS UUID)
join gas_tanks on gas_tanks.id = gas_pumps.gas_tank_id
join gas_stations on gas_stations.id = gas_tanks.gas_station_id
where gas_stations.name = 'Cửa hàng xăng dầu số 27' and actual_end_time >= CURRENT_TIMESTAMP - INTERVAL '48 hours'
order by actual_end_time asc;

sudo journalctl -xeu petro-api --since "48 hour ago" | grep "error"

staff_shift_noises.error,

SELECT
staff_shift_noises.actual_start_time + INTERVAL '7 hours' as start_time,
staff_shift_noises.actual_end_time + INTERVAL '7 hours' as end_time,
staff_shift_noises.starting_volume,
staff_shift_noises.ending_volume,
staff_shift_noises.total_revenue,
staff_shift_noises.fuel_volume,
staff_shift_noises.record_id,
gas_stations.name, gas_pumps.name
FROM staff_shift_noises
JOIN gas_pumps ON gas_pumps.id = CAST(staff_shift_noises.gas_pump_id AS UUID)
join gas_tanks on gas_tanks.id = gas_pumps.gas_tank_id
join gas_stations on gas_stations.id = gas_tanks.gas_station_id
where gas_stations.name = 'Cửa hàng xăng dầu số 28' and actual_end_time >= CURRENT_TIMESTAMP - INTERVAL '24 hours' and gas_pumps.name = '07'
order by actual_end_time;

SELECT
staff_shifts.actual_start_time + INTERVAL '7 hours' as start_time,
staff_shifts.actual_end_time + INTERVAL '7 hours' as end_time,
staff_shifts.starting_volume,
staff_shifts.ending_volume,
staff_shifts.total_revenue,
staff_shifts.fuel_volume,
staff_shifts.record_id,
gas_stations.name, gas_pumps.name
FROM staff_shifts
JOIN gas_pumps ON gas_pumps.id = staff_shifts.gas_pump_id
join gas_tanks on gas_tanks.id = gas_pumps.gas_tank_id
join gas_stations on gas_stations.id = gas_tanks.gas_station_id
where gas_pumps.name = '01' and gas_stations.name = 'Cửa hàng xăng dầu số 28' and actual_end_time >= CURRENT_TIMESTAMP - INTERVAL '24 hours'
order by actual_end_time;
