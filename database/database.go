package database

import (
	"fmt"
	"log"
	"os"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

var DB_URL = getenv("DB_READ", `host=4.194.163.254 port=5432 dbname=petro-station user=nguyendangan password=123123 sslmode=disable`) // another more sercure password: 123123@!<>:)

var (
	db *gorm.DB
	DB *gorm.DB

	gormLogger = logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second,  // Slow SQL threshold
			LogLevel:                  logger.Error, // Log level
			IgnoreRecordNotFoundError: true,         // Ignore ErrRecordNotFound error for logger
			// Colorful:                  false,         // Disable color
		},
	)
)

func GetDb() *gorm.DB {
	if db != nil {
		return db
	}
	db, _ = gorm.Open(postgres.Open(DB_URL), &gorm.Config{
		SkipDefaultTransaction: true, // Dang An: 30%+ performance improvement
		PrepareStmt:            true, // Dang An: PreparedStmt creates a prepared statement when executing any SQL and caches them to speed up future calls
		Logger:                 gormLogger,
	})
	sqlDB, err := db.DB()

	if err != nil {
		log.Print("Can not open the database, Check env DB_URL", DB_URL, err)
		panic("Can not open the database: " + err.Error())
	} else {
		fmt.Println("Successfully connected to PsqlDB!")

	}
	sqlDB.SetConnMaxIdleTime(5 * time.Second)
	sqlDB.SetMaxIdleConns(50)
	// SetMaxOpenConns sets the maximum number of open connections to the database.
	sqlDB.SetMaxOpenConns(450)
	sqlDB.SetConnMaxLifetime(10 * time.Minute)
	DB = db
	return db
}

func Migration() (*gorm.DB, error) {
	GetDb()
	err := db.AutoMigrate(
		&User{},
		&GasStation{},
		&GasPump{},
		&GasTank{},
		&Good{},
		&Customer{},
		&Staff{},
		&Shift{},
		&StaffShift{},
		&StaffShiftInfo{},
		&Provider{},
		&FuelImport{},
		&ApiAction{},
		&Permission{},
		&ApiActionPermission{},
		&UserPermission{},
		&UserGasStation{},
		&Transaction{},
		&StaffShiftNoise{},
		&InvoiceSetting{},
		&Invoice{},
	)
	return db, err
}
