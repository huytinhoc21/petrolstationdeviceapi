package database

import (
	"petrol-station/types"
	"time"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

// Created by: Nguyen Dang An
// Creation date: 31/5/2023
// This file creates psql models for database migration
// Litle infomation:
//GORM prefers convention over configuration.
//By default, GORM uses ID as primary key, pluralizes struct name to snake_cases as table name,
//snake_case as column name, and uses CreatedAt, UpdatedAt to track creating/updating time

type User struct {
	gorm.Model
	ID       uuid.UUID `json:"id" gorm:"type:uuid;default:uuid_generate_v4()" swaggerignore:"true"`
	Name     string    `json:"name" gorm:"not null"`
	Username string    `json:"username" gorm:"not null;unique"`
	Password string    `json:"password" gorm:"not null"`
	Phone    string    `json:"phone"`
	Email    string    `json:"email" gorm:"not null;unique"`
	Role     string    `json:"role" gorm:"not null"`
}

type GasStation struct {
	ID           uuid.UUID      `json:"id" gorm:"type:uuid;default:uuid_generate_v4()" swaggerignore:"true"`
	Name         string         `json:"name" gorm:"not null"`
	Address      string         `json:"address" gorm:"not null"`
	StationChief string         `json:"station_chief"`
	Phone        string         `json:"phone"`
	NumEmployees int            `json:"num_employees" gorm:"not null;default:0"`
	OpeningHours time.Time      `json:"opening_hours"`
	ClosingHours time.Time      `json:"closing_hours"`
	DeletedAt    gorm.DeletedAt `json:"-" gorm:"index" swaggertype:"string"`

	AllowTransactionManualy bool `json:"allow_transaction_manualy"`
}

type GasPump struct {
	ID   uuid.UUID `json:"id" gorm:"type:uuid;default:uuid_generate_v4()" swaggerignore:"true"`
	Name string    `json:"name" gorm:"not null" example:"Vòi số 1"`

	GasTankID *uuid.UUID `json:"gas_tank_id" gorm:"type:uuid"`
	GasTank   GasTank    `json:"-" swaggerignore:"true"`

	DeletedAt  gorm.DeletedAt `json:"-" gorm:"index" swaggertype:"string"`
	UpdatedAt  time.Time      `json:"updated_at"`
	StatusData types.JsonPsql `json:"status_data" gorm:"type:json" swaggertype:"object"`
}

type GasTank struct {
	ID   uuid.UUID `json:"id" gorm:"type:uuid;default:uuid_generate_v4()" swaggerignore:"true"`
	Name string    `json:"name" gorm:"not null"`

	GasStationID *uuid.UUID `json:"gas_station_id" gorm:"type:uuid"`
	GasStation   GasStation `json:"-" swaggerignore:"true"`

	GoodID *uint `json:"good_id"`
	Good   Good  `json:"-" swaggerignore:"true"`

	Capacity uint `json:"capacity" gorm:"not null" example:"1000"`

	DeletedAt gorm.DeletedAt `json:"-" gorm:"index" swaggertype:"string"`

	StatusData types.JsonPsql `json:"status_data" gorm:"type:json" swaggertype:"object"`
}

type Transaction struct {
	ID uuid.UUID `json:"id" gorm:"type:uuid;default:uuid_generate_v4()" swaggerignore:"true"`

	GasPumpID uuid.UUID `json:"gas_pump_id" gorm:"type:uuid;not null;uniqueIndex:gas_pump_id_and_record_id_unique;uniqueIndex:created_gas_pump_unique" example:"87f2b563-0f58-457c-9bd3-5e260ec6e457"`
	GasPump   GasPump   `json:"-" swaggerignore:"true"`

	CustomerID *uint    `json:"customer_id" example:"1"`
	Customer   Customer `json:"-" swaggerignore:"true"`

	StaffID *uuid.UUID `json:"staff_id" example:"87f2b563-0f58-457c-9bd3-5e260ec6e457" gorm:"type:uuid" swaggerignore:"true"`
	Staff   Staff      `json:"-" swaggerignore:"true"`

	FuelVolume   float64 `json:"fuel_volume" gorm:"not null" example:"1.5"`
	UnitPrice    float64 `json:"unit_price" gorm:"not null" example:"20500"`
	TotalRevenue float64 `json:"total_revenue" example:"34500"`

	CreatedAt time.Time `json:"created_at" gorm:"not null;uniqueIndex:created_gas_pump_unique" exmaple:"2023-08-17T15:40:58.131023+07:00"`
	//'Chuyển khoản', 'Tiền mặt'
	PaymentType *string `json:"payment_type" gorm:"not null;check:payment_type IN ('Chuyển khoản', 'Tiền mặt');default:Tiền mặt"`
	//'Chờ chuyển khoản', 'Đã chuyển khoản'
	Status *string `json:"status" gorm:"check:status IN ('Chờ chuyển khoản', 'Đã chuyển khoản', '')"`

	PaymentDate *time.Time `json:"payment_date"`

	DepositID *uint   `json:"deposit_id"`
	Deposit   Deposit `json:"-" swaggerignore:"true"`

	RecordID string `json:"record_id" gorm:"uniqueIndex:gas_pump_id_and_record_id_unique"`

	ShiftID *uint `json:"shift_id" example:"1" swaggerignore:"true"`
	Shift   Shift `json:"-" swaggerignore:"true"`

	StaffShiftID *uint      `json:"staff_shift_id" example:"1" swaggerignore:"true"`
	StaffShift   StaffShift `json:"-" swaggerignore:"true"`

	DeletedAt gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggerignore:"true"`
}

type Good struct {
	ID   uint   `json:"id" gorm:"primaryKey" swaggerignore:"true"`
	Name string `json:"name" gorm:"not null"`
	Unit string `json:"unit" example:"Lít"`

	GasStationID uuid.UUID  `json:"gas_station_id" gorm:"type:uuid"`
	GasStation   GasStation `json:"-" swaggerignore:"true"`

	Description string  `json:"description"`
	Exchange    int     `json:"exchange"`
	Group       string  `json:"group"`
	Vat         float64 `json:"vat"`

	DeletedAt gorm.DeletedAt `json:"-" gorm:"index" swaggerignore:"true"`
}

type Provider struct {
	ID          uuid.UUID `json:"id" gorm:"type:uuid;default:uuid_generate_v4()" swaggerignore:"true"`
	Code        string    `json:"code" gorm:"primaryKey"`
	Name        string    `json:"name" gorm:"not null"`
	Address     string    `json:"address"`
	Phone       string    `json:"phone" gorm:"not null;unique"`
	Email       string    `json:"email" gorm:"not null;unique"`
	Description string    `json:"description"`

	GasStationID uuid.UUID  `json:"gas_station_id" gorm:"type:uuid"`
	GasStation   GasStation `json:"-" swaggerignore:"true"`

	DeletedAt gorm.DeletedAt `json:"-" gorm:"index" swaggerignore:"true"`
}

type Customer struct {
	ID   uint   `json:"id" gorm:"primaryKey" swaggerignore:"true"`
	Code string `json:"code" example:"KH001"`
	Name string `json:"name" gorm:"not null" example:"Nguyễn Cao Thắng"`
	// 'Khách lẻ', 'Khách công nợ', 'Khách nội bộ'
	Type     string  `json:"type" gorm:"not null;check:type IN ('Khách lẻ', 'Khách công nợ', 'Khách nội bộ')" example:"Khách lẻ"`
	DebtNorm uint    `json:"debt_norm" gorm:"not null" example:"200000000"`
	Phone    *string `json:"phone" example:"0394823828"`
	Email    *string `json:"email" example:"ndan@gmail.com"`
	Address  string  `json:"address" example:"Khu phố 6, Đ. Võ Trường Toản, Phường Linh Trung, Thủ Đức, Thành phố Hồ Chí Minh"`

	GasStationID uuid.UUID  `json:"gas_station_id" gorm:"type:uuid;" example:"cbaff033-aa5e-4bda-bb78-208be3a39f0a"`
	GasStation   GasStation `json:"-" swaggerignore:"true"`

	InitialDeposit uint `json:"initial_deposit" example:"200000000"`
	InitialDebt    uint `json:"initial_debt" example:"300000000"`

	DeletedAt gorm.DeletedAt `json:"-" gorm:"index" swaggerignore:"true"`
}

type Staff struct {
	ID       uuid.UUID `json:"id" gorm:"type:uuid;default:uuid_generate_v4()" swaggerignore:"true"`
	Code     string    `json:"code"  example:"NV001"`
	Name     string    `json:"name" gorm:"not null" example:"Nguyễn Văn Hà"`
	Phone    string    `json:"phone" gorm:"not null;unique" example:"0329382939"`
	Email    string    `json:"email" gorm:"not null;unique" example:"hwe@gmail.com"`
	Address  string    `json:"address" example:"Phường Bến Thành, Quận 1, Thành phố Hồ Chí Minh, Việt Nam"`
	Type     string    `json:"type" example:"Khách lẻ"`
	Birthday time.Time `json:"birthday" example:"2023-08-17T15:40:58.131023+07:00"`

	GasStationID uuid.UUID  `json:"gas_station_id" gorm:"type:uuid" example:"5d0f9989-b887-4ccd-8dfd-55773426bb2b"`
	GasStation   GasStation `json:"-" swaggerignore:"true"`

	DeletedAt gorm.DeletedAt `json:"-" gorm:"index" swaggerignore:"true"`
}

type Shift struct {
	ID        uint   `json:"id" gorm:"primaryKey;not null" swaggerignore:"true"`
	Name      string `json:"name" gorm:"not null" example:"Ca 1"`
	StartTime string `json:"start_time" gorm:"not null" example:"08:00"`
	EndTime   string `json:"end_time" gorm:"not null" example:"09:00"`

	GasStationID uuid.UUID  `json:"gas_station_id" gorm:"type:uuid" example:"5d0f9989-b887-4ccd-8dfd-55773426bb2b"`
	GasStation   GasStation `json:"-" swaggerignore:"true"`

	DeletedAt gorm.DeletedAt `json:"-" gorm:"index" swaggerignore:"true"`
}

type StaffShiftInfo struct {
	StaffID uuid.UUID `json:"staff_id" example:"d4602cc9-1b48-48ea-9d94-35c39c992704" gorm:"type:uuid;primaryKey"`
	Staff   Staff     `json:"-" swaggerignore:"true"`

	ShiftID uint  `json:"shift_id" example:"1" gorm:"primaryKey"`
	Shift   Shift `json:"-" swaggerignore:"true"`

	Date string `json:"date" gorm:"primaryKey"`

	Cash types.JsonPsql `json:"cash" gorm:"type:json" swaggertype:"object"`
	Note string         `json:"note"`
}
type StaffShift struct {
	ID uint `json:"id" gorm:"primaryKey" swaggerignore:"true"`

	StaffID uuid.UUID `json:"staff_id" example:"d4602cc9-1b48-48ea-9d94-35c39c992704" gorm:"type:uuid"`
	Staff   Staff     `json:"-" swaggerignore:"true"`

	ShiftID uint  `json:"shift_id" example:"1"`
	Shift   Shift `json:"-" swaggerignore:"true"`

	StartingVolume float64 `json:"starting_volume"`
	EndingVolume   float64 `json:"ending_volume"`

	ActualStartTime time.Time `json:"actual_start_time" example:"2023-08-17T00:00:00+07:00"`
	ActualEndTime   time.Time `json:"actual_end_time" example:"2023-08-17T00:00:00+07:00" gorm:"uniqueIndex:end_time_gas_pump_unique"`

	Date time.Time `json:"date" gorm:"not null;type:date" example:"2023-08-17T00:00:00+07:00"`

	GasPumpID uuid.UUID `json:"gas_pump_id" gorm:"type:uuid;uniqueIndex:record_gas_pump_unique;uniqueIndex:end_time_gas_pump_unique" example:"87f2b563-0f58-457c-9bd3-5e260ec6e457"`
	GasPump   GasPump   `json:"-" swaggerignore:"true"`

	FuelVolume   float64 `json:"fuel_volume"  example:"1.5"`
	UnitPrice    float64 `json:"unit_price" example:"20500"`
	TotalRevenue float64 `json:"total_revenue" example:"34500"`

	PendingAmount   float64        `json:"pending_amount"`
	CompletedAmount float64        `json:"completed_amount"`
	RecordID        string         `json:"record_id" gorm:"uniqueIndex:record_gas_pump_unique"`
	DeletedAt       gorm.DeletedAt `json:"-" gorm:"index" swaggerignore:"true"`
}

type Permission struct {
	ID   uint   `json:"id" gorm:"not null;primaryKey" swaggerignore:"true"`
	Name string `json:"name" gorm:"not null;unique" example:"Nhân viên cân đá"`
}

type ApiAction struct {
	ID   uint   `json:"id" gorm:"not null;primaryKey" swaggerignore:"true"`
	Name string `json:"name" gorm:"not null;unique" example:"Sửa sổ nộp tiền"`
}

type ApiActionPermission struct {
	ApiActionID uint      `json:"api_action_id" gorm:"not null;primaryKey"`
	ApiAction   ApiAction `json:"-" swaggerignore:"true" gorm:"constraint:OnDelete:CASCADE"`

	PermissionID uint       `json:"permission_id" gorm:"not null;primaryKey"`
	Permission   Permission `json:"-" swaggerignore:"true"  gorm:"constraint:OnDelete:CASCADE"`
}

type UserPermission struct {
	UserID       uuid.UUID  `json:"user_id" gorm:"not null;primaryKey" example:"1af5793f-d3ba-4229-a92c-b3fb87fb55e3"`
	User         User       `json:"-" swaggerignore:"true" gorm:"constraint:OnDelete:CASCADE;"`
	PermissionID uint       `json:"permission_id" gorm:"not null;primaryKey" example:"1"`
	Permission   Permission `json:"-" swaggerignore:"true" gorm:"constraint:OnDelete:CASCADE;"`
}

type UserGasStation struct {
	UserID       uuid.UUID  `json:"user_id" gorm:"not null;primaryKey" example:"1af5793f-d3ba-4229-a92c-b3fb87fb55e3"`
	User         User       `json:"-" swaggerignore:"true" gorm:"constraint:OnDelete:CASCADE;"`
	GasStationID uuid.UUID  `json:"gas_station_id" gorm:"not null;primaryKey" example:"1"`
	GasStation   GasStation `json:"-" swaggerignore:"true" gorm:"constraint:OnDelete:CASCADE;"`
}

type Deposit struct {
	ID        uint      `json:"id" gorm:"not null;primaryKey" swaggerignore:"true"`
	CreatedAt time.Time `json:"created_at"`

	CustomerID *uint    `json:"customer_id"`
	Customer   Customer `json:"-" swaggerignore:"true"`

	TransferContent string `json:"transfer_content"`
	Amount          int64  `json:"amount" gorm:"not null"`

	Cashier *uuid.UUID `json:"cashier" gorm:"type:uuid"`
	User    User       `gorm:"foreignKey:Cashier" json:"-" swaggerignore:"true"`

	Status string `json:"status"`
}

type FuelImport struct {
	ID uuid.UUID `json:"id" gorm:"type:uuid;default:uuid_generate_v4()" swaggerignore:"true"`

	Amount float32 `json:"amount" gorm:"not null" example:"1.5"`

	CreatedAt time.Time  `json:"created_at" gorm:"not null" exmaple:"2023-08-17T15:40:58.131023+07:00"`
	GasTankID *uuid.UUID `json:"gas_tank_id" gorm:"type:uuid;not null"`
	GasTank   GasTank    `json:"-" swaggerignore:"true"`
}

type GasPumpRealTimeData struct {
	Disconnected bool      `json:"disconnected"`
	FuelVolume   float32   `json:"fuel_volume" example:"1.5"`
	GasType      string    `json:"gas_type" example:"1.5"`
	StationTime  time.Time `json:"station_time" example:"1.5"`
	Status       uint      `json:"status" example:"Đang bơm"`
	TotalCost    float32   `json:"total_cost" example:"34500"`
	UnitPrice    float32   `json:"unit_price" example:"23000"`
}

type MinBackup struct {
	Arr []Transaction
	Gap float64
}

type StaffShiftNoise struct {
	ID uint `json:"id" gorm:"primaryKey" swaggerignore:"true"`

	StaffID         string    `json:"staff_id" example:"d4602cc9-1b48-48ea-9d94-35c39c992704"`
	ShiftID         uint      `json:"shift_id" example:"1"`
	StartingVolume  float64   `json:"starting_volume"`
	EndingVolume    float64   `json:"ending_volume"`
	ActualStartTime time.Time `json:"actual_start_time" example:"2023-08-17T00:00:00+07:00"`
	ActualEndTime   time.Time `json:"actual_end_time" example:"2023-08-17T00:00:00+07:00"`
	Date            time.Time `json:"date" gorm:"type:date" example:"2023-08-17T00:00:00+07:00"`
	GasPumpID       string    `json:"gas_pump_id" example:"87f2b563-0f58-457c-9bd3-5e260ec6e457"`

	FuelVolume   float64 `json:"fuel_volume"  example:"1.5"`
	UnitPrice    float64 `json:"unit_price" example:"20500"`
	TotalRevenue float64 `json:"total_revenue" example:"34500"`

	PendingAmount   float64 `json:"pending_amount"`
	CompletedAmount float64 `json:"completed_amount"`
	RecordID        string  `json:"record_id"`
	Error           string  `json:"error"`
}

type InvoiceSetting struct {
	ID             uint       `json:"id" gorm:"primaryKey" swaggerignore:"true"`
	SellerName     string     `json:"seller_name"`
	TaxCode        string     `json:"tax_code"`
	GasStationName string     `json:"gas_station_name"`
	Address        string     `json:"address"`
	Phone          string     `json:"phone"`
	Fax            string     `json:"fax"`
	BankID         string     `json:"bank_id"`
	BankName       string     `json:"bank_name"`
	AutoExport     bool       `json:"auto_export"`
	WaitingTime    uint       `json:"waiting_time"`
	GasStationID   uuid.UUID  `json:"gas_station_id" gorm:"type:uuid;unique"`
	GasStation     GasStation `json:"-" swaggerignore:"true"`
}

type Invoice struct {
	ID            uint    `json:"id" gorm:"primaryKey" swaggerignore:"true"`
	CustomerName  string  `json:"customer_name"`
	Unit          string  `json:"unit"`
	Email         string  `json:"email"`
	Address       string  `json:"address"`
	TaxCode       string  `json:"tax_code"`
	VehicleID     string  `json:"vehicle_id"`
	PaymentMethod int     `json:"payment_method"`
	GoodsRevenue  float64 `json:"goods_revenue"` // sum(goods.revenue)
	VatRevenue    float64 `json:"vat_revenue"`   // goods_revenue * vat
	TotalRevenue  float64 `json:"total_revenue"` // goods_revenue + vat_revenue

	TransactionID *uuid.UUID  `json:"transaction_id" gorm:"type:uuid;unique"`
	Transaction   Transaction `json:"-" swaggerignore:"true"`
	CreatedAt     time.Time   `json:"created_at" swaggerignore:"true"`

	Draft bool      `json:"draft"`
	Date  time.Time `json:"date"`

	InvoiceGUID string `json:"invoice_guid" gorm:"unique"`
	InvoiceNo   uint64 `json:"invoice_no"`
	InvoiceMsg  string `json:"invoice_msg"`

	Type      string     `json:"type" gorm:"not null;default:'';check:type IN ('', 'manual', 'transaction-manual', 'transaction-auto')" example:"Khách lẻ"`
	Scheduled *time.Time `json:"scheduled" swaggerignore:"true"`
	Note      string     `json:"note"`
}
