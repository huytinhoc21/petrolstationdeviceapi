package database

import "time"

type BizInfo struct {
}

type BKAVInvoice struct {
	InvoiceTypeID           int       // 1				 //Loại Hoá đơn, tham khảo tại đây
	InvoiceDate             time.Time // "2020-01-31T14:03:45.35617+07:00",  // Ngày Hoá đơn
	BuyerName               string    // "Nguyen Van A",						//Tên người mua
	BuyerTaxCode            string    // "0123456789",				//Mã số Thuế
	BuyerUnitName           string    // "CONG TY ABC",			//Đơn vị mua hàng
	BuyerAddress            string    // "So 632, Duong A, Q. Hai Ba Trung",// Địa chỉ người/đơn vị mua hàng
	BuyerBankAccount        string    //Số tài khoản ngân hàng người mua
	PayMethodID             int       //3,						//Hình thức thanh toán, tham khảo tại đây
	ReceiveTypeID           int       //3,//Hình thức nhận Hoá đơn, 1:Email, 2: SMS, 3: Email & SMS, 4: Chuyển phát nhanh
	ReceiverEmail           string    // "", 					//Email nhận thông báo tra cứu Hoá đơn
	ReceiverMobile          string    // "",					 //Số điện thoại nhận SMS tra cứu Hoá đơn (Nếu không đăng ký gửi hóa đơn qua SMS thì không cần truyền sđt)
	ReceiverAddress         string    // "", 					// Địa chỉ nhận Hoá đơn (Chuyển phát nhanh)
	ReceiverName            string    // "",  					//Tên ngưởi nhận Hoá đơn (Chuyển phát nhanh)
	Note                    string    // "Ghi Chú",					 //Ghi chú trên Hoá đơn
	BillCode                string    // "",					//Số hoá đơn/chứng từ nội bộ trong phần mềm kế toán
	CurrencyID              string    // "VND",					//Đơn vị tiền tệ, mặc định VND
	ExchangeRate            float32   //": 1.0,					//Tỷ giá, mặc định là 1
	InvoiceForm             string    // "1",				//Mẫu số
	InvoiceSerial           string    // "C23TAA",					//Ký hiệu Hoá đơn
	InvoiceNo               int       // 1,						//Số Hoá đơn
	MaCuaCQT                string    // "M1-23-CKSPG-00000000001",    // Mã của CQT từ máy tính tiền nếu client truyền lên, Bkav cấp tự động thì để trống
	CCCD                    string    // "999999999988",                                         // Căn cước công dân của ng mua trên HĐ MTT
	UserDefine              string    // "",					//Thông tin đặc biệt trên Hoá đơn, tham khảo tại đây
	IsBTH                   bool      `json:"isBTH"` // "true"             // true là HĐ không mã gửi theo BTH sang thuế, false là gửi từng lần sang thuế,
	OriginalInvoiceIdentify string    //"[01GTKT0/001]_[AA/17E]_[0000001]"/// Thông tin Hoá đơn gốc dùng trong trường hợp thay thế, điều chỉnh. Định dạng như sau: [Mẫu Số]_[Ký hiệu]_[Số Hoá đơn], ví dụ: [01GTKT0/001]_[AA/17E]_[0000001]
}

type CommandObject100 struct {
	Invoice                 BKAVInvoice
	ListInvoiceDetailsWS    []InvoiceDetailsWS
	ListInvoiceAttachFileWS []string
	PartnerInvoiceID        int64
	PartnerInvoiceStringID  string
}

type CommandObject301 struct {
	PartnerInvoiceStringID string
}

type CommandObject904 string
type CommandObject800 string

type CommandData struct {
	CmdType       int64
	CommandObject interface{}
}
type InvoiceDetailsWS struct {
	ItemName          string  // "Tên mặt hàng",			// Tên hàng hoá dịch vụ
	UnitName          string  // "Cái",					//Đơn vị tính
	Qty               float64 // 10.0,						//Số lượng, kiểu số, mặc định =0
	Price             float64 // 100.0,					//Đơn giá, kiểu số, mặc định =0
	Amount            float64 //1000.0,					//Thành tiền, kiểu số, mặc định =0
	TaxRateID         int     // 3
	TaxRate           float64 // 10.0,
	TaxAmount         float64 // 100.0, 				//Tiền thuế
	OtherAmount       float64 //2000,                                       // Thuế, phí khác theo chi tiết mặt hàng
	DiscountRate      float64 // 0.0,				//Tỷ lệ chiết khấu (xem tại đây)
	DiscountAmount    float64 //"",			//Số tiền chiết khấu (xem tại đây)
	IsDiscount        bool    // false,				// true - chiết khấu, false - Hàng hoá dịch vụ  (xem tại đây)
	UserDefineDetails string  // "",		           // Thông tin đặc biệt trên Hoá đơn (xem tại đây)
	ItemTypeID        int     // 0,				// Loại Item (xem tại đây)
	IsIncrease        *bool
}
