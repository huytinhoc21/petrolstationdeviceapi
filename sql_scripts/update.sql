SELECT
  debt_norm,
  ctm.id as customer_id,
  ctm.name as customer_name,
  (
    select
      sum(amount)
    from
      deposits
    where
      deposits.created_at < '2024-04-13T17:00:00.000Z'
      and deposits.customer_id = ctm.id
  ) + COALESCE(ctm.initial_deposit, 0) as accumulated_deposit,
  (
    select
      sum(total_revenue)
    from
      transactions
    where
      transactions.created_at < '2024-04-13T17:00:00.000Z'
      and transactions.customer_id = ctm.id
      and payment_type = 'Chuyển khoản'
  ) + COALESCE(ctm.initial_debt, 0) as accumulated_amount_spent,
  (
    select
      sum(amount)
    from
      deposits
    where
      deposits.created_at between '2024-04-13T17:00:00.000Z' and '2024-04-16T16:59:59.999Z'
      and deposits.customer_id = ctm.id
  ) as deposit,
  (
    select
      sum(total_revenue)
    from
      transactions
    where
      transactions.created_at between '2024-04-13T17:00:00.000Z' and '2024-04-16T16:59:59.999Z'
      and transactions.customer_id = ctm.id
      and payment_type = 'Chuyển khoản'
  ) as amount_spent
FROM
  customers as ctm
WHERE
  ctm.gas_station_id = '6c69964a-0b98-4ccb-a7d4-6f2a0d681e3e'
GROUP BY
  "ctm"."id"