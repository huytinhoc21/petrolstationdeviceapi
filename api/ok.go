package api

import (
	"github.com/gin-gonic/gin"
)

// Register godoc
//
//	@Summary		User register [OK]
//	@Description	Register a new user
//	@Tags			Users
//	@Accept			json
//	@Produce		json
//	@Param			Info	body		model.ReqRegister	true	"Register information"
//	@Success		200		{object}	model.ResRegister
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/users [post]
func Ok(router *gin.RouterGroup) {
	router.POST("/", func(c *gin.Context) {

	})
}
