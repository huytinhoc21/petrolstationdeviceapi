package api

import (
	"petrol-station/database"
	"petrol-station/model"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Get user_permissions godoc
//
//	@Summary		Get a list of user_permissions [OK]
//	@Description	Return a list of user_permissions by giving a filter
//	@Tags			UserPermissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResUserPermission
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/user_permissions [get]
func getUserPermissions(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		var user_permissions []model.ResUserPermission
		// Get all records
		result := db.Table("user_permissions").Find(&user_permissions)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, user_permissions)

	})
}

// Get a user_permission godoc
//
//	@Summary		Get a user_permission [OK]
//	@Description	Return a user_permission if successful
//	@Tags			UserPermissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			user-id			path		string	true	"User id"
//	@Param			permission-id	path		string	true	"Permission id"
//	@Success		200				{object}	model.ResUserPermission
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/user_permissions/{user-id}/{permission-id} [get]
func getAUserPermission(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:user-id/:permission-id", func(c *gin.Context) {

		userID := c.Param("user-id")
		permissionID := c.Param("permission-id")

		var user_permission model.ResUserPermission
		// Get all records
		result := db.Table("user_permissions").Where("user_id = ?", userID).Where("permission_id = ?", permissionID).First(&user_permission)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, user_permission)

	})
}

// Get a user_permission godoc
//
//	@Summary		Get a user_permission [OK]
//	@Description	Return a user_permission if successful
//	@Tags			UserPermissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			user-id	path		string	true	"User id"
//	@Success		200		{object}	model.ResUserPermission
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/user_permissions/{user-id} [get]
func getAUserPermissionByUserID(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:user-id", func(c *gin.Context) {

		userID := c.Param("user-id")

		var user_permission []model.ResUserPermission

		// Get all records
		result := db.Table("user_permissions").Where("user_id = ?", userID).Find(&user_permission)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, user_permission)

	})
}

// Get a user_permission godoc
//
//	@Summary		Get a user_permission [OK]
//	@Description	Return a user_permission if successful
//	@Tags			UserPermissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			permission-id	path		string	true	"Permission id"
//	@Success		200				{object}	model.ResUserPermission
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/user_permissions/all/{permission-id} [get]
func getAUserPermissionByPermissionID(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/all/:permission-id", func(c *gin.Context) {

		permissionID := c.Param("permission-id")

		var user_permission []model.ResUserPermission

		// Get all records
		result := db.Table("user_permissions").Where("permission_id = ?", permissionID).Find(&user_permission)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, user_permission)

	})
}

// Post user_permissions godoc
//
//	@Summary		Post user_permissions to database [OK]
//	@Description	Return a list of user_permissions if successful
//	@Tags			UserPermissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			UserPermission	body		[]model.ReqUserPermission	true	"New user_permissions data"
//	@Success		200				{array}		model.ReqUserPermission		"Inserted data"
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/user_permissions [post]
func postUserPermission(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {

		req := []model.ReqUserPermission{}
		res := []model.ReqUserPermission{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		// if err := CheckInputError(req); err != nil {
		// 	returnError(c, 400, err.Error())
		// 	return
		// }

		tx := db.Begin()

		for _, v := range req {
			result := tx.Table("user_permissions").Where(database.UserPermission{UserID: v.UserID, PermissionID: v.PermissionID}).Attrs(database.UserPermission{}).FirstOrCreate(&v)
			if result.Error != nil {
				tx.Rollback()
				returnError(c, 400, result.Error.Error())
				return
			}
			res = append(res, v)
		}

		tx.Commit()

		c.JSON(200, res)

	})
}

// Update a user_permission godoc
//
//	@Summary		Update a user_permission [OK]
//	@Description	Returns updated rows
//	@Tags			UserPermissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			user-id			path		string							true	"User id"
//	@Param			permission-id	path		string							true	"Permission id"
//	@Param			UserPermission	body		model.ReqPatchUserPermission	true	"UserPermission data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200				{integer}	uint							"The number of updated rows"
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/user_permissions/{user-id}/{permission-id} [patch]
func updateAUserPermission(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:user-id/:permission-id", func(c *gin.Context) {
		userID := c.Param("user-id")
		permissionID := c.Param("permission-id")

		req := model.ReqPatchUserPermission{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("user_permissions").Where("user_id = ?", userID).Where("permission_id = ?", permissionID).Updates(req)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Delete a user_permission godoc
//
//	@Summary		Delete a user_permission [OK]
//	@Description	Returns deleted rows
//	@Tags			UserPermissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			user-id			path		string	true	"User id"
//	@Param			permission-id	path		string	true	"Permission id"
//	@Success		200				{integer}	uint	"The number of deleted rows"
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/user_permissions/{user-id}/{permission-id} [delete]
func deleteAUserPermission(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:user-id/:permission-id", func(c *gin.Context) {
		userID := c.Param("user-id")
		permissionID := c.Param("permission-id")

		result := db.Table("user_permissions").Where("user_id = ?", userID).Where("permission_id = ?", permissionID).Delete(&database.UserPermission{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}
