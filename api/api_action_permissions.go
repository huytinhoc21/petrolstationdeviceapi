package api

import (
	"petrol-station/database"
	"petrol-station/model"
	"strconv"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Get api_action_permissions godoc
//
//	@Summary		Get a list of api_action_permissions [OK]
//	@Description	Return a list of api_action_permissions by giving a filter
//	@Tags			ApiActionPermissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResApiActionPermission
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/api_action_permissions [get]
func getApiActionPermissions(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		var api_action_permissions []model.ResApiActionPermission
		// Get all records
		result := db.Table("api_action_permissions").
			Select("api_action_permissions.*, api_actions.name as api_action_name").
			Joins("join api_actions on api_action_permissions.api_action_id = api_actions.id").
			Find(&api_action_permissions)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, api_action_permissions)

	})
}

// Get a api_action_permission godoc
//
//	@Summary		Get a api_action_permission [OK]
//	@Description	Return a api_action_permission if successful
//	@Tags			ApiActionPermissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			api_action-id	path		string	true	"ApiAction id"
//	@Param			permission-id	path		string	true	"Permission id"
//	@Success		200				{object}	model.ResApiActionPermission
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/api_action_permissions/{api_action-id}/{permission-id} [get]
func getAApiActionPermission(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:api_action-id/:permission-id", func(c *gin.Context) {

		api_actionID := c.Param("api_action-id")
		permissionID := c.Param("permission-id")

		var api_action_permission model.ResApiActionPermission
		// Get all records
		result := db.Table("api_action_permissions").Where("api_action_id = ?", api_actionID).Where("permission_id = ?", permissionID).First(&api_action_permission)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, api_action_permission)

	})
}

// Get a api_action_permission godoc
//
//	@Summary		Get a api_action_permission [OK]
//	@Description	Return a api_action_permission if successful
//	@Tags			ApiActionPermissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			api_action-id	path		string	true	"ApiAction id"
//	@Success		200				{object}	model.ResApiActionPermission
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/api_action_permissions/{api_action-id} [get]
func getAApiActionPermissionByApiActionID(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:api_action-id", func(c *gin.Context) {

		api_actionID := c.Param("api_action-id")

		var api_action_permission []model.ResApiActionPermission

		// Get all records
		result := db.Table("api_action_permissions").Where("api_action_id = ?", api_actionID).Find(&api_action_permission)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, api_action_permission)

	})
}

// Get a api_action_permission godoc
//
//	@Summary		Get a api_action_permission [OK]
//	@Description	Return a api_action_permission if successful
//	@Tags			ApiActionPermissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			permission-id	path		string	true	"Permission id"
//	@Success		200				{object}	model.ResApiActionPermission
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/api_action_permissions/all/{permission-id} [get]
func getAApiActionPermissionByPermissionID(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/all/:permission-id", func(c *gin.Context) {

		permissionID := c.Param("permission-id")

		var api_action_permission []model.ResApiActionPermission

		// Get all records
		result := db.Table("api_action_permissions").Where("permission_id = ?", permissionID).Find(&api_action_permission)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, api_action_permission)

	})
}

// Post api_action_permissions godoc
//
//	@Summary		Post api_action_permissions to database [OK]
//	@Description	Return a list of api_action_permissions if successful
//	@Tags			ApiActionPermissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			ApiActionPermission	body		[]model.ReqApiActionPermission	true	"New api_action_permissions data"
//	@Success		200					{array}		model.ReqApiActionPermission	"Inserted data"
//	@Failure		400					{object}	model.HTTPError400
//	@Router			/api_action_permissions [post]
func postApiActionPermission(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {

		req := []model.ReqApiActionPermission{}
		res := []model.ReqApiActionPermission{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		// if err := CheckInputError(req); err != nil {
		// 	returnError(c, 400, err.Error())
		// 	return
		// }

		tx := db.Begin()

		for _, v := range req {
			result := tx.Table("api_action_permissions").Where(database.ApiActionPermission{ApiActionID: v.ApiActionID, PermissionID: v.PermissionID}).Attrs(database.ApiActionPermission{}).FirstOrCreate(&v)
			if result.Error != nil {
				tx.Rollback()
				returnError(c, 400, result.Error.Error())
				return
			}
			res = append(res, v)
		}

		tx.Commit()

		c.JSON(200, res)

	})
}

// Update a api_action_permission godoc
//
//	@Summary		Update a api_action_permission [OK]
//	@Description	Returns updated rows
//	@Tags			ApiActionPermissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id					path		string								true	"ApiActionPermission id"
//	@Param			ApiActionPermission	body		model.ReqPatchApiActionPermission	true	"ApiActionPermission data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200					{integer}	uint								"The number of updated rows"
//	@Failure		400					{object}	model.HTTPError400
//	@Router			/api_action_permissions/{api_action-id}/{permission-id} [patch]
func updateAApiActionPermission(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:api_action-id/:permission-id", func(c *gin.Context) {
		api_actionID := c.Param("api_action-id")
		permissionID := c.Param("permission-id")

		req := model.ReqPatchApiActionPermission{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("api_action_permissions").Where("api_action_id = ?", api_actionID).Where("permission_id = ?", permissionID).Updates(req)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Update api_action_permission by api_action godoc
//
//	@Summary		Update a api_action_permission [OK]
//	@Description	Returns updated rows
//	@Tags			ApiActionPermissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id					path		string								true	"ApiActionPermission id"
//	@Param			ApiActionPermission	body		model.ReqPatchApiActionPermission	true	"ApiActionPermission data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200					{integer}	uint								"The number of updated rows"
//	@Failure		400					{object}	model.HTTPError400
//	@Router			/api_action_permissions/{api_action-id}/{permission-id} [put]
func updateApiActionPermissionByApiAction(router *gin.RouterGroup, db *gorm.DB) {
	router.PUT("/all/:permission-id", func(c *gin.Context) {
		permissionID, _ := strconv.ParseUint(c.Param("permission-id"), 10, 0)

		req := model.ReqPatchApiActionPermissionByApiAction{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		tx := db.Begin()

		err := tx.Exec(`delete from api_action_permissions where permission_id = ?`, uint(permissionID)).Error
		if err != nil {
			tx.Rollback()
			returnError(c, 400, err.Error())
			return
		}

		apiActionPermissions := []database.ApiActionPermission{}
		for _, v := range req.ApiActionIDs {
			apiActionPermissions = append(apiActionPermissions, database.ApiActionPermission{
				ApiActionID:  v,
				PermissionID: uint(permissionID),
			})
		}
		result := tx.CreateInBatches(apiActionPermissions, len(apiActionPermissions))

		if result.Error != nil {
			tx.Rollback()
			returnError(c, 400, result.Error.Error())
			return
		}
		tx.Commit()
		c.JSON(200, result.RowsAffected)
	})
}

// Delete a api_action_permission godoc
//
//	@Summary		Delete a api_action_permission [OK]
//	@Description	Returns deleted rows
//	@Tags			ApiActionPermissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			api_action-id	path		string	true	"ApiAction id"
//	@Param			permission-id	path		string	true	"Permission id"
//	@Success		200				{integer}	uint	"The number of deleted rows"
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/api_action_permissions/{api_action-id}/{permission-id} [delete]
func deleteAApiActionPermission(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:api_action-id/:permission-id", func(c *gin.Context) {
		api_actionID := c.Param("api_action-id")
		permissionID := c.Param("permission-id")

		result := db.Table("api_action_permissions").Where("api_action_id = ?", api_actionID).Where("permission_id = ?", permissionID).Delete(&database.ApiActionPermission{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}
