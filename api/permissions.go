package api

import (
	"petrol-station/database"
	"petrol-station/model"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Get permissions godoc
//
//	@Summary		Get a list of permissions [OK]
//	@Description	Return a list of permissions by giving a filter
//	@Tags			Permissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResPermission
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/permissions [get]
func getPermissions(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		var permissions []model.ResPermission
		// Get all records
		result := db.Table("permissions").Find(&permissions)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, permissions)

	})
}

// Get a permission godoc
//
//	@Summary		Get a permission [OK]
//	@Description	Return a permission if successful
//	@Tags			Permissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Permission id"
//	@Success		200	{object}	model.ResPermission
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/permissions/{id} [get]
func getAPermission(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:id", func(c *gin.Context) {

		id := c.Param("id")
		var permission model.ResPermission
		// Get all records
		result := db.Table("permissions").First(&permission, "id = ?", id)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, permission)

	})
}

// Post a permission godoc
//
//	@Summary		Post a permission to database [OK]
//	@Description	Return a id if successful
//	@Tags			Permissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			Permission	body		model.ReqPermission	true	"New permission data"
//	@Success		200			{string}	string				"Inserted Unit ID"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/permissions [post]
func postAPermission(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {
		req := model.ReqPermission{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		result := db.Table("permissions").Create(&req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, req.ID)

	})
}

// Update a permission godoc
//
//	@Summary		Update a permission [OK]
//	@Description	Returns updated rows
//	@Tags			Permissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id			path		string						true	"Permission id"
//	@Param			Permission	body		model.ReqPatchPermission	true	"Permission data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200			{integer}	uint						"The number of updated rows"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/permissions/{id} [patch]
func updateAPermission(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqPatchPermission{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("permissions").Where("id = ?", id).Updates(req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Delete a permission godoc
//
//	@Summary		Delete a permission [OK]
//	@Description	Returns deleted rows
//	@Tags			Permissions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Permission id"
//	@Success		200	{integer}	uint	"The number of deleted rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/permissions/{id} [delete]
func deleteAPermission(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		result := db.Where("id = ?", id).Delete(&database.Permission{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}
