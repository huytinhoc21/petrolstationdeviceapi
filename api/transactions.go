package api

import (
	"fmt"
	"math"
	"os"
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/types"
	"petrol-station/util"
	"time"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"
	"github.com/sirupsen/logrus"
)

// Get transactions godoc
//
//	@Summary		Get a list of transactions [OK]
//	@Description	Return a list of transactions by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			Transactions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			gas_station_id	query		string	false	"Filter by gas station id"
//	@Param			from			query		string	true	"Statistics start date. Dates provided must be in the following format: 2006-01-02 15:04:05-07:00 (psql timestamp format, the ISO 8601 format)"
//	@Param			to				query		string	true	"Statistics end date. Dates provided must be in the following format: "2006-01-02	15:04:05-07:00	(psql timestamp format, the ISO 8601 format)"
//	@Param			deposit_id		query		string	false	"Statistics end date. Dates provided must be in the following format: "2006-01-02	15:04:05-07:00	(psql timestamp format, the ISO 8601 format)"
//	@Param			good_id			query		string	false	"Good id"
//	@Success		200				{array}		model.ResTransaction
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/transactions [get]
func getTransactions(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		rowData := database.Transaction{}
		result := db.Table("transactions")

		deleted := c.Query("deleted")
		if deleted == "true" {
			result = result.Unscoped().Debug().
				Where(`transactions.deleted_at is null or (transactions.deleted_at is not null and 
					exists (select 1 from invoices where transaction_id = transactions.id))`)
		}

		from := c.Query("from")
		to := c.Query("to")
		depositIDs := c.QueryArray("deposit_id")
		goodID := c.Query("good_id")
		if from != "" && util.IsPostgreSQLDateFormat(from) {
			result = result.Where("transactions.created_at >= ?", from)
		}
		gasStationID := c.Query("gas_station_id")
		if gasStationID != "" {
			result = result.Where("gas_tanks.gas_station_id = ?", gasStationID)
		}

		if len(depositIDs) > 0 {

			depositIDsWithoutNull := make([]string, 0, len(depositIDs))
			for _, element := range depositIDs {
				if element != "null" {
					depositIDsWithoutNull = append(depositIDsWithoutNull, element)
				}
			}

			if len(depositIDsWithoutNull) < len(depositIDs) {
				util.RemoveQueryParameter(c, "deposit_id")
				if len(depositIDsWithoutNull) > 0 {
					result = result.Where("deposit_id in (?) or deposit_id is null", depositIDsWithoutNull)
				} else {
					result = result.Where("deposit_id is null")

				}
			} else {
				util.SetQueryArray(c, "deposit_id", depositIDsWithoutNull)
			}

		}

		if to != "" && util.IsPostgreSQLDateFormat(to) {
			result = result.Where("transactions.created_at <= ?", to)
		}

		if goodID != "" {
			result = result.Where("good_id = ?", goodID)
		}

		result = util.GetFilteredDB(result, c, rowData)

		var transactions []model.ResTransaction
		// Get all records

		result = result.
			Select(`transactions.*, 
			json_build_object(
				'id', customers.id, 'name', customers.name, 
				'type', customers.type, 'code', customers.code
				) as customer, 
			json_build_object('id', staffs.id, 'name', staffs.name, 'type', staffs.type) as staff, 
			json_build_object('id', gas_pumps.id, 'name', gas_pumps.name) as gas_pump, 
			json_build_object('id', goods.id, 'name', goods.name) as good,
			gas_stations.name as gas_station_name,
			gas_tanks.gas_station_id as gas_station_id,
			row_to_json(invoices) as invoice
			`).
			Joins("left join customers on transactions.customer_id = customers.id").
			Joins("left join staffs on transactions.staff_id = staffs.id").
			Joins("left join gas_pumps on transactions.gas_pump_id = gas_pumps.id").
			Joins("left join gas_tanks on gas_pumps.gas_tank_id = gas_tanks.id").
			Joins("left join goods on gas_tanks.good_id = goods.id").
			Joins("left join invoices on invoices.transaction_id = transactions.id").
			Joins("join gas_stations on gas_tanks.gas_station_id = gas_stations.id and gas_stations.deleted_at is null").
			Group("invoices.id, transactions.id, customers.id, staffs.id, gas_pumps.id, goods.id, gas_stations.id, gas_tanks.gas_station_id").
			Order("transactions.created_at desc").
			Find(&transactions)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, transactions)

	})
}

// Get transaction statistics data godoc
//
//	@Summary		Get transaction statistics data [OK]
//	@Description	Return a object of transaction statistics data
//	@Tags			Transactions
//	@Accept			json
//	@Produce		json
//	@Param			from		query	string	true	"Statistics start date. Dates provided must be in the following format: 2006-01-02 15:04:05-07:00 (psql timestamp format, the ISO 8601 format)"
//	@Param			to			query	string	true	"Statistics end date. Dates provided must be in the following format: "2006-01-02	15:04:05-07:00	(psql timestamp format, the ISO 8601 format)"
//	@Param			staff-id	query	string	true	"Staff ID"
//	@Security		ApiKeyAuth
//	@Success		200	{object}	util.TransactionStatisticsData
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/transactions/statistics [get]
func getTransactionStatisticsData(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/statistics", func(c *gin.Context) {

		from := c.Query("from")
		to := c.Query("to")
		staffID := c.Query("staff-id")
		res := util.AnalyzeTransactionData(from, to, staffID)

		if res == nil {
			returnError(c, 400, "Ngày không đúng format")
			return
		}

		c.JSON(200, res)

	})
}

// Get a transaction godoc
//
//	@Summary		Get a transaction [OK]
//	@Description	Return a transaction if successful
//	@Tags			Transactions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Transaction id"
//	@Success		200	{object}	model.ResTransaction
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/transactions/{id} [get]
func getATransaction(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:id", func(c *gin.Context) {

		id := c.Param("id")
		var transaction model.ResTransaction
		// Get all records
		result := db.Table("transactions").First(&transaction, "id = ?", id)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, transaction)

	})
}

// Post a transaction godoc
//
//	@Summary		Post a transaction to database [OK]
//	@Description	Return a id if successful
//	@Tags			Transactions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			Transaction	body		model.ReqTransaction	true	"New transaction data"
//	@Success		200			{string}	string					"Inserted Unit ID"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/transactions [post]
func postATransaction(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {
		fmt.Println("POST TX!!!!!!!!!!!!!!!!!!!!!!!!!!!")

		req := model.ReqTransaction{}

		if err1 := c.Bind(&req); err1 != nil {
			file, err := os.OpenFile("error.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
			if err != nil {
				fmt.Println("Failed to open log file:", err)
			}
			logger := logrus.New()
			logger.SetOutput(file)

			logger.Error("Binding error -" + err1.Error())
			defer file.Close()
			returnError(c, 400, err1.Error())
			return
		}
		if err1 := CheckInputError(req); err1 != nil {
			file, err := os.OpenFile("error.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
			if err != nil {
				fmt.Println("Failed to open log file:", err)
			}
			logger := logrus.New()
			logger.SetOutput(file)

			logger.Error("Checkinput error -" + err1.Error())
			logger.Info("GasPumpID: ", req.GasPumpID)
			logger.Info("CreatedAt: ", req.CreatedAt)
			logger.Info("FuelVolume: ", req.FuelVolume)
			logger.Info("TotalRevenue: ", req.TotalRevenue)
			logger.Info("UnitPrice: ", req.UnitPrice)
			logger.Info("RecordID: ", req.RecordID)
			defer file.Close()
			returnError(c, 400, err1.Error())
			return
		}

		// tmpID := struct {
		// 	ShiftID *uint     `json:"shift_id"`
		// 	StaffID uuid.UUID `json:"staff_id"`
		// }{}

		// result := db.Table("gas_pumps").
		// 	Select("staff_shifts.staff_id as staff_id, shifts.id as shift_id ").
		// 	Joins("join staff_shifts on staff_shifts.gas_pump_id = gas_pumps.id").
		// 	Joins("join shifts on shifts.id = staff_shifts.shift_id").
		// 	Where(`gas_pumps.id = ? and (CURRENT_DATE = DATE(staff_shifts.date)) and
		// 	(CURRENT_TIME::time BETWEEN shifts.start_time::time AND shifts.end_time::time)`, req.GasPumpID).Scan(&tmpID)

		// if result.Error != nil {
		// 	returnError(c, 400, result.Error.Error())
		// 	return
		// }

		// if tmpID.StaffID != uuid.Nil {
		// 	req.StaffID = &tmpID.StaffID
		// }

		// // if tmpID.ShiftID != nil {
		// req.ShiftID = tmpID.ShiftID
		// // }

		// var count int64

		// result := db.Table("transactions").
		// 	Where(`created_at = ? and gas_pump_id = ?`, req.CreatedAt, req.GasPumpID).Count(&count)

		// if result.Error != nil {
		// 	returnError(c, 400, result.Error.Error())
		// 	return
		// }
		// fmt.Println(count)
		// if count > 0 {
		// 	req.DeletedAt = gorm.DeletedAt{Time: time.Now(), Valid: true}
		// }

		// if strings.Contains(req.GasType, "_") {
		// 	req.RecordID = "68-" + req.RecordID
		// 	// file, err := os.OpenFile("error.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
		// 	// if err != nil {
		// 	// 	fmt.Println("Failed to open log file:", err)
		// 	// }
		// 	// logger := logrus.New()
		// 	// logger.SetOutput(file)
		// 	// defer file.Close()

		// 	// logger.Info("68 INFOR :))): ", req.GasPumpID)
		// 	// logger.Info("GasPumpID: ", req.GasPumpID)
		// 	// logger.Info("CreatedAt: ", req.CreatedAt)
		// 	// logger.Info("FuelVolume: ", req.FuelVolume)
		// 	// logger.Info("TotalRevenue: ", req.TotalRevenue)
		// 	// logger.Info("UnitPrice: ", req.UnitPrice)
		// 	// logger.Info("RecordID: ", req.RecordID)
		// }

		if math.Abs(float64(req.FuelVolume)*float64(req.UnitPrice)-req.TotalRevenue) > float64(10000) {
			req.DeletedAt = gorm.DeletedAt{Time: time.Now(), Valid: true}
		}
		if time.Now().Add(5*time.Minute).Compare(req.CreatedAt) == -1 {
			req.DeletedAt = gorm.DeletedAt{Time: time.Now(), Valid: true}
		}
		if req.TotalRevenue <= 0 {
			req.DeletedAt = gorm.DeletedAt{Time: time.Now(), Valid: true}
		}

		result := db.Table("transactions").Unscoped().Where(database.Transaction{GasPumpID: req.GasPumpID, CreatedAt: req.CreatedAt}).FirstOrCreate(&req)
		if result.Error != nil {
			file, err := os.OpenFile("error.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
			if err != nil {
				fmt.Println("Failed to open log file:", err)
			}
			logger := logrus.New()
			logger.SetOutput(file)
			defer file.Close()
			logger.Error("Add transaction error -" + result.Error.Error())
			logger.Info("GasPumpID: ", req.GasPumpID)
			logger.Info("CreatedAt: ", req.CreatedAt)
			logger.Info("FuelVolume: ", req.FuelVolume)
			logger.Info("TotalRevenue: ", req.TotalRevenue)
			logger.Info("UnitPrice: ", req.UnitPrice)
			logger.Info("RecordID: ", req.RecordID)

			returnError(c, 400, result.Error.Error())
			return
		}

		// if !req.DeletedAt.Valid {
		// 	// util.CreateOrUpdateBkavInvoice(req.Transaction, db)
		// 	go func() {
		// 		for i := 0; i < 10; i++ {
		// 			_, err := util.CreateInvoiceFromTransaction(req.Transaction, false, false, db)
		// 			if err == nil {
		// 				break
		// 			}
		// 			time.Sleep(5 * time.Second)
		// 		}
		// 	}()
		// }

		c.JSON(200, req.ID)
	})
}

// Post a transaction godoc
//
//	@Summary		Post a transaction to database [OK]
//	@Description	Return a id if successful
//	@Tags			Transactions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			Transaction	body		[]model.ReqTransaction	true	"New transaction data"
//	@Success		200			{integer}	uint					"Row affected"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/transactions/many [post]
func postTransactions(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("/many", func(c *gin.Context) {
		req := []model.ReqTransaction{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		var rowsAffected int64

		for _, tx := range req {
			result := db.Table("transactions").Create(&tx)
			rowsAffected += result.RowsAffected
		}

		c.JSON(200, rowsAffected)
	})
}

// Update a transaction godoc
//
//	@Summary		Update a transaction [OK]
//	@Description	Returns updated rows
//	@Tags			Transactions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id			path		string						true	"Transaction id"
//	@Param			Transaction	body		model.ReqPatchTransaction	true	"Transaction data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200			{integer}	uint						"The number of updated rows"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/transactions/{id} [patch]
func updateATransaction(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqPatchTransaction{}
		c.Bind(&req)
		if req.CustomerID != nil {
			paymentType := "Chuyển khoản"
			req.PaymentType = &paymentType
		}

		result := db.Table("transactions").Where("id = ?", id).Updates(&req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		go func() {
			transaction := database.Transaction{}
			db.Table("transactions").First(&transaction, "id = ?", id)
			if transaction.StaffShiftID != nil && *transaction.StaffShiftID != 0 {
				util.CreateInvoiceFromTransaction(transaction, true, false, db)
			}
		}()

		c.JSON(200, result.RowsAffected)
	})
}

// Create invoice from a transaction godoc
//
//	@Summary		Create invoice from a transaction [OK]
//	@Description	Returns created invoice
//	@Tags			Transactions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id			path		string								true	"Transaction id"
//	@Param			Transaction	body		model.ReqCreateTransactionInvoice	true	"Options to create invoice"
//	@Success		200			{integer}	database.Invoice					"The number of updated rows"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/transactions/{id}/create-invoice [patch]
func createATransactionInvoice(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("/:id/create-invoice", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqCreateTransactionInvoice{}
		c.Bind(&req)

		transaction := database.Transaction{}
		result := db.First(&transaction, "id = ?", id)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		invoice, err := util.CreateInvoiceFromTransaction(transaction, false, req.UseCurrentDate, db)
		if invoice == nil {
			returnError(c, 400, err.Error())
			return
		}
		c.JSON(200, invoice)
	})
}

// Get transactions by pump godoc
//
//	@Summary		Get a list of transactions by pump [OK]
//	@Description	Return a list of transactions by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			Transactions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			gas_station_id	query		string	false	"Filter by gas station id"
//	@Param			from			query		string	false	"Statistics start date. Dates provided must be in the following format: 2006-01-02 15:04:05-07:00 (psql timestamp format, the ISO 8601 format)"
//	@Param			to				query		string	false	"Statistics end date. Dates provided must be in the following format: "2006-01-02	15:04:05-07:00	(psql timestamp format, the ISO 8601 format)"
//	@Success		200				{array}		model.ResTransactionByPump
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/transactions/by-pump [get]
func getTransactionsByPump(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/by-pump", func(c *gin.Context) {

		rowData := database.Transaction{}
		result := db.Table("transactions")
		from := c.Query("from")
		to := c.Query("to")
		goodID := c.Query("good_id")
		if from != "" && util.IsPostgreSQLDateFormat(from) {
			result = result.Where("created_at >= ?", from)
		}

		if to != "" && util.IsPostgreSQLDateFormat(to) {
			result = result.Where("created_at <= ?", to)
		}

		if goodID != "" {
			result = result.Where("good_id = ?", goodID)
		}
		gasStationID := c.Query("gas_station_id")
		if gasStationID != "" {
			result = result.Where("gas_tanks.gas_station_id = ?", gasStationID)
		}

		result = util.GetFilteredDB(result, c, rowData)

		var res []model.ResTransactionByPump
		// Get all records

		result = result.
			Select(`json_build_object('id', gas_pumps.id, 'name', gas_pumps.name) as gas_pump, 
			json_build_object('id', goods.id, 'name', goods.name) as good,
			sum(transactions.total_revenue) as total_revenue,
			sum(transactions.fuel_volume) as fuel_volume,
			sum(CASE 
					WHEN transactions.payment_type = 'Chuyển khoản' and transactions.status = 'Chờ chuyển khoản' 
					THEN transactions.total_revenue
					ELSE 0
			END) as pending_amount,
			sum(CASE 
					WHEN transactions.payment_type = 'Tiền mặt'
					THEN transactions.total_revenue
					ELSE 0
			END) as cash,
			  sum(CASE 
					WHEN transactions.payment_type = 'Chuyển khoản' and transactions.status = 'Đã chuyển khoản' 
					THEN transactions.total_revenue
					ELSE 0
			END) as completed_amount`).
			Joins("left join gas_pumps on transactions.gas_pump_id = gas_pumps.id").
			Joins("left join gas_tanks on gas_pumps.gas_tank_id = gas_tanks.id").
			Joins("left join goods on gas_tanks.good_id = goods.id").
			Group("gas_pumps.id, goods.id").
			Find(&res)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, res)

	})
}

// Get transactions by customer godoc
//
//	@Summary		Get a list of transactions by customer [OK]
//	@Description	Return a list of transactions by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			Transactions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			from	query		string	false	"Statistics start date. Dates provided must be in the following format: 2006-01-02 15:04:05-07:00 (psql timestamp format, the ISO 8601 format)"
//	@Param			to		query		string	false	"Statistics end date. Dates provided must be in the following format: "2006-01-02	15:04:05-07:00	(psql timestamp format, the ISO 8601 format)"
//	@Success		200		{array}		model.ResTransactionByPump
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/transactions/by-station [get]
func getTransactionsByCustomer(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/by-station", func(c *gin.Context) {

		rowData := database.Transaction{}
		result := db.Table("transactions")
		from := c.Query("from")
		to := c.Query("to")
		goodID := c.Query("good_id")
		if from != "" && util.IsPostgreSQLDateFormat(from) {
			result = result.Where("created_at >= ?", from)
		}

		if to != "" && util.IsPostgreSQLDateFormat(to) {
			result = result.Where("created_at <= ?", to)
		}

		if goodID != "" {
			result = result.Where("good_id = ?", goodID)
		}

		result = util.GetFilteredDB(result, c, rowData)

		var res []struct {
			GasStation    types.JsonPsql `json:"gas_station"`
			TotalRevenue  float64        `json:"total_revenue"`
			TotalGasTanks int            `json:"total_gas_tanks"`
			TotalGasPumps int            `json:"total_gas_pumps"`
		}

		// Get all records

		result = result.
			Select(`row_to_json(gas_stations) as gas_station, 
			sum(transactions.total_revenue) as total_revenue,
			count(DISTINCT gas_tanks.id) as total_gas_tanks,
			count(DISTINCT gas_pumps.id) as total_gas_pumps`).
			Joins("left join gas_pumps on transactions.gas_pump_id = gas_pumps.id").
			Joins("left join gas_tanks on gas_pumps.gas_tank_id = gas_tanks.id").
			Joins("join gas_stations on gas_tanks.gas_station_id = gas_stations.id and gas_stations.deleted_at is null").
			Group("gas_stations.id").
			Find(&res)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, res)

	})
}

// Get transactions by station godoc
//
//	@Summary		Get a list of transactions by station [OK]
//	@Description	Return a list of transactions by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			Transactions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			from	query		string	false	"Statistics start date. Dates provided must be in the following format: 2006-01-02 15:04:05-07:00 (psql timestamp format, the ISO 8601 format)"
//	@Param			to		query		string	false	"Statistics end date. Dates provided must be in the following format: "2006-01-02	15:04:05-07:00	(psql timestamp format, the ISO 8601 format)"
//	@Success		200		{array}		model.ResTransactionByPump
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/transactions/by-customer [get]
func getTransactionsByStation(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/by-customer", func(c *gin.Context) {

		rowData := database.Transaction{}
		result := db.Table("transactions")
		from := c.Query("from")
		to := c.Query("to")
		goodID := c.Query("good_id")
		if from != "" && util.IsPostgreSQLDateFormat(from) {
			result = result.Where("created_at >= ?", from)
		}

		if to != "" && util.IsPostgreSQLDateFormat(to) {
			result = result.Where("created_at <= ?", to)
		}

		if goodID != "" {
			result = result.Where("good_id = ?", goodID)
		}

		result = util.GetFilteredDB(result, c, rowData)

		var res []struct {
			Customer        types.JsonPsql `json:"customer" `
			TotalRevenue    float64        `json:"total_revenue"`
			PendingAmount   float64        `json:"pending_amount"`
			Cash            float64        `json:"cash"`
			CompletedAmount float64        `json:"completed_amount"`
		}

		// Get all records

		result = result.
			Select(`row_to_json(customers) as customer, 
			sum(transactions.total_revenue) as total_revenue,
			sum(CASE 
								WHEN transactions.payment_type = 'Chuyển khoản' and transactions.status = 'Chờ chuyển khoản' 
								THEN transactions.total_revenue
								ELSE 0
						END) as pending_amount,
						sum(CASE 
								WHEN transactions.payment_type = 'Tiền mặt'
								THEN transactions.total_revenue
								ELSE 0
						END) as cash,
						  sum(CASE 
								WHEN transactions.payment_type = 'Chuyển khoản' and transactions.status = 'Đã chuyển khoản' 
								THEN transactions.total_revenue
								ELSE 0
						END) as completed_amount`).
			Joins("join customers on customers.id = transactions.customer_id").
			Group("customers.id").
			Find(&res)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, res)

	})
}

// Get transactions by good godoc
//
//	@Summary		Get a list of transactions by good [OK]
//	@Description	Return a list of transactions by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			Transactions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			gas_station_id	query		string	false	"Filter by gas station id"
//	@Param			from			query		string	false	"Statistics start date. Dates provided must be in the following format: 2006-01-02 15:04:05-07:00 (psql timestamp format, the ISO 8601 format)"
//	@Param			to				query		string	false	"Statistics end date. Dates provided must be in the following format: "2006-01-02	15:04:05-07:00	(psql timestamp format, the ISO 8601 format)"
//	@Param			good_id			query		string	false	"Good id"
//	@Success		200				{array}		model.ResTransactionByGood
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/transactions/by-good [get]
func getTransactionsByGood(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/by-good", func(c *gin.Context) {

		rowData := database.Transaction{}
		result := db.Table("transactions")
		from := c.Query("from")
		to := c.Query("to")
		goodID := c.Query("good_id")
		if from != "" && util.IsPostgreSQLDateFormat(from) {
			result = result.Where("created_at >= ?", from)
		}

		gasStationID := c.Query("gas_station_id")
		if gasStationID != "" {
			result = result.Where("gas_tanks.gas_station_id = ?", gasStationID)
		}

		if to != "" && util.IsPostgreSQLDateFormat(to) {
			result = result.Where("created_at <= ?", to)
		}

		if goodID != "" {
			result = result.Where("good_id = ?", goodID)
		}

		result = util.GetFilteredDB(result, c, rowData)

		var res []model.ResTransactionByGood
		// Get all records

		result = result.
			Select(`
			json_build_object('id', goods.id, 'name', goods.name) as good,
			sum(transactions.total_revenue) as total_revenue,
			sum(transactions.fuel_volume) as fuel_volume,
			sum(CASE 
					WHEN transactions.payment_type = 'Chuyển khoản' and transactions.status = 'Chờ chuyển khoản' 
					THEN transactions.total_revenue
					ELSE 0
			END) as pending_amount,
			sum(CASE 
					WHEN transactions.payment_type = 'Tiền mặt'
					THEN transactions.total_revenue
					ELSE 0
			END) as cash,
			  sum(CASE 
					WHEN transactions.payment_type = 'Chuyển khoản' and transactions.status = 'Đã chuyển khoản' 
					THEN transactions.total_revenue
					ELSE 0
			END) as completed_amount`).
			Joins("left join gas_pumps on transactions.gas_pump_id = gas_pumps.id").
			Joins("left join gas_tanks on gas_pumps.gas_tank_id = gas_tanks.id").
			Joins("left join goods on gas_tanks.good_id = goods.id").
			Group("goods.id").
			Find(&res)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, res)

	})
}

// Get transactions by good godoc
//
//	@Summary		Get a list of transactions by day [OK]
//	@Description	Return a list of transactions by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			Transactions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			from		query		string	false	"Statistics start date. Dates provided must be in the following format: 2006-01-02 15:04:05-07:00 (psql timestamp format, the ISO 8601 format)"
//	@Param			to			query		string	false	"Statistics end date. Dates provided must be in the following format: "2006-01-02	15:04:05-07:00	(psql timestamp format, the ISO 8601 format)"
//	@Param			station-id	query		string	false	"Statistics end date. Dates provided must be in the following format: "2006-01-02	15:04:05-07:00	(psql timestamp format, the ISO 8601 format)"
//	@Success		200			{array}		model.ResTransactionByGood
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/transactions/by-day [get]
func getTransactionsByDay(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/by-day", func(c *gin.Context) {

		rowData := database.Transaction{}
		result := db.Table("transactions")
		from := c.Query("from")
		to := c.Query("to")
		stationID := c.Query("gas_station_id")
		if from != "" && util.IsPostgreSQLDateFormat(from) {
			result = result.Where("created_at >= ?", from)
		}

		if to != "" && util.IsPostgreSQLDateFormat(to) {
			result = result.Where("created_at <= ?", to)
		}

		if stationID != "" {
			result = result.Where("gas_tanks.gas_station_id = ?", stationID)
		}

		result = util.GetFilteredDB(result, c, rowData)

		var res []map[string]interface{}
		// Get all records

		result = result.Debug().
			Select(`
			sum(transactions.total_revenue) as total_revenue,
			sum(transactions.fuel_volume) as fuel_volume,
			date_trunc('day', transactions.created_at) as date,
			sum(CASE 
					WHEN transactions.payment_type = 'Chuyển khoản' and transactions.status = 'Chờ chuyển khoản' 
					THEN transactions.total_revenue
					ELSE 0
			END) as pending_amount,
			sum(CASE 
					WHEN transactions.payment_type = 'Tiền mặt'
					THEN transactions.total_revenue
					ELSE 0
			END) as cash,
			sum(CASE 
					WHEN transactions.payment_type = 'Chuyển khoản' and transactions.status = 'Đã chuyển khoản' 
					THEN transactions.total_revenue
					ELSE 0
			END) as completed_amount`).
			Joins("left join gas_pumps on transactions.gas_pump_id = gas_pumps.id").
			Joins("left join gas_tanks on gas_pumps.gas_tank_id = gas_tanks.id").
			Group("date_trunc('day', transactions.created_at)").
			Find(&res)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, res)

	})
}

// Delete a transaction godoc
//
//	@Summary		Delete a transaction [OK]
//	@Description	Returns deleted rows
//	@Tags			Transactions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Transaction id"
//	@Success		200	{integer}	uint	"The number of deleted rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/transaction/{id} [delete]
func deleteATransaction(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		tx := db.Begin()
		result := tx.Table("transactions").Where("id = ?", id).Update("deleted_at", gorm.DeletedAt{Time: time.Now(), Valid: true})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		invoice := database.Invoice{}
		tx.Table("invoices").First(&invoice, "transaction_id = ?", id)
		if invoice.Draft && invoice.InvoiceGUID != "" {
			command := []database.CommandObject301{{
				PartnerInvoiceStringID: fmt.Sprintf("%d", invoice.ID),
			}}
			transaction := database.Transaction{}
			tx.Unscoped().First(&transaction, "id = ?", id)
			fmt.Println("transaction.ID", transaction.ID)

			good := util.GetInputGood(transaction, tx)
			_, err := util.BKAV_Request(301, command, good.GasStationID)

			if err != nil {
				tx.Rollback()
				returnError(c, 400, "Không thể xoá hoá đơn | "+err.Error())
				return
			}
		}
		tx.Commit()
		c.JSON(200, result.RowsAffected)
	})
}

// Restore a transaction godoc
//
//	@Summary		Restore a transaction [OK]
//	@Description	Returns restored rows
//	@Tags			Transactions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Transaction id"
//	@Success		200	{integer}	uint	"The number of restored rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/transaction/{id}/restore [post]
func restoreATransaction(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("/:id/restore", func(c *gin.Context) {
		id := c.Param("id")
		result := db.Table("transactions").Where("id = ?", id).Update("deleted_at", nil)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

type InputGood struct {
	GoodName     string    `json:"good_name"`
	Vat          int       `json:"vat"`
	GasStationID uuid.UUID `json:"gas_station_id" gorm:"type:uuid;unique"`
}
