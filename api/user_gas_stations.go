package api

import (
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/util"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Get user_gas_stations godoc
//
//	@Summary		Get a list of user_gas_stations [OK]
//	@Description	Return a list of user_gas_stations by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			UserGasStations
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResUserGasStation
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/user_gas_stations [get]
func getUserGasStations(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		rowData := database.UserGasStation{}
		result := db.Table("user_gas_stations")
		result = util.GetFilteredDB(result, c, rowData)

		var user_gas_stations []model.ResUserGasStation
		// Get all records
		deleted := c.Query("deleted")
		if deleted == "true" {
			result = result.Unscoped().Where("deleted_at is not null").Find(&user_gas_stations)
		} else {
			result = result.Find(&user_gas_stations)
		}

		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, user_gas_stations)

	})
}

// Get a user_gas_station godoc
//
//	@Summary		Get a user_gas_station [OK]
//	@Description	Return a user_gas_station if successful
//	@Tags			UserGasStations
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			user-id			path		string	true	"User id"
//	@Param			gas_station-id	path		string	true	"GasStation id"
//	@Success		200				{object}	model.ResUserGasStation
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/user_gas_stations/{user-id}/{gas_station-id} [get]
func getAUserGasStation(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:user-id/:gas_station-id", func(c *gin.Context) {

		userID := c.Param("user-id")
		gas_stationID := c.Param("gas_station-id")

		var user_gas_station model.ResUserGasStation
		// Get all records
		result := db.Table("user_gas_stations").Where("user_id = ?", userID).Where("gas_station_id = ?", gas_stationID).First(&user_gas_station)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, user_gas_station)

	})
}

// Get a user_gas_station godoc
//
//	@Summary		Get a user_gas_station [OK]
//	@Description	Return a user_gas_station if successful
//	@Tags			UserGasStations
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			user-id	path		string	true	"User id"
//	@Success		200		{object}	model.ResUserGasStation
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/user_gas_stations/{user-id} [get]
func getAUserGasStationByUserID(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:user-id", func(c *gin.Context) {

		userID := c.Param("user-id")

		var user_gas_station []model.ResUserGasStation

		// Get all records
		result := db.Table("user_gas_stations").Where("user_id = ?", userID).Find(&user_gas_station)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, user_gas_station)

	})
}

// Get a user_gas_station godoc
//
//	@Summary		Get a user_gas_station [OK]
//	@Description	Return a user_gas_station if successful
//	@Tags			UserGasStations
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			gas_station-id	path		string	true	"GasStation id"
//	@Success		200				{object}	model.ResUserGasStation
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/user_gas_stations/all/{gas_station-id} [get]
func getAUserGasStationByGasStationID(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/all/:gas_station-id", func(c *gin.Context) {

		gas_stationID := c.Param("gas_station-id")

		var user_gas_station []model.ResUserGasStation

		// Get all records
		result := db.Table("user_gas_stations").Where("gas_station_id = ?", gas_stationID).Find(&user_gas_station)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, user_gas_station)

	})
}

// Post user_gas_stations godoc
//
//	@Summary		Post user_gas_stations to database [OK]
//	@Description	Return a list of user_gas_stations if successful
//	@Tags			UserGasStations
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			UserGasStation	body		[]model.ReqUserGasStation	true	"New user_gas_stations data"
//	@Success		200				{array}		model.ReqUserGasStation		"Inserted data"
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/user_gas_stations [post]
func postUserGasStation(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {

		req := []model.ReqUserGasStation{}
		res := []model.ReqUserGasStation{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		// if err := CheckInputError(req); err != nil {
		// 	returnError(c, 400, err.Error())
		// 	return
		// }

		tx := db.Begin()

		for _, v := range req {
			result := tx.Table("user_gas_stations").Where(database.UserGasStation{UserID: v.UserID, GasStationID: v.GasStationID}).Attrs(database.UserGasStation{}).FirstOrCreate(&v)
			if result.Error != nil {
				tx.Rollback()
				returnError(c, 400, result.Error.Error())
				return
			}
			res = append(res, v)
		}

		tx.Commit()

		c.JSON(200, res)

	})
}

// Update a user_gas_station godoc
//
//	@Summary		Update a user_gas_station [OK]
//	@Description	Returns updated rows
//	@Tags			UserGasStations
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id				path		string							true	"UserGasStation id"
//	@Param			UserGasStation	body		model.ReqPatchUserGasStation	true	"UserGasStation data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200				{integer}	uint							"The number of updated rows"
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/user_gas_stations/{user-id}/{gas_station-id} [patch]
func updateAUserGasStation(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:user-id/:gas_station-id", func(c *gin.Context) {
		userID := c.Param("user-id")
		gas_stationID := c.Param("gas_station-id")

		req := model.ReqPatchUserGasStation{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("user_gas_stations").Where("user_id = ?", userID).Where("gas_station_id = ?", gas_stationID).Updates(req)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Delete a user_gas_station godoc
//
//	@Summary		Delete a user_gas_station [OK]
//	@Description	Returns deleted rows
//	@Tags			UserGasStations
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			user-id			path		string	true	"User id"
//	@Param			gas_station-id	path		string	true	"GasStation id"
//	@Success		200				{integer}	uint	"The number of deleted rows"
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/user_gas_stations/{user-id}/{gas_station-id} [delete]
func deleteAUserGasStation(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:user-id/:gas_station-id", func(c *gin.Context) {
		userID := c.Param("user-id")
		gas_stationID := c.Param("gas_station-id")

		result := db.Table("user_gas_stations").Where("user_id = ?", userID).Where("gas_station_id = ?", gas_stationID).Delete(&database.UserGasStation{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}