package api

import (
	"github.com/gin-gonic/gin"
)

var handleCheckVersion = func(router *gin.Engine) {
	router.GET("", func(c *gin.Context) {
		c.String(200, "1.0")
	})
}
