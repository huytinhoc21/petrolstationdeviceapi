package api

import (
	"fmt"
	"strings"

	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/util"

	ginJWT "github.com/appleboy/gin-jwt/v2"
	"github.com/gofrs/uuid"
	"github.com/lib/pq"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Register godoc
//
//	@Summary		User register [OK]
//	@Description	Register a new user
//	@Tags			Users
//	@Accept			json
//	@Produce		json
//	@Param			Info	body		model.ReqRegister	true	"Register information"
//	@Success		200		{object}	model.ResRegister
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/users [post]
func registerHandler(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("/users", func(c *gin.Context) {
		req := model.ReqRegister{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		req.Username = strings.TrimSpace(req.Username)
		req.Password = strings.TrimSpace(req.Password)
		req.Email = strings.TrimSpace(req.Email)
		req.Name = strings.TrimSpace(req.Name)

		if !util.IsValidEmail(req.Email) {
			returnError(c, 400, "Email is not valid")
			return
		}

		tx := db.Begin()
		id := ""
		insertErr := func() error {

			// Dang An: Create a user
			err := tx.Raw(`insert into users(name, username, password, email, role)
			values(?, ?, ?, ?, ?) returning id`,
				req.Name, req.Username, req.Password, req.Email, "user").Row().Scan(&id)
			if err != nil {
				return err
			}

			return nil
		}()

		if insertErr == nil {
			insertErr = tx.Commit().Error
		}
		if insertErr != nil {
			tx.Rollback()
			returnError(c, 400, insertErr.Error())
			return
		}
		// go util.SendVerificationEmail(id, req.Name, "user", req.Email)

		c.JSON(200, model.ResRegister{
			Id: id,
		})

	})
}

// Login godoc
//
//	@Summary		User login [OK]
//	@Description	User login with username and password
//	@Tags			Users
//	@Accept			json
//	@Produce		json
//	@Param			Info	body		model.ReqLogin	true	"Login information"
//	@Success		200		{object}	model.ResLogin
//	@Failure		400		{object}	model.HTTPError400
//	@Failure		500		{object}	model.HTTPError500
//	@Router			/users/login [post]
func loginHandler(router *gin.RouterGroup, loginHandler gin.HandlerFunc) {
	router.POST("/users/login", loginHandler)
}

// Get user info godoc
//
//	@Summary		Get user information [OK]
//	@Description	Returns user information
//	@Tags			Users
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{object}	model.ResGetUserInfo
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/users/info [get]
func getUserInfoHandler(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/info", func(c *gin.Context) {

		tmp, _ := c.Get("JWT_PAYLOAD")
		payload := tmp.(ginJWT.MapClaims)
		id := payload["ID"]

		var res struct {
			ID         uuid.UUID      `json:"id" gorm:"type:uuid;default:uuid_generate_v4()" swaggerignore:"true"`
			Name       string         `json:"name" gorm:"not null"`
			Username   string         `json:"username" gorm:"not null;unique"`
			Password   string         `json:"password" gorm:"not null"`
			Phone      string         `json:"phone"`
			Email      string         `json:"email" gorm:"not null;unique"`
			Role       string         `json:"role" gorm:"not null"`
			ApiActions pq.StringArray `json:"api_actions" gorm:"type:text[]"`
		}

		result := db.Table("users").
			Select("users.*, array_remove(array_agg(api_actions.name), NULL) as api_actions").
			Joins("left join user_permissions on users.id = user_permissions.user_id").
			Joins("left join permissions on permissions.id = user_permissions.permission_id").
			Joins("left join api_action_permissions on permissions.id = api_action_permissions.permission_id").
			Joins("left join api_actions on api_actions.id = api_action_permissions.api_action_id").
			Group("users.id").
			Where("users.id = ?", id).
			Scan(&res)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, res)

	})
}

// Get users godoc
//
//	@Summary		Get a list of users [OK]
//	@Description	Return a list of users by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			Users
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			deleted	query		bool	false	"Query whether users are deleted or not"
//	@Success		200		{array}		model.UserDataRes
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/users [get]
func getUsers(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		var users []model.UserDataRes
		result := db.Table("users")
		deleted := c.Query("deleted")

		if deleted == "true" {
			result = result.Debug().
				Select(`
				users.*, 
				array_remove(array_agg(DISTINCT user_gas_stations.gas_station_id), NULL) AS gas_station_id,
    			array_remove(array_agg(DISTINCT user_permissions.permission_id), NULL) AS permission_id`).
				Joins("left join user_gas_stations on users.id = user_gas_stations.user_id").
				Joins("left join user_permissions on users.id = user_permissions.user_id").
				Where("users.deleted_at is not null and role = 'user'").
				Group("users.id").Scan(&users)

		} else {
			result = result.Debug().
				Select(`
				users.*, 
				array_remove(array_agg(DISTINCT user_gas_stations.gas_station_id), NULL) AS gas_station_id,
    			array_remove(array_agg(DISTINCT user_permissions.permission_id), NULL) AS permission_id`).
				Joins("left join user_gas_stations on users.id = user_gas_stations.user_id").
				Joins("left join user_permissions on users.id = user_permissions.user_id").
				Where("role = 'user'").
				Group("users.id").Scan(&users)
		}

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, users)

	})
}

// Update a user godoc
//
//	@Summary		Update a user [OK]
//	@Description	Returns the number of rows updated
//	@Tags			Users
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id		path		string				true	"User id"
//	@Param			Good	body		model.ReqPatchUser	true	"User data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200		{integer}	uint				"The number of updated rows"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/users/{id} [patch]
func updateAUser(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqPatchUser{}
		c.Bind(&req)

		updateData := database.User{
			Name:     req.Name,
			Username: req.Username,
			Phone:    req.Phone,
			Email:    req.Email,
			Password: req.Password,
		}

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		tx := db.Begin()
		result := tx.Table("users").Where("id = ?", id).Updates(updateData)
		if result.Error != nil {
			tx.Rollback()
			returnError(c, 400, result.Error.Error())
			return
		} else {
			fmt.Println(result.RowsAffected)
		}

		result = tx.Table("user_permissions").Where("user_id = ?", id).Delete(&database.UserPermission{})
		if result.Error != nil {
			tx.Rollback()
			returnError(c, 400, result.Error.Error())
			return
		}

		result = tx.Table("user_gas_stations").Where("user_id = ?", id).Delete(&database.UserGasStation{})
		if result.Error != nil {
			tx.Rollback()
			returnError(c, 400, result.Error.Error())
			return
		}

		for _, v := range req.PermissionID {
			uid, _ := uuid.FromString(id)
			result = tx.Table("user_permissions").Create(&database.UserPermission{UserID: uid, PermissionID: v})
			if result.Error != nil {
				tx.Rollback()
				returnError(c, 400, result.Error.Error())
				return
			}
		}

		for _, v := range req.GasStationID {
			uid, _ := uuid.FromString(id)
			stationID, _ := uuid.FromString(v)
			result = tx.Table("user_gas_stations").Create(&database.UserGasStation{UserID: uid, GasStationID: stationID})
			if result.Error != nil {
				tx.Rollback()
				returnError(c, 400, result.Error.Error())
				return
			}
		}

		tx.Commit()

		c.JSON(200, result.RowsAffected)
	})
}
