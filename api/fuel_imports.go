package api

import (
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/util"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Get fuel_imports godoc
//
//	@Summary		Get a list of fuel_imports [OK]
//	@Description	Return a list of fuel_imports by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			FuelImports
//	@Accept			json
//	@Produce		json
//	@Param			from			query	string	false	"Statistics start date. Dates provided must be in the following format: 2006-01-02 15:04:05-07:00 (psql timestamp format, the ISO 8601 format)"
//	@Param			to				query	string	false	"Statistics end date. Dates provided must be in the following format: "2006-01-02	15:04:05-07:00	(psql timestamp format, the ISO 8601 format)"
//	@Param			good_id			query	string	false	"Good id"
//	@Param			gas_station_id	query	string	false	"Filter by gas station id"
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResFuelImport
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/fuel_imports [get]
func getFuelImports(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		rowData := database.FuelImport{}
		result := db.Table("fuel_imports")
		from := c.Query("from")
		to := c.Query("to")
		goodID := c.Query("good_id")
		gasStationID := c.Query("gas_station_id")
		if gasStationID != "" {
			result = result.Where("gas_tanks.gas_station_id = ?", gasStationID)
		}

		if from != "" && util.IsPostgreSQLDateFormat(from) {
			result = result.Where("created_at >= ?", from)
		}

		if to != "" && util.IsPostgreSQLDateFormat(to) {
			result = result.Where("created_at <= ?", to)
		}

		if goodID != "" {
			result = result.Where("good_id = ?", goodID)
		}

		result = util.GetFilteredDB(result, c, rowData)

		var fuel_imports []model.ResFuelImport
		// // Get all records
		// deleted := c.Query("deleted")
		// if deleted == "true" {
		// 	result = result.Unscoped().Where("deleted_at is not null").Find(&fuel_imports)
		// } else {
		// 	result = result.Find(&fuel_imports)
		// }

		result = result.Select("fuel_imports.*, row_to_json(goods) as good").
			Joins("left join gas_tanks on fuel_imports.gas_tank_id = gas_tanks.id").
			Joins("left join goods on good_id = goods.id").
			Group("fuel_imports.id, goods.id").Find(&fuel_imports)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, fuel_imports)

	})
}

// Get a fuel_import godoc
//
//	@Summary		Get a fuel_import [OK]
//	@Description	Return a fuel_import if successful
//	@Tags			FuelImports
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"FuelImport id"
//	@Success		200	{object}	model.ResFuelImport
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/fuel_imports/{id} [get]
func getAFuelImport(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:id", func(c *gin.Context) {

		id := c.Param("id")
		var fuel_import model.ResFuelImport
		// Get all records
		result := db.Table("fuel_imports").First(&fuel_import, "id = ?", id)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, fuel_import)

	})
}

// Post a fuel_import godoc
//
//	@Summary		Post a fuel_import to database [OK]
//	@Description	Return a id if successful
//	@Tags			FuelImports
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			FuelImport	body		model.ReqFuelImport	true	"New fuel_import data"
//	@Success		200			{string}	string				"Inserted Unit ID"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/fuel_imports [post]
func postAFuelImport(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {
		req := model.ReqFuelImport{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		result := db.Table("fuel_imports").Create(&req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, req.ID)

	})
}

// Update a fuel_import godoc
//
//	@Summary		Update a fuel_import [OK]
//	@Description	Returns updated rows
//	@Tags			FuelImports
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id			path		string						true	"FuelImport id"
//	@Param			FuelImport	body		model.ReqPatchFuelImport	true	"FuelImport data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200			{integer}	uint						"The number of updated rows"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/fuel_imports/{id} [patch]
func updateAFuelImport(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqPatchFuelImport{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("fuel_imports").Where("id = ?", id).Updates(req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Delete a fuel_import godoc
//
//	@Summary		Delete a fuel_import [OK]
//	@Description	Returns deleted rows
//	@Tags			FuelImports
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"FuelImport id"
//	@Success		200	{integer}	uint	"The number of deleted rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/fuel_imports/{id} [delete]
func deleteAFuelImport(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		result := db.Where("id = ?", id).Delete(&database.FuelImport{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Get import statistics godoc
//
//	@Summary		Get a list of import statistics [OK]
//	@Description	Return a list of import statistics by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			FuelImports
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			from			query		string	true	"If provided, statistics will be calculated from the 'from' date. Dates provided must be in the following format: 2023-08-20"
//	@Param			to				query		string	true	"If provided, statistics will be calculated as of the 'to' date. Dates provided must be in the following format: 2023-08-20"
//	@Param			gas-station-id	query		string	true	"Gas station ID"
//	@Success		200				{array}		model.ResDeposit
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/fuel_imports/statistics [get]
func getImportStatistics(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/statistics", func(c *gin.Context) {

		from := c.Query("from")
		to := c.Query("to")
		gasStationID := c.Query("gas-station-id")
		result := db.Table("transactions")

		var res []map[string]interface{}

		result = result.
			Select(`
			goods.id as good_id, goods.name as good_name, goods.unit,
			(select sum(amount) from fuel_imports where gas_tank_id = any (array_agg(gas_tanks.id)) and created_at < ?) as accumulated_import_amount,
			(select sum(amount) from fuel_imports where gas_tank_id = any (array_agg(gas_tanks.id)) and created_at between ? and ?) as import_amount,
			sum(
				case
					when transactions.created_at < ?
					then transactions.fuel_volume
					else 0
				end 
			) as accumulated_export_amount,
			sum(
				case
					when transactions.created_at between ? and ?
					then transactions.fuel_volume
					else 0
				end 
			) as export_amount`, from, from, to, from, from, to).
			Joins("join gas_pumps on gas_pumps.id = transactions.gas_pump_id").
			Joins("join gas_tanks on gas_tanks.id = gas_pumps.gas_tank_id and gas_tanks.gas_station_id = ?", gasStationID).
			Joins("join goods on goods.id = gas_tanks.good_id").
			Group("goods.id").Scan(&res)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		if res == nil {
			res = []map[string]interface{}{}
		}

		c.JSON(200, res)

	})
}
