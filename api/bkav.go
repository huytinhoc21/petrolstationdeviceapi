package api

import (
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/util"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"
)

// Get invoices godoc
//
//	@Summary		Get a business info
//	@Description	Return the relative business data relative to the mst
//	@Tags			BKAV
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			mst	path		string	true	"MST"
//	@Success		200	{object}	database.BizInfo
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/bkav/get-biz/{mst} [get]
func getBizData(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/get-biz/:mst", func(c *gin.Context) {
		// rowData := database.BizInfo{}
		mst := c.Param("mst")

		res, err := util.BKAV_Request(904,
			database.CommandObject904(mst),
			uuid.Must((uuid.FromString("6c69964a-0b98-4ccb-a7d4-6f2a0d681e3e"))))
		if err != nil {
			returnError(c, 400, err.Error())
			return
		}
		c.JSON(200, res)

	})
}

// Get invoice status godoc
//
//	@Summary		Get invoice status
//	@Description	Return the invoice status relative to the mst
//	@Tags			BKAV
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Invoice id"
//	@Success		200	{object}	database.BizInfo
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/bkav/invoices/{id}/status [get]
func getInvoiceStatus(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/invoices/:id/status", func(c *gin.Context) {
		// rowData := database.BizInfo{}
		id := c.Param("id")
		type InputGood struct {
			GasStationID uuid.UUID `json:"gas_station_id" gorm:"type:uuid;unique"`
		}
		var good InputGood
		db.Table("invoices").
			Select(`gas_tanks.gas_station_id`).
			Joins("left join transactions on invoices.transaction_id = transactions.id").
			Joins("left join gas_pumps on transactions.gas_pump_id = gas_pumps.id").
			Joins("left join gas_tanks on gas_pumps.gas_tank_id = gas_tanks.id").
			Joins("left join goods on gas_tanks.good_id = goods.id").
			Where("invoices.id = ?", id).
			Find(&good)
		if good.GasStationID == uuid.Nil {
			returnError(c, 400, "Invalid invoice")
			return
		}
		bkavRes, err := util.BKAV_Request(800, database.CommandObject800(id), good.GasStationID)
		if err != nil {
			returnError(c, 400, err.Error())
			return
		}
		objectJSON, _ := bkavRes["Object"].(string)
		objects, err := util.ParseBKAVObjectRes800(objectJSON)
		if err != nil {
			returnError(c, 400, err.Error())
			return
		}

		if objects.Invoice.InvoiceStatusID == 2 || objects.Invoice.InvoiceStatusID == 6 || objects.Invoice.InvoiceStatusID == 8 {
			result := db.Table("invoices").Where("id = ?", id).Update("draft", false)
			if result.Error != nil {
				returnError(c, 400, result.Error.Error())
				return
			}
		} else {
			result := db.Table("invoices").Where("id = ?", id).Update("draft", true)
			if result.Error != nil {
				returnError(c, 400, result.Error.Error())
				return
			}
		}
		var invoice model.ResInvoice
		result := db.Table("invoices").First(&invoice, "id = ?", id)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, invoice)

	})
}

// // Get invoices godoc
// //
// //	@Summary		Get a business info
// //	@Description	Return the relative business data relative to the mst
// //	@Tags			BKAV
// //	@Accept			json
// //	@Produce		json
// //	@Security		ApiKeyAuth
// //	@Param			BKAVInvoice	body		model.BKAVInvoice	true	"New customer data"
// //	@Success		200			{object}	database.BizInfo
// //	@Failure		400			{object}	model.HTTPError400
// //	@Router			/bkav/transactions/{id}/invoice [post]
// func postBKAVInvoice(router *gin.RouterGroup, db *gorm.DB) {
// 	router.POST("/transactions/:id/invoice", func(c *gin.Context) {
// 		// rowData := database.BizInfo{}
// 		txID := c.Param("id")
// 		// 1 Hóa đơn Giá trị gia tăng
// 		// 2 Hóa đơn bán hàng
// 		var transaction database.Transaction
// 		// Get all records
// 		result := db.Table("transactions").First(&transaction, "id = ?", txID)
// 		if result.Error != nil {
// 			returnError(c, 400, result.Error.Error())
// 			return
// 		}
// 		command := database.CommandObject100{
// 			Invoice: database.BKAVInvoice{
// 				InvoiceTypeID: 1,
// 				InvoiceDate:   transaction.CreatedAt,
// 				BuyerName:     "Khách lẻ",
// 				// BuyerTaxCode            string    `json:"buyer_tax_code"`            // "0123456789",				//Mã số Thuế
// 				// BuyerUnitName           string    `json:"buyer_unit_name"`           // "CONG TY ABC",			//Đơn vị mua hàng
// 				// BuyerAddress            string    `json:"buyer_address"`             // "So 632, Duong A, Q. Hai Ba Trung",// Địa chỉ người/đơn vị mua hàng
// 				// BuyerBankAccount        string    `json:"buyer_bank_account"`        //Số tài khoản ngân hàng người mua
// 				PayMethodID: 1, //1: tiền mặt 2 chuyển khoản
// 				// ReceiveTypeID           int       `json:"receive_type_id"`           //3,//Hình thức nhận Hoá đơn, 1:Email, 2: SMS, 3: Email & SMS, 4: Chuyển phát nhanh
// 				// ReceiverEmail           string    `json:"receiver_email"`            // "", 					//Email nhận thông báo tra cứu Hoá đơn
// 				// ReceiverMobile          string    `json:"receiver_mobile"`           // "",					 //Số điện thoại nhận SMS tra cứu Hoá đơn (Nếu không đăng ký gửi hóa đơn qua SMS thì không cần truyền sđt)
// 				// ReceiverAddress         string    `json:"receiver_address"`          // "", 					// Địa chỉ nhận Hoá đơn (Chuyển phát nhanh)
// 				// ReceiverName            string    `json:"receiver_name"`             // "",  					//Tên ngưởi nhận Hoá đơn (Chuyển phát nhanh)
// 				// Note                    string    `json:"note"`                      // "Ghi Chú",					 //Ghi chú trên Hoá đơn
// 				// BillCode: transaction.ID.String(), // "",					//Số hoá đơn/chứng từ nội bộ trong phần mềm kế toán
// 				// CurrencyID              string    `json:"currency_id"`               // "VND",					//Đơn vị tiền tệ, mặc định VND
// 				// ExchangeRate            float32   `json:"exchange_rate"`             //": 1.0,					//Tỷ giá, mặc định là 1
// 				// InvoiceForm             string    `json:"invoice_form"`              // "1",				//Mẫu số
// 				// InvoiceSerial           string    `json:"invoice_serial"`            // "C23TAA",					//Ký hiệu Hoá đơn
// 				// InvoiceNo               int       `json:"invoice_no"`                // 1,						//Số Hoá đơn
// 				// MaCuaCQT                string    `json:"ma_cua_cqt"`                // "M1-23-CKSPG-00000000001",    // Mã của CQT từ máy tính tiền nếu client truyền lên, Bkav cấp tự động thì để trống
// 				// CCCD                    string    `json:"cccd"`                      // "999999999988",                                         // Căn cước công dân của ng mua trên HĐ MTT
// 				// UserDefine              string    `json:"user_define"`               // "",					//Thông tin đặc biệt trên Hoá đơn, tham khảo tại đây
// 				// IsBTH                   bool      `json:"is_bth"`                    // "true"             // true là HĐ không mã gửi theo BTH sang thuế, false là gửi từng lần sang thuế,
// 				// OriginalInvoiceIdentify string    `json:"original_invoice_identify"` //"[01GTKT0/001]_[AA/17E]_[0000001]"/// Thông tin Hoá đơn gốc dùng trong trường hợp thay thế, điều chỉnh. Định dạng như sau: [Mẫu Số]_[Ký hiệu]_[Số Hoá đơn], ví dụ: [01GTKT0/001]_[AA/17E]_[0000001]
// 			},
// 			ListInvoiceDetailsWS: []database.InvoiceDetailsWS{
// 				{
// 					// ItemName: transaction.
// 					UnitName: "Lít",
// 					Qty:      transaction.FuelVolume,
// 					Price:    transaction.UnitPrice,
// 					Amount:   transaction.TotalRevenue,
// 					// TODO
// 					TaxRateID: 1, // 1: 0%, 2: 5%, 3: 10%
// 					// TaxRate           float64 // 10.0,
// 					// TODO
// 					// TaxAmount         float64 // 100.0, 				//Tiền thuế
// 					// OtherAmount       float64 //2000,                                       // Thuế, phí khác theo chi tiết mặt hàng
// 					// DiscountRate      float64 // 0.0,				//Tỷ lệ chiết khấu (xem tại đây)
// 					// DiscountAmount    float64 //"",			//Số tiền chiết khấu (xem tại đây)
// 					// IsDiscount        bool    // false,				// true - chiết khấu, false - Hàng hoá dịch vụ  (xem tại đây)
// 					// UserDefineDetails string  // "",		           // Thông tin đặc biệt trên Hoá đơn (xem tại đây)
// 					// ItemTypeID        int     // 0,				// Loại Item (xem tại đây)
// 					// IsIncrease        *bool
// 				}},
// 			PartnerInvoiceStringID: txID,
// 		}
// 		res, err := util.BKAV_Request(100, command)
// 		if err != nil {
// 			returnError(c, 400, err.Error())
// 			return
// 		}
// 		c.JSON(200, res)

// 	})
// }
