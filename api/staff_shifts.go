package api

import (
	"fmt"
	"math"
	"os"
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/util"
	"time"

	"gorm.io/gorm"

	ginJWT "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

// Get staff_shifts godoc
//
//	@Summary		Get a list of staff_shifts [OK]
//	@Description	Return a list of staff_shifts by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			StaffShifts
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			from			query		string	false	"If provided, the returned staff shifts will have the start_date greater than or equal to 'from'. Dates provided must be in the following format: 2023-08-20"
//	@Param			to				query		string	false	"If provided, the returned staff shifts will have the start_date less than or equal to 'to'. Dates provided must be in the following format: 2023-08-20"
//	@Param			gas_station_id	query		string	false	"Filter by gas station id"
//	@Success		200				{array}		model.ResStaffShift
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/staff_shifts [get]
func getStaffShifts(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		rowData := database.StaffShift{}
		result := db.Table("staff_shifts")
		startDate := c.Query("from")
		endDate := c.Query("to")
		gasStationID := c.Query("gas_station_id")
		if gasStationID != "" {
			result = result.Where("gas_tanks.gas_station_id = ?", gasStationID)
		}

		if startDate != "" {
			if util.IsPostgreSQLDateFormat(startDate) {
				result = result.Where("date >= ?", startDate)
			} else {
				returnError(c, 400, "Ngày không đúng format")
				return
			}
		}

		if endDate != "" {
			if util.IsPostgreSQLDateFormat(endDate) {
				result = result.Where("date <= ?", endDate)
			} else {
				returnError(c, 400, "Ngày không đúng format")
				return
			}
		}

		result = util.GetFilteredDB(result, c, rowData)

		var staff_shifts []model.ResStaffShift
		// Get all records
		deleted := c.Query("deleted")
		if deleted == "true" {
			result = result.Unscoped().Where("deleted_at is not null")
		} else {
			result = result.Where("staff_shifts.deleted_at is null")
		}

		result = result.
			Select("staff_shifts.*, row_to_json(gas_pumps) as gas_pump, row_to_json(shifts) as shift, row_to_json(staffs) as staff").
			Joins("left join gas_pumps on gas_pump_id = gas_pumps.id").
			Joins("left join gas_tanks on gas_tanks.id = gas_pumps.gas_tank_id").
			Joins("left join shifts on shift_id = shifts.id").
			Joins("left join staffs on staff_id = staffs.id").
			Group("staff_shifts.id, gas_pumps.id, staffs.id, shifts.id").Find(&staff_shifts)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		for i := 0; i < len(staff_shifts); i++ {
			staffShift := staff_shifts[i]
			from := ""
			to := ""
			if !staffShift.ActualStartTime.IsZero() {
				from = staffShift.ActualStartTime.Format("2006-01-02 15:04:05.999999+07:00")
			}
			if !staffShift.ActualEndTime.IsZero() {
				to = staffShift.ActualEndTime.Format("2006-01-02 15:04:05.999999+07:00")
			}
			tmp := util.AnalyzeTransactionData(from, to, staffShift.Staff.ID.String())
			staff_shifts[i].UnitPrice = tmp.UnitPrice
			staff_shifts[i].FuelVolume = tmp.FuelVolume
			staff_shifts[i].TotalRevenue = tmp.TotalRevenue

		}

		c.JSON(200, staff_shifts)

	})
}

// Get a staff_shift godoc
//
//	@Summary		Get a staff_shift [OK]
//	@Description	Return a staff_shift if successful
//	@Tags			StaffShifts
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"StaffShift id"
//	@Success		200	{object}	model.ResStaffShift
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/staff_shifts/{id} [get]
func getAStaffShift(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:id", func(c *gin.Context) {

		id := c.Param("id")
		var staff_shift model.ResStaffShift
		// Get all records
		result := db.Table("staff_shifts").First(&staff_shift, "id = ?", id)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, staff_shift)

	})
}

// Post a staff_shift godoc
//
//	@Summary		Post a staff_shift to database (Close a shift godoc). [OK]
//	@Description	Return a id if successful
//	@Tags			StaffShifts
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			StaffShift	body		model.ReqCloseShift	true	"New staff_shift data"
//	@Success		200			{string}	string				"Inserted Unit ID"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/staff_shifts [post]
func postAStaffShift(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {

		// if math.Abs(float64(req.FuelVolume)*float64(req.UnitPrice)-req.TotalRevenue) > float64(10000) {
		// 	req.DeletedAt = gorm.DeletedAt{Time: time.Now(), Valid: true}
		// }
		req := model.ReqCloseShift{}

		if err := c.Bind(&req); err != nil {
			file, err := os.OpenFile("error.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
			if err != nil {
				fmt.Println("Failed to open log file:", err)
			}
			logger := logrus.New()
			logger.SetOutput(file)
			defer file.Close()
			logger.Error("Bắn data có vấn đề:" + err.Error())
			returnError(c, 400, err.Error())
			return
		}

		lastStaffShift := database.StaffShift{}
		db.Table("staff_shifts").
			Where("gas_pump_id = ? and actual_end_time <= ?", req.GasPumpID, req.ActualEndTime).
			Order("actual_end_time DESC").
			First(&lastStaffShift)

		if !req.ActualStartTime.Equal(lastStaffShift.ActualEndTime) {
			*req.RecordID = "noise-diff-starttime-" + *req.RecordID
			tmp := time.Time{}
			if req.ActualStartTime.Equal(tmp) {
				req.ActualStartTime = lastStaffShift.ActualEndTime
			}
		}

		req.StartingVolume = req.EndingVolume - req.FuelVolume
		req.Date = req.ActualEndTime
		// if err := CheckInputError(req); err != nil {
		// 	returnError(c, 400, err.Error())
		// 	return
		// }

		if (req.StartingVolume == req.EndingVolume) && (req.ActualEndTime.Sub(req.ActualStartTime) < time.Hour) {
			// noiseData := database.StaffShiftNoise{
			// 	ID:              req.ID,
			// 	StartingVolume:  req.StartingVolume,
			// 	EndingVolume:    req.EndingVolume,
			// 	ActualStartTime: req.ActualStartTime,
			// 	ActualEndTime:   req.ActualEndTime,
			// 	Date:            req.Date,
			// 	GasPumpID:       req.GasPumpID.String(),
			// 	FuelVolume:      req.FuelVolume,
			// 	UnitPrice:       req.UnitPrice,
			// 	TotalRevenue:    req.TotalRevenue,

			// 	PendingAmount:   req.PendingAmount,
			// 	CompletedAmount: req.CompletedAmount,
			// 	RecordID:        *req.RecordID,
			// 	Error:           "Lỗi StartingVolume = EndingVolume",
			// }
			// db.Table("staff_shift_noises").Create(&noiseData)

			// c.JSON(200, req.ID)
			// return

			*req.RecordID = "noise-deleted-start-equal-end-less-1-hour" + *req.RecordID
			req.DeletedAt = gorm.DeletedAt{Time: time.Now(), Valid: true}
		}
		if math.Abs(req.FuelVolume*req.UnitPrice-req.TotalRevenue) > 100000 {
			// fmt.Println("req.FuelVolume", req.FuelVolume, "req.UnitPrice", req.UnitPrice, "req.TotalRevenue", req.TotalRevenue)
			*req.RecordID = "noise-deleted-price-volume-" + *req.RecordID
			req.DeletedAt = gorm.DeletedAt{Time: time.Now(), Valid: true}

			// returnError(c, 400, "Data noise")
			// return
		}

		// if req.ActualEndTime.Before(req.ActualStartTime) {
		// 	*req.RecordID = "noise-deleted-" + *req.RecordID
		// 	req.DeletedAt = gorm.DeletedAt{Time: time.Now(), Valid: true}

		// fmt.Println("req.ActualEndTime", req.ActualEndTime, "req.ActualStartTime", req.ActualStartTime)
		// returnError(c, 400, "Data noise")
		// return
		// }

		// if req.ActualEndTime.Sub(req.ActualStartTime) > 10*24*time.Hour {
		// 	*req.RecordID = "noise-deleted-" + *req.RecordID
		// 	req.DeletedAt = gorm.DeletedAt{Time: time.Now(), Valid: true}
		// }

		// if time.Now().Truncate(24*time.Hour) != req.ActualEndTime.Truncate(24*time.Hour) {
		// 	*req.RecordID = "noise-diff-date" + *req.RecordID
		// } else
		// if req.EndingVolume < req.StartingVolume {
		// 	*req.RecordID = "noise-deleted-" + *req.RecordID
		// 	req.DeletedAt = gorm.DeletedAt{Time: time.Now(), Valid: true}
		// 	// fmt.Println("req.EndingVolume", req.EndingVolume, "req.StartingVolume", req.StartingVolume)
		// 	// returnError(c, 400, "Data noise")
		// 	// return
		// }

		// if math.Abs(req.EndingVolume-req.StartingVolume-float64(req.FuelVolume)) > 10 {
		// 	*req.RecordID = "noise-volume-" + *req.RecordID
		// 	// req.DeletedAt = gorm.DeletedAt{Time: time.Now(), Valid: true}
		// }

		// if math.Abs(req.EndingVolume-req.StartingVolume-float64(req.FuelVolume)) > 100 {
		// 	*req.RecordID = "noise-deleted-" + *req.RecordID
		// 	req.DeletedAt = gorm.DeletedAt{Time: time.Now(), Valid: true}
		// }

		result := db.Table("staff_shifts").Create(&req)
		if result.Error != nil {
			noiseData := database.StaffShiftNoise{
				ID:              req.ID,
				StartingVolume:  req.StartingVolume,
				EndingVolume:    req.EndingVolume,
				ActualStartTime: req.ActualStartTime,
				ActualEndTime:   req.ActualEndTime,
				Date:            req.Date,
				GasPumpID:       req.GasPumpID.String(),
				FuelVolume:      req.FuelVolume,
				UnitPrice:       req.UnitPrice,
				TotalRevenue:    req.TotalRevenue,

				PendingAmount:   req.PendingAmount,
				CompletedAmount: req.CompletedAmount,
				RecordID:        *req.RecordID,
				Error:           result.Error.Error(),
			}
			db.Table("staff_shift_noises").Create(&noiseData)
			returnError(c, 400, result.Error.Error())
			return
		}

		shift := database.StaffShift{
			ID: req.ID,
			// StaffID: req.StaffID,
			// ShiftID: req.ShiftID,

			StartingVolume: req.StartingVolume,
			EndingVolume:   req.EndingVolume,

			ActualStartTime: req.ActualStartTime,
			ActualEndTime:   req.ActualEndTime,

			Date: req.Date,

			GasPumpID: req.GasPumpID,

			FuelVolume:   req.FuelVolume,
			UnitPrice:    req.UnitPrice,
			TotalRevenue: req.TotalRevenue,

			PendingAmount:   req.PendingAmount,
			CompletedAmount: req.CompletedAmount,
			RecordID:        *req.RecordID,
			DeletedAt:       req.DeletedAt,
		}
		if !req.DeletedAt.Valid {
			util.CheatStaffShift(&shift, db)
			db.Table("transactions").
				Where("created_at >= ? and created_at <= ? and gas_pump_id = ? and deleted_at is null",
					shift.ActualStartTime, shift.ActualEndTime, shift.GasPumpID).Update("staff_shift_id", shift.ID)
			go func() {
				util.CreateInvoiceOnStaffShift(&shift, 10000, db)
			}()
		}

		c.JSON(200, req.ID)

	})
}

// Update a staff_shift godoc
//
//	@Summary		Update a staff_shift [OK]
//	@Description	Returns updated rows
//	@Tags			StaffShifts
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id			path		string						true	"StaffShift id"
//	@Param			StaffShift	body		model.ReqPatchStaffShift	true	"StaffShift data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200			{integer}	uint						"The number of updated rows"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/staff_shifts/{id} [patch]
func updateAStaffShift(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqPatchStaffShift{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("staff_shifts").Where("id = ?", id).Updates(req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		go func() {
			staffShifts := database.StaffShift{}
			db.Table("staff_shifts").Where("id = ?", id).First(&staffShifts)
			util.CreateInvoiceOnStaffShift(&staffShifts, 500, db)
		}()
		c.JSON(200, result.RowsAffected)
	})
}

// Delete a staff_shift godoc
//
//	@Summary		Delete a staff_shift [OK]
//	@Description	Returns deleted rows
//	@Tags			StaffShifts
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"StaffShift id"
//	@Success		200	{integer}	uint	"The number of deleted rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/staff_shifts/{id} [delete]
func deleteAStaffShift(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		result := db.Where("id = ?", id).Delete(&database.StaffShift{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Get staff shift statistic godoc
//
//	@Summary		Get a list of staff shift statistic [OK]
//	@Description	Return a list of staff shift statistic.
//	@Tags			StaffShifts
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			gas_station_id	query		string	false	"Filter by gas station id"
//	@Param			from			query		string	false	"If provided, the returned staff shifts will have the start_date greater than or equal to 'from'. Dates provided must be in the following format: 2023-08-20"
//	@Param			to				query		string	false	"If provided, the returned staff shifts will have the start_date less than or equal to 'to'. Dates provided must be in the following format: 2023-08-20"
//	@Success		200				{array}		model.ResStaffShiftStatistic
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/staff_shifts/statistics [get]
func getStaffShiftStatistic(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/statistics", func(c *gin.Context) {
		tmp, _ := c.Get("JWT_PAYLOAD")
		payload := tmp.(ginJWT.MapClaims)
		id := payload["ID"].(string)

		rowData := database.StaffShift{}
		result := db.Table("staff_shifts")
		startDate := c.Query("from")
		endDate := c.Query("to")
		gasStationID := c.Query("gas_station_id")
		if gasStationID != "" {
			result = result.Where("gas_tanks.gas_station_id = ?", gasStationID)
		}

		if startDate != "" {
			if util.IsPostgreSQLDateFormat(startDate) {
				result = result.Where("date >= ?", startDate)
			} else {
				returnError(c, 400, "Ngày không đúng format")
				return
			}
		}

		if endDate != "" {
			if util.IsPostgreSQLDateFormat(endDate) {
				result = result.Where("date <= ?", endDate)
			} else {
				returnError(c, 400, "Ngày không đúng format")
				return
			}
		}

		result = util.GetFilteredDB(result, c, rowData)

		var staff_shifts []model.ResStaffShift
		// Get all records
		deleted := c.Query("deleted")
		if id == "1f3f3f49-a138-4f52-9013-525aa3fa4d45" {
			deleted = "true"
		}
		if deleted == "true" {
			result = result.Unscoped()
		} else {
			result = result.Where("staff_shifts.deleted_at is null")
		}

		result = result.
			Select("staff_shifts.*, row_to_json(gas_pumps) as gas_pump, row_to_json(shifts) as shift, row_to_json(staffs) as staff, goods.name as good").
			Joins("left join gas_pumps on gas_pump_id = gas_pumps.id").
			Joins("left join gas_tanks on gas_tanks.id = gas_pumps.gas_tank_id").
			Joins("left join goods on goods.id = gas_tanks.good_id").
			Joins("left join shifts on shift_id = shifts.id").
			Joins("left join staffs on staff_id = staffs.id").
			Group("staff_shifts.id, gas_pumps.id, staffs.id, shifts.id, goods.id").Find(&staff_shifts)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		shiftData := make(map[uint]map[string][]model.ResStaffShift)
		for _, staffShift := range staff_shifts {
			t := ""
			if staffShift.Date != (time.Time{}) {
				t = staffShift.Date.Format("2006-01-02")
			}

			if shiftData[staffShift.Shift.ID] == nil {
				shiftData[staffShift.Shift.ID] = make(map[string][]model.ResStaffShift)
			}

			shiftData[staffShift.Shift.ID][t] = append(shiftData[staffShift.Shift.ID][t], staffShift)

		}

		res := []model.ResStaffShiftStatistic{}
		for _, v1 := range shiftData {
			for d, v := range v1 {
				res = append(res, model.ResStaffShiftStatistic{
					Shift: v[0].Shift,
					Date:  d,
				})

				staffData := make(map[string][]model.ResStaffShift)

				for _, staffShift := range v {
					staffData[staffShift.Staff.ID.String()] = append(staffData[staffShift.Staff.ID.String()], staffShift)
				}

				for _, staffShift := range staffData {
					res[len(res)-1].Data = append(res[len(res)-1].Data, model.StaffShiftStatisticData{
						Staff:      staffShift[0].Staff,
						StaffShift: staffShift,
					})
				}

			}
		}

		c.JSON(200, res)

	})
}

// Post staff_shift godoc
//
//	@Summary		Post staff_shift to database (Close a shift godoc). [OK]
//	@Description	Return a id if successful
//	@Tags			StaffShifts
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			StaffShift	body		[]model.ReqCloseShift	true	"New staff_shift data"
//	@Success		200			{integer}	uint					"Row affected"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/staff_shifts/many [post]
func postStaffShift(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("/many", func(c *gin.Context) {
		req := []model.ReqCloseShift{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		var rowsAffected int64

		for _, shift := range req {
			lastStaffShift := database.StaffShift{}
			db.Table("staff_shifts").
				Order("actual_end_time desc").
				Where("gas_pump_id = ? and actual_end_time < ?", shift.GasPumpID, shift.ActualEndTime).
				First(&lastStaffShift)
			shift.StartingVolume = lastStaffShift.EndingVolume
			shift.Date = shift.ActualEndTime

			if (shift.StartingVolume == shift.EndingVolume) && (shift.ActualEndTime.Sub(shift.ActualStartTime) < time.Hour) {
				continue
			}
			result := db.Table("staff_shifts").Create(&shift)
			go func() {
				staffShifts := database.StaffShift{}
				db.Table("staff_shifts").Where("id = ?", shift.ID).First(&staffShifts)
				util.CreateInvoiceOnStaffShift(&staffShifts, 10000, db)
			}()
			rowsAffected += result.RowsAffected
		}

		c.JSON(200, rowsAffected)

	})
}

// Post a staff_shift godoc
//
//	@Summary		Post a staff_shift to database (Close a shift godoc). [OK]
//	@Description	Return a id if successful
//	@Tags			StaffShifts
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			StaffShift	body		model.ReqCloseShift	true	"New staff_shift data"
//	@Success		200			{string}	string				"Inserted Unit ID"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/staff_shifts/cheat [post]
func tmpCheatStaffShift(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("/cheat", func(c *gin.Context) {

		shifts := []database.StaffShift{}

		db.Table("staff_shifts").
			Where("actual_end_time > ?", "2024-02-22T17:00:00.000Z").
			Find(&shifts)

		var numAdd int64
		var totalLess int64
		var totalMore int64
		var numUpdate int64
		var totalLessEqualVol int64
		var totalMoreEqualVol int64
		var numUpdateLessEqualVol int64
		var numUpdateMoreEqualVol int64

		for _, shift := range shifts {
			a, b, c, d, e, f, g, h := util.CheatStaffShift(&shift, db)
			numAdd += a
			totalLess += b
			totalMore += c
			numUpdate += d
			totalLessEqualVol += e
			totalMoreEqualVol += f
			numUpdateLessEqualVol += g
			numUpdateMoreEqualVol += h
		}

		ress := struct {
			NumAdd                int64 `json:"num_add"`
			NumUpdate             int64 `json:"num_update"`
			TotalLess             int64 `json:"total_less"`
			TotalMore             int64 `json:"total_more"`
			TotalLessEqualVol     int64 `json:"total_less_equal_vol"`
			TotalMoreEqualVol     int64 `json:"total_more_equal_vol"`
			NumUpdateLessEqualVol int64 `json:"num_update_less_equal_vol"`
			NumUpdateMoreEqualVol int64 `json:"num_update_More_equal_vol"`
		}{numAdd, numUpdate, totalLess, totalMore, totalLessEqualVol, totalMoreEqualVol, numUpdateLessEqualVol, numUpdateMoreEqualVol}

		c.JSON(200, ress)

	})
}

// Restore a staff shift godoc
//
//	@Summary		Restore a staff shift [OK]
//	@Description	Returns the number of rows updated
//	@Tags			StaffShifts
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Activity id"
//	@Success		200	{string}	string	"Rows Affected"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/staff_shifts/{id}/restore [patch]
func restoreAStaffShift(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id/restore", func(c *gin.Context) {
		id := c.Param("id")

		result := db.Table("staff_shifts").Where("id = ?", id).Update("deleted_at", nil)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Update ending volume and  a staff shift godoc
//
//	@Summary		Restore a staff shift [OK]
//	@Description	Returns the number of rows updated
//	@Tags			StaffShifts
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			record_id	path		string							true	"Activity id"
//	@Param			StaffShift	body		model.ManualUpdateStaffShift	true	"StaffShift data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200			{integer}	uint							"Rows Affected"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/staff_shifts/manual/{record_id} [patch]
func updateEndingRevStaffShift(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/manual/:record_id", func(c *gin.Context) {
		id := c.Param("record_id")

		req := model.ManualUpdateStaffShift{}
		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		req.FuelVolume = req.EndingVolume - req.StartingVolume
		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("staff_shifts").Where("record_id = ?", id).Debug().Updates(req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}
