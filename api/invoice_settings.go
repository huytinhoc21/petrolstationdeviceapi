package api

import (
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/util"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Get invoice_settings godoc
//
//	@Summary		Get a list of invoice_settings [OK]
//	@Description	Return a list of invoice_settings by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			InvoiceSettings
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResInvoiceSetting
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/invoice_settings [get]
func getInvoiceSettings(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		rowData := database.InvoiceSetting{}
		result := db.Table("invoice_settings")
		result = util.GetFilteredDB(result, c, rowData)

		var invoice_settings []model.ResInvoiceSetting
		// Get all records
		deleted := c.Query("deleted")
		if deleted == "true" {
			result = result.Unscoped().Where("deleted_at is not null").Find(&invoice_settings)
		} else {
			result = result.Find(&invoice_settings)
		}

		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, invoice_settings)

	})
}

// Get a invoice_setting godoc
//
//	@Summary		Get a invoice_setting [OK]
//	@Description	Return a invoice_setting if successful
//	@Tags			InvoiceSettings
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"InvoiceSetting id"
//	@Success		200	{object}	model.ResInvoiceSetting
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/invoice_settings/{id} [get]
func getAInvoiceSetting(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:id", func(c *gin.Context) {

		id := c.Param("id")
		var invoice_setting model.ResInvoiceSetting
		// Get all records
		result := db.Table("invoice_settings").First(&invoice_setting, "id = ?", id)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, invoice_setting)

	})
}

// Post a invoice_setting godoc
//
//	@Summary		Post a invoice_setting to database [OK]
//	@Description	Return a id if successful
//	@Tags			InvoiceSettings
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			InvoiceSetting	body		model.ReqInvoiceSetting	true	"New invoice_setting data"
//	@Success		200				{string}	string					"Inserted Unit ID"
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/invoice_settings [post]
func postAInvoiceSetting(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {
		req := model.ReqInvoiceSetting{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		result := db.Table("invoice_settings").Create(&req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, req.ID)

	})
}

// Update a invoice_setting godoc
//
//	@Summary		Update a invoice_setting [OK]
//	@Description	Returns updated rows
//	@Tags			InvoiceSettings
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id				path		string							true	"InvoiceSetting id"
//	@Param			InvoiceSetting	body		model.ReqPatchInvoiceSetting	true	"InvoiceSetting data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200				{integer}	uint							"The number of updated rows"
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/invoice_settings/{id} [patch]
func updateAInvoiceSetting(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqPatchInvoiceSetting{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		result := db.Table("invoice_settings").Where("id = ?", id).Update("auto_export", req.AutoExport)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		result = db.Table("invoice_settings").Where("id = ?", id).Updates(&req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		// if req.AutoExport {
		// 	go func() {
		// 		util.CreateOrUpdateBkavInvoiceOnStart(db, req.GasStationID)
		// 	}()
		// }

		c.JSON(200, result.RowsAffected)
	})
}

// Delete a invoice_setting godoc
//
//	@Summary		Delete a invoice_setting [OK]
//	@Description	Returns deleted rows
//	@Tags			InvoiceSettings
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"InvoiceSetting id"
//	@Success		200	{integer}	uint	"The number of deleted rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/invoice_settings/{id} [delete]
func deleteAInvoiceSetting(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		result := db.Where("id = ?", id).Delete(&database.InvoiceSetting{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}
