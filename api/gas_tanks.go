package api

import (
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/util"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Get gas_tanks godoc
//
//	@Summary		Get a list of gas_tanks [OK]
//	@Description	Return a list of gas_tanks by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			GasTanks
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResGasTank{status_data=model.GasTankStatus,gas_pumps=[]model.NameAndID,good=model.NameAndID}
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/gas_tanks [get]
func getGasTanks(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		rowData := database.GasTank{}
		result := db.Table("gas_tanks")
		result = util.GetFilteredDB(result, c, rowData)

		var gas_tanks []model.ResGasTank
		// Get all records
		deleted := c.Query("deleted")
		if deleted == "true" {
			result = result.Unscoped().Where("deleted_at is not null")
		}

		result = result.Select("gas_tanks.*, json_build_object('id', goods.id, 'name', goods.name) as good,json_agg(json_build_object('id', gas_pumps.id, 'name', gas_pumps.name)) as gas_pumps").
			Joins("left join gas_pumps on gas_tank_id = gas_tanks.id").
			Joins("left join goods on good_id = goods.id").
			Group("gas_tanks.id, goods.id").Find(&gas_tanks)

		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, gas_tanks)

	})
}

// Get a gas_tank godoc
//
//	@Summary		Get a gas_tank [OK]
//	@Description	Return a gas_tank if successful
//	@Tags			GasTanks
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"GasTank id"
//	@Success		200	{object}	model.ResGasTank{status_data=model.GasTankStatus}
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/gas_tanks/{id} [get]
func getAGasTank(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:id", func(c *gin.Context) {

		id := c.Param("id")
		var gas_tank model.ResGasTank
		// Get all records
		result := db.Table("gas_tanks").First(&gas_tank, "id = ?", id)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, gas_tank)

	})
}

// Post a gas_tank godoc
//
//	@Summary		Post a gas_tank to database [OK]
//	@Description	Return a id if successful
//	@Tags			GasTanks
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			GasTank	body		model.ReqGasTank{status_data=model.GasTankStatus}	true	"New gas_tank data"
//	@Success		200		{string}	string												"Inserted Unit ID"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/gas_tanks [post]
func postAGasTank(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {
		req := model.ReqGasTank{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		result := db.Table("gas_tanks").Create(&req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, req.ID)

	})
}

// Update a gas_tank godoc
//
//	@Summary		Update a gas_tank [OK]
//	@Description	Returns updated rows
//	@Tags			GasTanks
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id		path		string													true	"GasTank id"
//	@Param			GasTank	body		model.ReqPatchGasTank{status_data=model.GasTankStatus}	true	"GasTank data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200		{integer}	uint													"The number of updated rows"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/gas_tanks/{id} [patch]
func updateAGasTank(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqPatchGasTank{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("gas_tanks").Where("id = ?", id).Updates(req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Delete a gas_tank godoc
//
//	@Summary		Delete a gas_tank [OK]
//	@Description	Returns deleted rows
//	@Tags			GasTanks
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"GasTank id"
//	@Success		200	{integer}	uint	"The number of deleted rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/gas_tanks/{id} [delete]
func deleteAGasTank(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		result := db.Where("id = ?", id).Delete(&database.GasTank{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Update pumps for a gas tank godoc
//
//	@Summary		Update pumps for a gas tank [OK]
//	@Description	Returns updated pumps
//	@Tags			GasTanks
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id		path		string					true	"GasTank id"
//	@Param			IDArray	body		[]model.ReqDeleteMany	true	"Pump ids array."
//	@Success		200		{integer}	uint					"The number of updated pumps"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/gas_tanks/{id}/pumps [patch]
func updatePumpsForGasTank(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id/pumps", func(c *gin.Context) {
		id := c.Param("id")

		req := []model.ReqDeleteMany{}
		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		tx := db.Begin()

		result := tx.Table("gas_pumps").Where("gas_tank_id = ?", id).Update("gas_tank_id", gorm.Expr("NULL"))
		if result.Error != nil {
			tx.Rollback()
			returnError(c, 400, result.Error.Error())
			return
		}

		ids := []string{}
		for _, v := range req {
			ids = append(ids, v.ID)
		}
		result = tx.Table("gas_pumps").Where("id in ?", ids).Update("gas_tank_id", id)

		if result.Error != nil {
			tx.Rollback()
			returnError(c, 400, result.Error.Error())
			return
		}

		tx.Commit()

		c.JSON(200, result.RowsAffected)
	})
}
