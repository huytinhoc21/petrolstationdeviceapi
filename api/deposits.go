package api

import (
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/types"
	"time"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Get deposits godoc
//
//	@Summary		Get a list of deposits [OK]
//	@Description	Return a list of deposits by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			Deposits
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			from	query		string	false	"If provided, statistics will be calculated from the 'from' date. Dates provided must be in the following format: 2023-08-20"
//	@Param			to		query		string	false	"If provided, statistics will be calculated as of the 'to' date. Dates provided must be in the following format: 2023-08-20"
//	@Success		200		{array}		model.ResDeposit
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/deposits [get]
func getDeposits(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		from := c.Query("from")
		to := c.Query("to")
		result := db.Table("deposits as dp")

		if from != "" {
			result = result.Where("dp.created_at >= ?", from)
		}

		if to != "" {
			result = result.Where("dp.created_at <= ?", to)
		}

		var res []struct {
			ID                   int            `json:"id"`
			CustomerID           uint           `json:"customer_id"`
			CreatedAt            time.Time      `json:"created_at"`
			ProcessedTransaction int            `json:"processed_transaction"`
			PendingTransaction   int            `json:"pending_transaction"`
			CashierData          types.JsonPsql `json:"cashier_data" `
			CustomerName         string         `json:"customer_name"`
			AccumulatedAmount    float64        `json:"accumulated_amount"`
			ProcessedAmount      float64        `json:"processed_amount"`
			TransferContent      string         `json:"transfer_content"`
			Amount               int64          `json:"amount" gorm:"not null"`
			Status               string         `json:"status"`
		}

		result = result.Debug().
			Select(`
			dp.*,
			count(transactions.id) as processed_transaction,
			(select count(transactions.id) from transactions 
				where transactions.created_at < dp.created_at 
					and created_at >= current_date - interval '1 week'
					and customer_id = dp.customer_id
					and status = 'Chờ chuyển khoản' 
					and deposit_id is null) as pending_transaction,
			json_build_object('id', users.id, 'name', users.name) as cashier_data,
			customers.name as customer_name,
			(select sum(deposits.amount) from deposits where customer_id = customers.id and created_at < dp.created_at) as accumulated_amount,
			sum(transactions.total_revenue) as processed_amount`).
			Joins("left join customers on customers.id = dp.customer_id").
			Joins("left join users on users.id = dp.cashier").
			Joins("left join transactions on transactions.deposit_id = dp.id and transactions.created_at < dp.created_at").
			Group("dp.id, users.id, customers.id").Scan(&res)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, res)

	})
}

// Get accounts receivable godoc
//
//	@Summary		Get a list of accounts receivable [OK]
//	@Description	Return a list of accounts receivable by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			Deposits
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			gas_station_id	query		string	false	"Filter by gas station id"
//	@Param			from			query		string	true	"If provided, statistics will be calculated from the 'from' date. Dates provided must be in the following format: 2023-08-20"
//	@Param			to				query		string	true	"If provided, statistics will be calculated as of the 'to' date. Dates provided must be in the following format: 2023-08-20"
//	@Success		200				{array}		model.ResDeposit
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/deposits/accounts_receivable [get]
func getAccountsReceivable(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/accounts_receivable", func(c *gin.Context) {

		from := c.Query("from")
		to := c.Query("to")
		result := db.Table("customers as ctm")
		gasStationID := c.Query("gas_station_id")
		if gasStationID != "" {
			result = result.Where("ctm.gas_station_id = ?", gasStationID)
		}

		if from == "" || to == "" {
			returnError(c, 400, "Cần cung cấp thời gian bắt đầu và kêt thúc")
			return
		}
		var res []map[string]interface{}

		result = result.
			Select(`
			debt_norm,
			ctm.id as customer_id,
			ctm.name as customer_name,
			(select sum(amount)
			from deposits 
			where deposits.created_at < ? and deposits.customer_id = ctm.id
			) + COALESCE(ctm.initial_deposit,0) as accumulated_deposit,
			(select sum(total_revenue)
			from transactions
			where transactions.created_at < ? and transactions.customer_id = ctm.id and payment_type = 'Chuyển khoản'
			) + COALESCE(ctm.initial_debt,0) as accumulated_amount_spent,
			(
			select sum(amount)
				from deposits
				where deposits.created_at between ? and ? and deposits.customer_id = ctm.id
			) as deposit,
			(
			select sum(total_revenue)
				from transactions
				where transactions.created_at between ? and ? and transactions.customer_id = ctm.id  and payment_type = 'Chuyển khoản'
			) as amount_spent
			`, from, from, from, to, from, to).
			Group("ctm.id").Scan(&res)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		if res == nil {
			res = []map[string]interface{}{}
		}

		c.JSON(200, res)

	})
}

// Get accounts receivable godoc
//
//	@Summary		Get a list of accounts receivable [OK]
//	@Description	Return a list of accounts receivable by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			Deposits
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id		path		string					true	"Deposit id"
//	@Param			IDArray	body		[]model.ReqDeleteMany	true	"Transaction ids"
//	@Success		200		{integer}	uint					"The number of updated transaction rows"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/deposits/{id}/receive-transactions [post]
func receiveTransactions(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("/:id/receive-transactions", func(c *gin.Context) {

		req := []model.ReqDeleteMany{}
		depositID := c.Param("id")

		deposit := database.Deposit{}

		if result := db.Table("deposits").Select("customer_id").Where("id = ?", depositID).Scan(&deposit); result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		// var res int64 = 0
		for _, v := range req {
			db.Table("transactions").Where("id = ? and customer_id = ? and status = 'Chờ chuyển khoản'", v.ID, deposit.CustomerID).Updates(map[string]interface{}{"deposit_id": depositID, "status": "Đã chuyển khoản"})
			// if result.Error == nil {
			// 	res += result.RowsAffected
			// }

		}

		var res struct {
			ID                   int            `json:"id"`
			CustomerID           uint           `json:"customer_id"`
			CreatedAt            time.Time      `json:"created_at"`
			ProcessedTransaction int            `json:"processed_transaction"`
			PendingTransaction   int            `json:"pending_transaction"`
			CashierData          types.JsonPsql `json:"cashier_data" gorm:"type:json"`
			CustomerName         string         `json:"customer_name"`
			AccumulatedAmount    float64        `json:"accumulated_amount"`
			ProcessedAmount      float64        `json:"processed_amount"`
			TransferContent      string         `json:"transfer_content"`
			Amount               int64          `json:"amount" gorm:"not null"`
		}

		result := db.Table("deposits as dp").
			Select(`
			dp.*,
			count(transactions.id) as processed_transaction,
			(select count(transactions.id) from transactions 
				where transactions.created_at < dp.created_at and customer_id = dp.customer_id and status = 'Chờ chuyển khoản' and deposit_id is null) as pending_transaction,
			json_build_object('id', users.id, 'name', users.name) as cashier_data,
			customers.name as customer_name,
			(select sum(deposits.amount) from deposits where customer_id = customers.id and created_at < dp.created_at) as accumulated_amount,
			sum(transactions.total_revenue) as processed_amount`).
			Joins("left join customers on customers.id = dp.customer_id").
			Joins("left join users on users.id = dp.cashier").
			Joins("left join transactions on transactions.deposit_id = dp.id and transactions.created_at < dp.created_at").
			Where("dp.id = ?", depositID).
			Group("dp.id, users.id, customers.id").Scan(&res)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, res)

	})
}

// Get a deposit godoc
//
//	@Summary		Get a deposit [OK]
//	@Description	Return a deposit if successful
//	@Tags			Deposits
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Deposit id"
//	@Success		200	{object}	model.ResDeposit
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/deposits/{id} [get]
func getADeposit(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:id", func(c *gin.Context) {

		id := c.Param("id")
		var deposit model.ResDeposit
		// Get all records
		result := db.Table("deposits").First(&deposit, "id = ?", id)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, deposit)

	})
}

// Post a deposit godoc
//
//	@Summary		Post a deposit to database [OK]
//	@Description	Return the number of inserted rows if successful
//	@Tags			Deposits
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			Deposit	body		[]model.ReqDeposit	true	"New deposit data"
//	@Success		200		{string}	string				"The number of inserted rows i"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/deposits [post]
func postADeposit(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {
		req := []model.ReqDeposit{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		insertData := []database.Deposit{}
		for _, v := range req {
			insertData = append(insertData, database.Deposit{
				ID:              v.ID,
				CustomerID:      v.CustomerID,
				TransferContent: v.TransferContent,
				Amount:          v.Amount,
				Cashier:         v.CashierID,
				CreatedAt:       v.CreatedAt,
			})
		}
		// if err := CheckInputError(req); err != nil {
		// 	returnError(c, 400, err.Error())
		// 	return
		// }

		result := db.Table("deposits").Create(&insertData)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		// Retrieve the IDs of the newly inserted rows
		ids := make([]uint, len(insertData))
		for i, deposit := range insertData {
			ids[i] = deposit.ID
		}

		c.JSON(200, gin.H{"ids": ids})

	})
}

// Update a deposit godoc
//
//	@Summary		Update a deposit [OK]
//	@Description	Returns updated rows
//	@Tags			Deposits
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id		path		string					true	"Deposit id"
//	@Param			Deposit	body		model.ReqPatchDeposit	true	"Deposit data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200		{integer}	uint					"The number of updated rows"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/deposits/{id} [patch]
func updateADeposit(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqPatchDeposit{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("deposits").Where("id = ?", id).Updates(req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Delete a deposit godoc
//
//	@Summary		Delete a deposit [OK]
//	@Description	Returns deleted rows
//	@Tags			Deposits
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Deposit id"
//	@Success		200	{integer}	uint	"The number of deleted rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/deposits/{id} [delete]
func deleteADeposit(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		result := db.Where("id = ?", id).Delete(&database.Deposit{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}
