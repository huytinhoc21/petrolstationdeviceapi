package api

import (
	"fmt"
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/util"
	"time"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"
)

// Get invoices godoc
//
//	@Summary		Get a list of invoices [OK]
//	@Description	Return a list of invoices by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			Invoices
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResInvoice
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/invoices [get]
func getInvoices(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		rowData := database.Invoice{}
		result := db.Table("invoices")
		result = util.GetFilteredDB(result, c, rowData)

		var invoices []model.ResInvoice
		// Get all records
		deleted := c.Query("deleted")
		if deleted == "true" {
			result = result.Unscoped().Where("deleted_at is not null").Find(&invoices)
		} else {
			result = result.Find(&invoices)
		}

		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, invoices)

	})
}

// Get a invoice godoc
//
//	@Summary		Get a invoice [OK]
//	@Description	Return a invoice if successful
//	@Tags			Invoices
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Invoice id"
//	@Success		200	{object}	model.ResInvoice
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/invoices/{id} [get]
func getAInvoice(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:id", func(c *gin.Context) {

		id := c.Param("id")
		var invoice model.ResInvoice
		// Get all records
		result := db.Table("invoices").First(&invoice, "id = ?", id)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, invoice)

	})
}

// Post a invoice godoc
//
//	@Summary		Post a invoice to database [OK]
//	@Description	Return a id if successful
//	@Tags			Invoices
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			Invoice	body		model.ReqInvoice	true	"New invoice data"
//	@Success		200		{string}	string				"Inserted Unit ID"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/invoices [post]
func postAInvoice(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {
		req := model.ReqInvoice{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		req.Draft = true

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		if req.TaxCode != "" && (req.Address == "" || req.Unit == "") {
			returnError(c, 400, "Thiếu thông tin")
			return
		}

		now := req.Date
		yesterday := now.AddDate(0, 0, -1)

		start := time.Date(yesterday.Year(), yesterday.Month(), yesterday.Day(), 0, 0, 0, 0, time.Local).Add(-7 * time.Hour)
		end := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.Local).Add(-7 * time.Hour)

		var count int64 = 0
		db.Debug().Table("transactions").
			Joins("join gas_pumps gp on transactions.gas_pump_id = gp.id").
			Joins("join gas_tanks gt on gp.gas_tank_id = gt.id").
			Where(`
		gt.gas_station_id = ? and
        created_at >= ? and created_at <= ? and deleted_at is null and 
        not exists (
            select 1 from invoices 
            where transaction_id = transactions.id and invoice_guid is not null
        )`, req.GasStationID, start, end).Count(&count)
		if count > 0 {
			returnError(c, 400, "Tồn tại giao dịch ngày cũ chưa được tạo hoá đơn")
			return
		}

		req.Type = "transaction-manual"
		var cnt int64 = 0
		db.Table("invoices").Where("transaction_id = ?", req.TransactionID).Count(&cnt)
		var result *gorm.DB
		if cnt == 0 {
			result = db.Table("invoices").Omit("gas_station_id").Create(&req)
		} else {
			result = db.Table("invoices").Omit("gas_station_id").
				Where("transaction_id = ?", req.TransactionID).Updates(&req)
		}

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		tx := db.Begin()

		invoice := database.Invoice{}
		tx.Table("invoices").Where("id = ?", req.ID).First(&invoice)
		objects, err := util.CreateOrUpdateInvoice(req, invoice.InvoiceGUID != "", &req.GasStationID, tx)

		invoiceDate := invoice.Date
		if invoice.TransactionID != &uuid.Nil {
			transaction := database.Transaction{}
			tx.Table("transactions").First(&transaction, "id = ?", invoice.TransactionID)
			invoiceDate = transaction.CreatedAt
		}

		if err != nil {
			tx.Rollback()
			updates := map[string]any{
				"invoice_msg": err.Error(),
			}
			db.Table("invoices").Where("id = ?", req.ID).Updates(updates)
			returnError(c, 400, err.Error())
			return
		}

		invoiceSetting := database.InvoiceSetting{}
		db.Table("invoice_settings").First(&invoiceSetting, "gas_station_id = ?", req.GasStationID)
		updates := map[string]any{
			"invoice_guid": objects[0].InvoiceGUID,
			"invoice_no":   objects[0].InvoiceNo,
			"scheduled":    invoiceDate.Add(time.Duration(invoiceSetting.WaitingTime) * time.Minute),
		}

		tx.Table("invoices").Where("id = ?", req.ID).Updates(updates)

		scheduled := invoiceDate.Add(time.Duration(invoiceSetting.WaitingTime) * time.Minute)
		invoice.InvoiceGUID = objects[0].InvoiceGUID
		invoice.InvoiceNo = objects[0].InvoiceNo
		invoice.Scheduled = &scheduled
		tx.Commit()
		c.JSON(200, invoice)
	})
}

// Update a invoice godoc
//
//	@Summary		Update a invoice [OK]
//	@Description	Returns updated rows
//	@Tags			Invoices
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id		path		string					true	"Invoice id"
//	@Param			Invoice	body		model.ReqPatchInvoice	true	"Invoice data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200		{integer}	uint					"The number of updated rows"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/invoices/{id} [patch]
func updateAInvoice(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqInvoice{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		req.Draft = true

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		if req.TaxCode != "" && (req.Address == "" || req.Unit == "") {
			returnError(c, 400, "Thiếu thông tin")
			return
		}

		tx := db.Begin()
		req.Type = "transaction-manual"
		result := tx.Table("invoices").Omit("gas_station_id").Where("id = ?", id).Updates(&req)

		if result.Error != nil {
			tx.Rollback()
			returnError(c, 400, result.Error.Error())
			return
		}

		invoice := database.Invoice{}
		tx.Table("invoices").Where("id = ?", req.ID).First(&invoice)
		objects, err := util.CreateOrUpdateInvoice(req, invoice.InvoiceGUID != "", &req.GasStationID, tx)

		invoiceDate := invoice.Date
		if invoice.TransactionID != &uuid.Nil {
			transaction := database.Transaction{}
			tx.Table("transactions").First(&transaction, "id = ?", invoice.TransactionID)
			invoiceDate = transaction.CreatedAt
		}

		if err != nil {
			tx.Rollback()
			updates := map[string]any{
				"invoice_msg": err.Error(),
			}
			db.Table("invoices").Where("id = ?", req.ID).Updates(updates)
			returnError(c, 400, err.Error())
			return
		}

		invoiceSetting := database.InvoiceSetting{}
		db.Table("invoice_settings").First(&invoiceSetting, "gas_station_id = ?", req.GasStationID)
		updates := map[string]any{
			"invoice_guid": objects[0].InvoiceGUID,
			"invoice_no":   objects[0].InvoiceNo,
			"scheduled":    invoiceDate.Add(time.Duration(invoiceSetting.WaitingTime) * time.Minute),
			"invoice_msg":  "",
		}
		fmt.Println("updates", updates)

		tx.Table("invoices").Where("id = ?", req.ID).Updates(updates)

		scheduled := invoiceDate.Add(time.Duration(invoiceSetting.WaitingTime) * time.Minute)
		invoice.InvoiceGUID = objects[0].InvoiceGUID
		invoice.InvoiceNo = objects[0].InvoiceNo
		invoice.Scheduled = &scheduled
		invoice.InvoiceMsg = ""
		tx.Commit()
		c.JSON(200, invoice)
	})
}

// Delete a invoice godoc
//
//	@Summary		Delete a invoice [OK]
//	@Description	Returns deleted rows
//	@Tags			Invoices
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Invoice id"
//	@Success		200	{integer}	uint	"The number of deleted rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/invoices/{id} [delete]
func deleteAInvoice(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		result := db.Where("id = ?", id).Delete(&database.Invoice{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		command := []database.CommandObject301{{
			PartnerInvoiceStringID: id,
		}}

		type InputGood struct {
			GoodName     string    `json:"good_name"`
			Vat          int       `json:"vat"`
			GasStationID uuid.UUID `json:"gas_station_id" gorm:"type:uuid;unique"`
		}
		var good InputGood
		db.Table("invoices").
			Select(`goods.name as good_name, goods.vat as vat, gas_tanks.gas_station_id`).
			Joins("left join transactions on invoices.transaction_id = transactions.id").
			Joins("left join gas_pumps on transactions.gas_pump_id = gas_pumps.id").
			Joins("left join gas_tanks on gas_pumps.gas_tank_id = gas_tanks.id").
			Joins("left join goods on gas_tanks.good_id = goods.id").
			Where("invoices.id = ?", id).
			Find(&good)
		if good.GasStationID != uuid.Nil {
			util.BKAV_Request(301, command, good.GasStationID)
		}
		c.JSON(200, result.RowsAffected)
	})
}
