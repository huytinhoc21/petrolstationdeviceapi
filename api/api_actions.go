package api

import (
	"petrol-station/database"
	"petrol-station/model"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Get api_actions godoc
//
//	@Summary		Get a list of api_actions [OK]
//	@Description	Return a list of api_actions by giving a filter
//	@Tags			ApiActions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResApiAction
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/api_actions [get]
func getApiActions(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		var api_actions []model.ResApiAction
		// Get all records
		result := db.Table("api_actions").Find(&api_actions)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, api_actions)

	})
}

// Get a api_action godoc
//
//	@Summary		Get a api_action [OK]
//	@Description	Return a api_action if successful
//	@Tags			ApiActions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"ApiAction id"
//	@Success		200	{object}	model.ResApiAction
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/api_actions/{id} [get]
func getAApiAction(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:id", func(c *gin.Context) {

		id := c.Param("id")
		var api_action model.ResApiAction
		// Get all records
		result := db.Table("api_actions").First(&api_action, "id = ?", id)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, api_action)

	})
}

// Post a api_action godoc
//
//	@Summary		Post a api_action to database [OK]
//	@Description	Return a id if successful
//	@Tags			ApiActions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			ApiAction	body		model.ReqApiAction	true	"New api_action data"
//	@Success		200			{string}	string				"Inserted Unit ID"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/api_actions [post]
func postAApiAction(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {
		req := model.ReqApiAction{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		result := db.Table("api_actions").Create(&req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, req.ID)

	})
}

// Update a api_action godoc
//
//	@Summary		Update a api_action [OK]
//	@Description	Returns updated rows
//	@Tags			ApiActions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id			path		string					true	"ApiAction id"
//	@Param			ApiAction	body		model.ReqPatchApiAction	true	"ApiAction data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200			{integer}	uint					"The number of updated rows"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/api_actions/{id} [patch]
func updateAApiAction(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqPatchApiAction{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("api_actions").Where("id = ?", id).Updates(req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Delete a api_action godoc
//
//	@Summary		Delete a api_action [OK]
//	@Description	Returns deleted rows
//	@Tags			ApiActions
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"ApiAction id"
//	@Success		200	{integer}	uint	"The number of deleted rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/api_actions/{id} [delete]
func deleteAApiAction(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		result := db.Where("id = ?", id).Delete(&database.ApiAction{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}
