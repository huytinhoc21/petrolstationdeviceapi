package api

import (
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/util"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Get staffs godoc
//
//	@Summary		Get a list of staffs [OK]
//	@Description	Return a list of staffs by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			Staffs
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResStaff
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/staffs [get]
func getStaffs(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		rowData := database.Staff{}
		result := db.Table("staffs")
		result = util.GetFilteredDB(result, c, rowData)

		var staffs []model.ResStaff
		// Get all records
		deleted := c.Query("deleted")
		if deleted == "true" {
			result = result.Unscoped().Where("deleted_at is not null").Find(&staffs)
		} else {
			result = result.Find(&staffs)
		}

		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, staffs)

	})
}

// Get a staff godoc
//
//	@Summary		Get a staff [OK]
//	@Description	Return a staff if successful
//	@Tags			Staffs
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Staff id"
//	@Success		200	{object}	model.ResStaff
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/staffs/{id} [get]
func getAStaff(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:id", func(c *gin.Context) {

		id := c.Param("id")
		var staff model.ResStaff
		// Get all records
		result := db.Table("staffs").First(&staff, "id = ?", id)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, staff)

	})
}

// Post a staff godoc
//
//	@Summary		Post a staff to database [OK]
//	@Description	Return a id if successful
//	@Tags			Staffs
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			Staff	body		model.ReqStaff	true	"New staff data"
//	@Success		200		{string}	string			"Inserted Unit ID"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/staffs [post]
func postAStaff(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {
		req := model.ReqStaff{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		result := db.Table("staffs").Create(&req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, req.ID)

	})
}

// Update a staff godoc
//
//	@Summary		Update a staff [OK]
//	@Description	Returns updated rows
//	@Tags			Staffs
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id		path		string				true	"Staff id"
//	@Param			Staff	body		model.ReqPatchStaff	true	"Staff data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200		{integer}	uint				"The number of updated rows"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/staffs/{id} [patch]
func updateAStaff(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqPatchStaff{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("staffs").Where("id = ?", id).Updates(req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Delete a staff godoc
//
//	@Summary		Delete a staff [OK]
//	@Description	Returns deleted rows
//	@Tags			Staffs
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Staff id"
//	@Success		200	{integer}	uint	"The number of deleted rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/staffs/{id} [delete]
func deleteAStaff(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		result := db.Where("id = ?", id).Delete(&database.Staff{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}