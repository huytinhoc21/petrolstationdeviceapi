package api

import (
	"errors"
	"petrol-station/model"
	"petrol-station/util"
	"time"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Dang An
// Login Callback Flow
// Authenticator
// PayloadFunc
// LoginResponse

// MiddlewareFunc Callback Flow (Loggined)
// IdentityHandler
// Authorizator

// Logout Request flow (using LogoutHandler)
// LogoutResponse

// Refresh Request flow (using RefreshHandler)
// RefreshResponse

// Failures with logging in, bad tokens, or lacking privileges
// Unauthorized

// the jwt middleware
var getAuthMiddleware = func(db *gorm.DB) (*jwt.GinJWTMiddleware, error) {
	return jwt.New(&jwt.GinJWTMiddleware{
		Realm:      "ZoneV1",
		Key:        util.JWT_SECRET,
		Timeout:    time.Hour * 24 * 30 * 6,
		MaxRefresh: time.Hour * 24 * 30 * 6,
		// IdentityKey: identityKey,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*model.UserDataRes); ok {
				return jwt.MapClaims{
					"ID":   v.ID,
					"Name": v.Name,
					// "Phone": v.Phone,
					// "Email":  v.Email,
					// "Avatar": v.Avatar,
					"Role": v.Role,
				}
			}
			return jwt.MapClaims{}
		},
		// The default of this function is likely sufficient for your needs.
		//The purpose of this function is to fetch the user identity from
		//claims embedded within the jwt token, and pass this identity value to Authorizator.
		//This function assumes [IdentityKey: some_user_identity] is one of the attributes
		//embedded within the claims of the jwt token (determined by PayloadFunc).
		// IdentityHandler: func(c *gin.Context) interface{} {
		// 	claims := jwt.ExtractClaims(c)
		// 	return &User{
		// 		UserName: claims[identityKey].(string),
		// 	}
		// },
		Authenticator: func(c *gin.Context) (interface{}, error) {
			req := model.ReqLogin{}
			c.Bind(&req)
			if err := CheckInputError(req); err != nil {
				return nil, err
			}
			user := model.UserDataRes{}
			err := (func() error {

				// Dang An: Login with username and password
				db.Raw(`select id, name, username, email, phone, role from users where username=? and password=?`,
					req.Username, req.Password).Scan(&user)

				// Dang An: Or login with email and password
				if user.ID == "" {
					db.Raw(`select id, name, username, email, phone, role from users where email=? and password=?`,
						req.Username, req.Password).Scan(&user)
				}
				if user.ID == "" {
					return errors.New("thông tin đăng nhập sai")
				}

				// Dang An: Temporarily, Qlead does not have the user role
				// if !U.IsUserInRole(user.ID, req.Role) {
				// 	return errors.New("wrong role")
				// }
				return nil
			})()
			if err != nil {
				return nil, err
			}
			// token, err := U.GenerateToken(user.ID, user.Username, user.Password)
			if err != nil {
				return nil, err
			}
			c.Set("loginedUser", user)
			return &user, nil
		},
		LoginResponse: func(c *gin.Context, code int, token string, expire time.Time) {
			userp, _ := c.Get("loginedUser")
			user := userp.(model.UserDataRes)
			c.JSON(code, model.ResLogin{
				Code:   code,
				Token:  token,
				Expire: expire.Format(time.RFC3339),
				Data:   user,
			})
		},
		// Authorizator: func(data interface{}, c *gin.Context) bool {
		// 	// if v, ok := data.(*User); ok && v.UserName == "admin" {
		// 	// 	return true
		// 	// }

		// 	return true
		// },
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		// TokenLookup is a string in the form of "<source>:<name>" that is used
		// to extract token from the request.
		// Optional. Default value "header:Authorization".
		// Possible values:
		// - "header:<name>"
		// - "query:<name>"
		// - "cookie:<name>"
		// - "param:<name>"
		TokenLookup: "header: Authorization, query: token, cookie: jwt",
		// TokenLookup: "query:token",
		// TokenLookup: "cookie:token",

		// TokenHeadName is a string in the header. Default value is "Bearer"
		TokenHeadName: "Bearer",

		// TimeFunc provides the current time. You can override it to use another time value. This is useful for testing or if your server uses a different time zone than your tokens.
		TimeFunc: time.Now,
	})
}
