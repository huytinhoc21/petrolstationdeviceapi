package api

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	cors "github.com/itsjamie/gin-cors"
	"gorm.io/gorm"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"petrol-station/database"
	"petrol-station/util"
)

type Routes struct {
	Path    string
	Handler []func(*gin.RouterGroup, *gorm.DB)
}

var routes = []Routes{

	{"/users", []func(*gin.RouterGroup, *gorm.DB){updateAUser, getUsers, getUserInfoHandler}},
	{"/permissions", []func(*gin.RouterGroup, *gorm.DB){getAPermission, getPermissions, postAPermission, updateAPermission, deleteAPermission}},
	{"/user_permissions", []func(*gin.RouterGroup, *gorm.DB){getAUserPermission, getUserPermissions, postUserPermission, updateAUserPermission, deleteAUserPermission, getAUserPermissionByPermissionID, getAUserPermissionByUserID}},
	{"/api_actions", []func(*gin.RouterGroup, *gorm.DB){getAApiAction, getApiActions, postAApiAction, updateAApiAction, deleteAApiAction}},
	{"/api_action_permissions", []func(*gin.RouterGroup, *gorm.DB){getAApiActionPermission, getApiActionPermissions, postApiActionPermission, updateAApiActionPermission, updateApiActionPermissionByApiAction, deleteAApiActionPermission, getAApiActionPermissionByApiActionID, getAApiActionPermissionByPermissionID}},
	{"/gas_stations", []func(*gin.RouterGroup, *gorm.DB){deleteGasStations, getGasStationInformation, getAGasStation, getGasStations, postAGasStation, updateAGasStation, deleteAGasStation}},
	{"/gas_tanks", []func(*gin.RouterGroup, *gorm.DB){updatePumpsForGasTank, getAGasTank, getGasTanks, postAGasTank, updateAGasTank, deleteAGasTank}},
	{"/gas_pumps", []func(*gin.RouterGroup, *gorm.DB){getAGasPump, getGasPumps, postAGasPump, updateAGasPump, deleteAGasPump}},
	{"/goods", []func(*gin.RouterGroup, *gorm.DB){getAGood, getGoods, postAGood, updateAGood, deleteAGood}},
	{"/transactions", []func(*gin.RouterGroup, *gorm.DB){restoreATransaction, createATransactionInvoice, postTransactions, getTransactionsByDay, getTransactionsByStation, getTransactionsByCustomer, getTransactionsByGood, getTransactionsByPump, getTransactionStatisticsData, getATransaction, getTransactions, postATransaction, updateATransaction, deleteATransaction}},
	{"/customers", []func(*gin.RouterGroup, *gorm.DB){getACustomer, getCustomers, postACustomer, postCustomers, updateACustomer, deleteACustomer}},
	{"/staffs", []func(*gin.RouterGroup, *gorm.DB){getAStaff, getStaffs, postAStaff, updateAStaff, deleteAStaff}},
	{"/staff_shifts", []func(*gin.RouterGroup, *gorm.DB){updateEndingRevStaffShift, restoreAStaffShift, tmpCheatStaffShift, postStaffShift, getStaffShiftStatistic, getAStaffShift, getStaffShifts, postAStaffShift, updateAStaffShift, deleteAStaffShift}},
	{"/shifts", []func(*gin.RouterGroup, *gorm.DB){getAShift, getShifts, postAShift, updateAShift, deleteAShift}},
	{"/staff_shift_infos", []func(*gin.RouterGroup, *gorm.DB){getAStaffShiftInfo, getStaffShiftInfos, postStaffShiftInfo, updateAStaffShiftInfo, deleteAStaffShiftInfo, getAStaffShiftInfoByStaffID, getAStaffShiftInfoByShiftID}},
	{"/deposits", []func(*gin.RouterGroup, *gorm.DB){receiveTransactions, getAccountsReceivable, getADeposit, getDeposits, postADeposit, updateADeposit, deleteADeposit}},
	{"/providers", []func(*gin.RouterGroup, *gorm.DB){getAProvider, getProviders, postAProvider, updateAProvider, deleteAProvider}},
	{"/fuel_imports", []func(*gin.RouterGroup, *gorm.DB){getImportStatistics, getAFuelImport, getFuelImports, postAFuelImport, updateAFuelImport, deleteAFuelImport}},
	{"/user_gas_stations", []func(*gin.RouterGroup, *gorm.DB){getAUserGasStation, getUserGasStations, postUserGasStation, updateAUserGasStation, deleteAUserGasStation, getAUserGasStationByUserID, getAUserGasStationByGasStationID}},
	{"/invoice_settings", []func(*gin.RouterGroup, *gorm.DB){getAInvoiceSetting, getInvoiceSettings, postAInvoiceSetting, updateAInvoiceSetting, deleteAInvoiceSetting}},
	{"/invoices", []func(*gin.RouterGroup, *gorm.DB){getAInvoice, getInvoices, postAInvoice, updateAInvoice, deleteAInvoice}},
	{"/bkav", []func(*gin.RouterGroup, *gorm.DB){getInvoiceStatus, getBizData}},
	//API_MỚI
}

func RunServer(db *gorm.DB) {
	gin.SetMode(util.RUNNING_MODE)
	r := gin.Default()

	// // Add middleware to handle CORS headers

	corsConfig := cors.Config{
		Origins:         "*",
		Methods:         "GET, PUT, POST, DELETE, OPTIONS, PATCH",
		RequestHeaders:  "Origin, Authorization, Content-Type",
		ExposedHeaders:  "",
		MaxAge:          15 * time.Second,
		ValidateHeaders: false,
	}

	if util.RUNNING_MODE == gin.ReleaseMode {
		corsConfig.Origins = "https://smartpetro.vn, https://api-mc-petro.shub.edu.vn, http://localhost:3000, https://mc-petro.shub.edu.vn"
		corsConfig.Credentials = true
	}
	r.Use(cors.Middleware(corsConfig))

	authMiddleware, err := getAuthMiddleware(db)
	if err != nil {
		log.Fatal("JWT Error:" + err.Error())
	}
	// When you use jwt.New(), the function is already automatically called for checking,
	// which means you don't need to call it again.
	errInit := authMiddleware.MiddlewareInit()
	if errInit != nil {
		log.Fatal("authMiddleware.MiddlewareInit() Error:" + errInit.Error())
	}
	handleCheckVersion(r)
	v1 := r.Group("/api/v1")

	v1.Use(gin.LoggerWithConfig(gin.LoggerConfig{
		SkipPaths: []string{"/gas_tanks/*any", "/gas_pumps/*any"},
	}))

	// Dang An: Login with JWT Handler
	loginHandler(v1, authMiddleware.LoginHandler)
	registerHandler(v1, db)

	// if util.RUNNING_MODE == gin.ReleaseMode {
	v1.Use(authMiddleware.MiddlewareFunc())
	// }

	// Dang An: Loop all routes
	for _, route := range routes {
		// Dang An: Loop all controllers for each route
		for _, controller := range route.Handler {
			controller(v1.Group(route.Path), db)
		}
	}

	if util.RUNNING_MODE == gin.DebugMode {
		// Create or update missing inovice of transactions
		if false {
			go func() {
				invoiceSettings := []database.InvoiceSetting{}
				db.Table("invoice_settings").Find(&invoiceSettings)
				for _, invoiceSetting := range invoiceSettings {
					util.CreateOrUpdateBkavInvoiceOnStart(db, invoiceSetting.GasStationID)

				}
			}()
		}
		// Create missing invoice {
		if true {
			go func() {
				transactions := []database.Transaction{}
				db.Raw(`select t.* from transactions t
				join gas_pumps gp on t.gas_pump_id = gp.id
				join gas_tanks gt on gp.gas_tank_id = gt.id
				and t.deleted_at is null
				and t.created_at > '2024-04-15 17:00:00'
				and t.created_at < '2024-04-16 17:00:00'
				and not exists (
					select 1 from invoices where transaction_id = t.id
					and invoice_guid is not null
				) order by t.created_at asc`).
					Find(&transactions)

					// 		db.Raw(`select t.* from transactions t
					// join invoices i on i.transaction_id = t.id
					// join gas_pumps gp on t.gas_pump_id = gp.id
					// join gas_tanks gt on gp.gas_tank_id = gt.id
					// where t.created_at > '2024-04-07 17:00:00'
					// and t.created_at < '2024-04-08 17:00:00'
					// and i.invoice_guid is null
					// and i.type <> 'transaction-manual'`).Find(&transactions)

				fmt.Println("************** Create Missing invoice *****************")
				fmt.Println("len(transactions)", len(transactions))
				fmt.Println("*************************************************************")
				for index, transaction := range transactions {
					for i := 0; i < 10; i++ { // Retry 10 times
						_, err := util.CreateInvoiceFromTransaction(transaction, true, false, db)
						if err == nil {
							break
						}
						fmt.Println("err.Error()", err.Error())

						time.Sleep(300 * time.Millisecond)
					}
					time.Sleep(100 * time.Millisecond)
					fmt.Println(index, "/", len(transactions))

				}

			}()
		}
		// Re-check inovice
		if false {
			go func() {
				for {
					invoices := []database.Invoice{}

					db.Debug().Raw(`select i.*
					from
					  invoices i
					join transactions t on i.transaction_id = t.id
					join gas_pumps on t.gas_pump_id = gas_pumps.id
					join gas_tanks on gas_pumps.gas_tank_id = gas_tanks.id
					where i.id in(205600)`).Find(&invoices)

					// db.Debug().Raw(`select i.* from invoices i
					// join transactions t on i.transaction_id = t.id
					// join gas_pumps gp on t.gas_pump_id = gp.id
					// join gas_tanks gt on gp.gas_tank_id = gt.id
					// where gt.gas_station_id = '7962b3bf-7916-4256-a4e6-53e93344d87d'
					// and t.created_at > '2024-03-30' and t.created_at < '2024-03-31'
					// and i.invoice_guid is null`).Find(&invoices)

					// db.Debug().Raw(`select i.* from invoices i
					// 	join transactions t on i.transaction_id = t.id
					// 	where t.created_at >= '2024-03-20 17:00:00'
					// 		and t.created_at < '2024-03-26 17:00:00'
					// 		and i.date < '2024-03-27 00:00:00'
					// 		and i.invoice_guid is not null
					// 		and i.invoice_no is null
					// 		and i.note is null
					// order by t.created_at asc`).Find(&invoices)

					fmt.Println("************** Scan Invoice *****************")
					fmt.Println("len(invoices)", len(invoices))
					fmt.Println("*********************************************")

					count := 0
					for index, invoice := range invoices {
						if index >= 1000 {
							break
						}

						time.Sleep(400 * time.Millisecond)
						fmt.Println("CHECKING invoice.ID", invoice.ID)
						transaction := database.Transaction{}
						db.First(&transaction, "id = ?", invoice.TransactionID)

						_, err := util.CreateInvoiceFromTransaction(transaction, true, false, db)
						if err != nil {
							fmt.Println("err.Error()", err.Error())
						} else {
							fmt.Println("OK invoice.InvoiceNo", invoice.InvoiceNo)
						}
						break

						good := util.GetInputGood(transaction, db)
						bkavRes, err := util.BKAV_Request(800, database.CommandObject800(strconv.Itoa(int(invoice.ID))), good.GasStationID)
						if err != nil {
							fmt.Println("err.Error()", err.Error(), strings.Contains(err.Error(), "không tồn tại"))
							continue
						}
						objectJSON, _ := bkavRes["Object"].(string)

						objects, err := util.ParseBKAVObjectRes800(objectJSON)
						if err != nil {
							fmt.Println("err.Error()", err.Error())
							db.Table("invoices").Where("id = ?", invoice.ID).Delete(&invoice)
							if strings.Contains(err.Error(), "không tồn tại") {
								_, err := util.CreateInvoiceFromTransaction(transaction, false, false, db)
								if err != nil {
									fmt.Println("err.Error()", err.Error())
								} else {
									fmt.Println("OK invoice.InvoiceNo", invoice.InvoiceNo)
								}
								count++
							}
							continue
						}

						fmt.Println(count, "/", index, "/", len(invoices), "   OK Exist InvoiceNo", objects.Invoice.InvoiceNo, objects.Invoice.InvoiceStatusID)

						if objects.Invoice.InvoiceStatusID == 2 || objects.Invoice.InvoiceStatusID == 6 || objects.Invoice.InvoiceStatusID == 8 {
							result := db.Debug().Table("invoices").Where("id = ?", invoice.ID).Updates(
								map[string]any{
									"draft":      false,
									"invoice_no": objects.Invoice.InvoiceNo, "invoice_guid": objects.Invoice.InvoiceGUID,
									"note":        "Re-checked " + time.Now().Format("2006-01-02T15:04:05.000Z"),
									"invoice_msg": nil,
								},
							)
							if result.Error != nil {
								fmt.Println("err.Error()", result.Error.Error())
								continue
							}
						} else {
							result := db.Debug().Table("invoices").Where("id = ?", invoice.ID).
								Updates(map[string]any{
									"draft":        true,
									"invoice_no":   objects.Invoice.InvoiceNo,
									"invoice_guid": objects.Invoice.InvoiceGUID,
									"note":         "Re-checked " + time.Now().Format("2006-01-02T15:04:05.000Z"),
									"invoice_msg":  nil,
									"scheduled":    invoice.CreatedAt.Add(time.Duration(24) * time.Hour),
								})
							if result.Error != nil {
								fmt.Println("err.Error()", result.Error.Error())
								continue
							}
						}
					}
					fmt.Println("count", count)
					fmt.Println("len(invoices)", len(invoices))
					time.Sleep(5 * time.Minute)
				}
			}()
		}
	}

	// use ginSwagger middleware to serve the API docs
	r.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.Run(":" + util.PORT)
}

func returnError(c *gin.Context, code int, message string) {
	c.JSON(code, gin.H{
		"message": message,
	})
}

// Use a single instance of Validate, it caches struct info
var validate *validator.Validate

// Dang An: Check if the body of the request is a valid json
func CheckInputError(input interface{}) error {
	validate = validator.New()
	err := validate.Struct(input)
	if err == nil {
		return nil
	}

	// Dang An: If the err is a invalid err, return err
	if _, ok := err.(*validator.InvalidValidationError); ok {
		return err
	}

	// Dang An: If err is valid, we can output in more detail
	fields := []string{}
	for _, err := range err.(validator.ValidationErrors) {
		// Dang An: err.Namespace() can differ when a custom TagNameFunc is registered or not (In this case, we use the name from json tag)
		fields = append(fields, err.Namespace()+" "+err.Type().Name())
	}
	return fmt.Errorf("Error:Field validation for: %v", strings.Join(fields, ", "))
}
