package api

import (
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/util"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Get staff_shift_infos godoc
//
//	@Summary		Get a list of staff_shift_infos [OK]
//	@Description	Return a list of staff_shift_infos by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			StaffShiftInfos
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResStaffShiftInfo{cash=model.StaffShiftCash}
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/staff_shift_infos [get]
func getStaffShiftInfos(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		rowData := database.StaffShiftInfo{}
		result := db.Table("staff_shift_infos")
		result = util.GetFilteredDB(result, c, rowData)

		var staff_shift_infos []model.ResStaffShiftInfo
		// Get all records
		deleted := c.Query("deleted")
		if deleted == "true" {
			result = result.Unscoped().Where("deleted_at is not null").Find(&staff_shift_infos)
		} else {
			result = result.Find(&staff_shift_infos)
		}

		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, staff_shift_infos)

	})
}

// Get a staff_shift_info godoc
//
//	@Summary		Get a staff_shift_info [OK]
//	@Description	Return a staff_shift_info if successful
//	@Tags			StaffShiftInfos
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			staff-id	path		string	true	"Staff id"
//	@Param			shift-id	path		string	true	"Shift id"
//	@Param			date		path		string	true	"Date"
//	@Success		200			{object}	model.ResStaffShiftInfo{cash=model.StaffShiftCash}
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/staff_shift_infos/{staff-id}/{shift-id}/{date} [get]
func getAStaffShiftInfo(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:staff-id/:shift-id/:date", func(c *gin.Context) {

		staffID := c.Param("staff-id")
		shiftID := c.Param("shift-id")
		date := c.Param("date")

		var staff_shift_info model.ResStaffShiftInfo
		// Get all records
		result := db.Table("staff_shift_infos").
			Where("staff_id = ?", staffID).
			Where("shift_id = ?", shiftID).
			Where("date = ?", date).
			First(&staff_shift_info)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, staff_shift_info)

	})
}

// Get a staff_shift_info godoc
//
//	@Summary		Get a staff_shift_info [OK]
//	@Description	Return a staff_shift_info if successful
//	@Tags			StaffShiftInfos
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			staff-id	path		string	true	"Staff id"
//	@Success		200			{object}	model.ResStaffShiftInfo{cash=model.StaffShiftCash}
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/staff_shift_infos/{staff-id} [get]
func getAStaffShiftInfoByStaffID(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:staff-id", func(c *gin.Context) {

		staffID := c.Param("staff-id")

		var staff_shift_info []model.ResStaffShiftInfo

		// Get all records
		result := db.Table("staff_shift_infos").Where("staff_id = ?", staffID).Find(&staff_shift_info)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, staff_shift_info)

	})
}

// Get a staff_shift_info godoc
//
//	@Summary		Get a staff_shift_info [OK]
//	@Description	Return a staff_shift_info if successful
//	@Tags			StaffShiftInfos
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			shift-id	path		string	true	"Shift id"
//	@Success		200			{object}	model.ResStaffShiftInfo
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/staff_shift_infos/all/{shift-id} [get]
func getAStaffShiftInfoByShiftID(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/all/:shift-id", func(c *gin.Context) {

		shiftID := c.Param("shift-id")

		var staff_shift_info []model.ResStaffShiftInfo

		// Get all records
		result := db.Table("staff_shift_infos").Where("shift_id = ?", shiftID).Find(&staff_shift_info)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, staff_shift_info)

	})
}

// Post staff_shift_infos godoc
//
//	@Summary		Post staff_shift_infos to database [OK]
//	@Description	Return a list of staff_shift_infos if successful
//	@Tags			StaffShiftInfos
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			StaffShiftInfo	body		[]model.ReqStaffShiftInfo{cash=model.StaffShiftCash}	true	"New staff_shift_infos data"
//	@Success		200				{array}		model.ReqStaffShiftInfo{cash=model.StaffShiftCash}		"Inserted data"
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/staff_shift_infos [post]
func postStaffShiftInfo(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {

		req := []model.ReqStaffShiftInfo{}
		res := []model.ReqStaffShiftInfo{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		// if err := CheckInputError(req); err != nil {
		// 	returnError(c, 400, err.Error())
		// 	return
		// }

		tx := db.Begin()

		for _, v := range req {
			result := tx.Table("staff_shift_infos").Where(database.StaffShiftInfo{StaffID: v.StaffID, ShiftID: v.ShiftID}).Attrs(database.StaffShiftInfo{}).FirstOrCreate(&v)
			if result.Error != nil {
				tx.Rollback()
				returnError(c, 400, result.Error.Error())
				return
			}
			res = append(res, v)
		}

		tx.Commit()

		c.JSON(200, res)

	})
}

// Update a staff_shift_info godoc
//
//	@Summary		Update a staff_shift_info [OK]
//	@Description	Returns updated rows
//	@Tags			StaffShiftInfos
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			staff-id		path		string													true	"Staff id"
//	@Param			shift-id		path		string													true	"Shift id"
//	@Param			date			path		string													true	"Date"
//	@Param			StaffShiftInfo	body		model.ReqPatchStaffShiftInfo{cash=model.StaffShiftCash}	true	"StaffShiftInfo data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200				{integer}	uint													"The number of updated rows"
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/staff_shift_infos/{staff-id}/{shift-id}/{date} [patch]
func updateAStaffShiftInfo(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:staff-id/:shift-id/:date", func(c *gin.Context) {
		staffID := c.Param("staff-id")
		shiftID := c.Param("shift-id")
		date := c.Param("date")

		req := model.ReqPatchStaffShiftInfo{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("staff_shift_infos").
			Where("staff_id = ?", staffID).
			Where("shift_id = ?", shiftID).
			Where("date = ?", date).
			Updates(req)

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Delete a staff_shift_info godoc
//
//	@Summary		Delete a staff_shift_info [OK]
//	@Description	Returns deleted rows
//	@Tags			StaffShiftInfos
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			staff-id	path		string	true	"Staff id"
//	@Param			shift-id	path		string	true	"Shift id"
//	@Param			date		path		string	true	"date"
//	@Success		200			{integer}	uint	"The number of deleted rows"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/staff_shift_infos/{staff-id}/{shift-id}/{date} [delete]
func deleteAStaffShiftInfo(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:staff-id/:shift-id/:date", func(c *gin.Context) {
		staffID := c.Param("staff-id")
		shiftID := c.Param("shift-id")
		date := c.Param("date")

		result := db.Table("staff_shift_infos").
			Where("staff_id = ?", staffID).
			Where("shift_id = ?", shiftID).
			Where("date = ?", date).
			Delete(&database.StaffShiftInfo{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}
