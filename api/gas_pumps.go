package api

import (
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/util"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Get gas_pumps godoc
//
//	@Summary		Get a list of gas_pumps [OK]
//	@Description	Return a list of gas_pumps by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			GasPumps
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			gas_station_id	query		string	false	"Filter by gas station id"
//	@Success		200				{array}		model.ResGasPump{status_data=model.GasPumpStatus}
//	@Failure		400				{object}	model.HTTPError400
//	@Router			/gas_pumps [get]
func getGasPumps(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		rowData := database.GasPump{}
		result := db.Table("gas_pumps").Joins("left join gas_tanks on gas_tanks.id = gas_pumps.gas_tank_id")
		result = util.GetFilteredDB(result, c, rowData)

		var gas_pumps []model.ResGasPump
		// Get all records
		gasStationID := c.Query("gas_station_id")
		if gasStationID != "" {
			result = result.Where("gas_tanks.gas_station_id = ?", gasStationID)
		}

		deleted := c.Query("deleted")
		if deleted == "true" {
			result = result.Unscoped().Where("deleted_at is not null").Find(&gas_pumps)
		} else {
			result = result.Find(&gas_pumps)
		}

		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, gas_pumps)

	})
}

// Get a gas_pump godoc
//
//	@Summary		Get a gas_pump [OK]
//	@Description	Return a gas_pump if successful.
//	@Tags			GasPumps
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"GasPump id"
//	@Success		200	{object}	model.ResGasPump{status_data=model.GasPumpStatus}
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/gas_pumps/{id} [get]
func getAGasPump(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:id", func(c *gin.Context) {

		id := c.Param("id")
		var gas_pump model.ResGasPump
		// Get all records
		result := db.Table("gas_pumps").First(&gas_pump, "id = ?", id)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, gas_pump)

	})
}

// Post a gas_pump godoc
//
//	@Summary		Post a gas_pump to database [OK]
//	@Description	Return a id if successful.
//	@Tags			GasPumps
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			GasPump	body		model.ReqGasPump{status_data=model.GasPumpStatus}	true	"New gas_pump data"
//	@Success		200		{string}	string												"Inserted Unit ID"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/gas_pumps [post]
func postAGasPump(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {
		req := model.ReqGasPump{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		result := db.Table("gas_pumps").Create(&req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, req.ID)

	})
}

// Update a gas_pump godoc
//
//	@Summary		Update a gas_pump [OK]
//	@Description	Returns updated rows. The model of status_data is described by GasPumpStatus below.
//	@Tags			GasPumps
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id		path		string													true	"GasPump id"
//	@Param			GasPump	body		model.ReqPatchGasPump{status_data=model.GasPumpStatus}	true	"GasPump data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200		{integer}	uint													"The number of updated rows"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/gas_pumps/{id} [patch]
func updateAGasPump(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqPatchGasPump{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		// gp := database.GasPump{}
		// db.Table("gas_pumps").First(&gp, "id = ?", id)
		// prev := database.GasPumpRealTimeData{}
		// data, _ := gp.StatusData.MarshalJSON()
		// json.Unmarshal(data, &prev)

		// next := database.GasPumpRealTimeData{}
		// data, err := req.StatusData.MarshalJSON()
		// if err != nil {
		// 	fmt.Println("Real time data sai cấu trúc 1:", err.Error())
		// }
		// err = json.Unmarshal(data, &next)
		// if err != nil {
		// 	fmt.Println("Real time data sai cấu trúc 2:", err.Error())
		// }

		// if strings.Contains(next.GasType, "_") {
		// 	tx := model.ReqTransaction{}
		// 	tx.GasPumpID = uuid.FromStringOrNil(id)
		// 	tx.FuelVolume = next.FuelVolume
		// 	tx.UnitPrice = next.UnitPrice
		// 	tx.TotalRevenue = float64(next.TotalCost)
		// 	tx.CreatedAt = next.StationTime
		// 	tx.RecordID = "68-" + next.StationTime.String()
		// 	result := db.Table("transactions").Create(&req)
		// 	if result.Error != nil {
		// 		fmt.Println("Không thêm được force 7", result.Error.Error())
		// 	}
		// }

		result := db.Table("gas_pumps").Where("id = ?", id).Updates(req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		// next := database.GasPumpRealTimeData{}
		// data, _ = req.StatusData.MarshalJSON()
		// json.Unmarshal(data, &next)

		// if prev.Status == 6 && next.Status == 8 {
		// 	tx := model.ReqTransaction{}
		// 	tx.GasPumpID = uuid.FromStringOrNil(id)
		// 	tx.FuelVolume = next.FuelVolume
		// 	tx.UnitPrice = next.UnitPrice
		// 	tx.TotalRevenue = float64(next.TotalCost)
		// 	tx.CreatedAt = next.StationTime
		// 	tx.RecordID = "68-" + next.StationTime.String()
		// 	result := db.Table("transactions").Create(&req)
		// 	if result.Error != nil {
		// 		fmt.Println(result.Error.Error())
		// 		fmt.Println("***********************")
		// 	}
		// }

		c.JSON(200, result.RowsAffected)
	})
}

// Delete a gas_pump godoc
//
//	@Summary		Delete a gas_pump [OK]
//	@Description	Returns deleted rows
//	@Tags			GasPumps
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"GasPump id"
//	@Success		200	{integer}	uint	"The number of deleted rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/gas_pumps/{id} [delete]
func deleteAGasPump(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		result := db.Where("id = ?", id).Delete(&database.GasPump{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}
