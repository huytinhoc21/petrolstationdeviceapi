package api

import (
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/util"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Get customers godoc
//
//	@Summary		Get a list of customers [OK]
//	@Description	Return a list of customers by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			Customers
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResCustomer
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/customers [get]
func getCustomers(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		rowData := database.Customer{}
		result := db.Table("customers")
		result = util.GetFilteredDB(result, c, rowData)

		var customers []model.ResCustomer
		// Get all records
		deleted := c.Query("deleted")
		if deleted == "true" {
			result = result.Unscoped().Where("deleted_at is not null").Find(&customers)
		} else {
			result = result.Find(&customers)
		}

		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, customers)

	})
}

// Get a customer godoc
//
//	@Summary		Get a customer [OK]
//	@Description	Return a customer if successful
//	@Tags			Customers
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Customer id"
//	@Success		200	{object}	model.ResCustomer
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/customers/{id} [get]
func getACustomer(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:id", func(c *gin.Context) {

		id := c.Param("id")
		var customer model.ResCustomer
		// Get all records
		result := db.Table("customers").First(&customer, "id = ?", id)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, customer)

	})
}

// Post a customer godoc
//
//	@Summary		Post a customer to database [OK]
//	@Description	Return a id if successful
//	@Tags			Customers
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			Customer	body		model.ReqCustomer	true	"New customer data"
//	@Success		200			{string}	string				"Inserted Unit ID"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/customers [post]
func postACustomer(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {
		req := model.ReqCustomer{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		result := db.Table("customers").Create(&req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, req.ID)

	})
}

// Post a customer godoc
//
//	@Summary		Post multiple customers to database [OK]
//	@Description	Return an array of IDs if successful
//	@Tags			Customers
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			Customers	body		[]model.ReqCustomer	true	"Array of new customer data"
//	@Success		200			{object}	[]int				"Inserted customer IDs"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/customers [post]
func postCustomers(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("/many", func(c *gin.Context) {
		var customers []model.ReqCustomer

		if err := c.BindJSON(&customers); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		for _, customer := range customers {
			if err := CheckInputError(customer); err != nil {
				returnError(c, 400, err.Error())
				return
			}
		}

		// Use Batch Create for efficiency
		result := db.Table("customers").CreateInBatches(&customers, 10) // adjust batch size as needed
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		// Extract inserted IDs
		var ids []uint
		for _, customer := range customers {
			ids = append(ids, customer.ID)
		}

		c.JSON(200, ids)
	})
}

// Update a customer godoc
//
//	@Summary		Update a customer [OK]
//	@Description	Returns updated rows
//	@Tags			Customers
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id			path		string					true	"Customer id"
//	@Param			Customer	body		model.ReqPatchCustomer	true	"Customer data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200			{integer}	uint					"The number of updated rows"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/customers/{id} [patch]
func updateACustomer(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqPatchCustomer{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("customers").Where("id = ?", id).Updates(req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Delete a customer godoc
//
//	@Summary		Delete a customer [OK]
//	@Description	Returns deleted rows
//	@Tags			Customers
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Customer id"
//	@Success		200	{integer}	uint	"The number of deleted rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/customers/{id} [delete]
func deleteACustomer(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		result := db.Where("id = ?", id).Delete(&database.Customer{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}
