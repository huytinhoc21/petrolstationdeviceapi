package api

import (
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/util"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Get goods godoc
//
//	@Summary		Get a list of goods [OK]
//	@Description	Return a list of goods by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			Goods
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResGood
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/goods [get]
func getGoods(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		rowData := database.Good{}
		result := db.Table("goods")
		result = util.GetFilteredDB(result, c, rowData)

		var goods []model.ResGood
		// Get all records
		deleted := c.Query("deleted")
		if deleted == "true" {
			result = result.Unscoped().Where("deleted_at is not null").Find(&goods)
		} else {
			result = result.Find(&goods)
		}

		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, goods)

	})
}

// Get a good godoc
//
//	@Summary		Get a good [OK]
//	@Description	Return a good if successful
//	@Tags			Goods
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Good id"
//	@Success		200	{object}	model.ResGood
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/goods/{id} [get]
func getAGood(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:id", func(c *gin.Context) {

		id := c.Param("id")
		var good model.ResGood
		// Get all records
		result := db.Table("goods").First(&good, "id = ?", id)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, good)

	})
}

// Post a good godoc
//
//	@Summary		Post a good to database [OK]
//	@Description	Return a id if successful
//	@Tags			Goods
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			Good	body		model.ReqGood	true	"New good data"
//	@Success		200		{string}	string			"Inserted Unit ID"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/goods [post]
func postAGood(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {
		req := model.ReqGood{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		result := db.Table("goods").Create(&req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, req.ID)

	})
}

// Update a good godoc
//
//	@Summary		Update a good [OK]
//	@Description	Returns updated rows
//	@Tags			Goods
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id		path		string				true	"Good id"
//	@Param			Good	body		model.ReqPatchGood	true	"Good data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200		{integer}	uint				"The number of updated rows"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/goods/{id} [patch]
func updateAGood(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqPatchGood{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("goods").Where("id = ?", id).Updates(req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Delete a good godoc
//
//	@Summary		Delete a good [OK]
//	@Description	Returns deleted rows
//	@Tags			Goods
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Good id"
//	@Success		200	{integer}	uint	"The number of deleted rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/goods/{id} [delete]
func deleteAGood(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		result := db.Where("id = ?", id).Delete(&database.Good{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}