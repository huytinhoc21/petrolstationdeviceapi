package api

import (
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/util"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Get providers godoc
//
//	@Summary		Get a list of providers [OK]
//	@Description	Return a list of providers by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			Providers
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResProvider
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/providers [get]
func getProviders(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		rowData := database.Provider{}
		result := db.Table("providers")
		result = util.GetFilteredDB(result, c, rowData)

		var providers []model.ResProvider
		// Get all records
		deleted := c.Query("deleted")
		if deleted == "true" {
			result = result.Unscoped().Where("deleted_at is not null").Find(&providers)
		} else {
			result = result.Find(&providers)
		}

		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, providers)

	})
}

// Get a provider godoc
//
//	@Summary		Get a provider [OK]
//	@Description	Return a provider if successful
//	@Tags			Providers
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Provider id"
//	@Success		200	{object}	model.ResProvider
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/providers/{id} [get]
func getAProvider(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:id", func(c *gin.Context) {

		id := c.Param("id")
		var provider model.ResProvider
		// Get all records
		result := db.Table("providers").First(&provider, "id = ?", id)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, provider)

	})
}

// Post a provider godoc
//
//	@Summary		Post a provider to database [OK]
//	@Description	Return a id if successful
//	@Tags			Providers
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			Provider	body		model.ReqProvider	true	"New provider data"
//	@Success		200			{string}	string				"Inserted Unit ID"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/providers [post]
func postAProvider(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {
		req := model.ReqProvider{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		result := db.Table("providers").Create(&req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, req.ID)

	})
}

// Update a provider godoc
//
//	@Summary		Update a provider [OK]
//	@Description	Returns updated rows
//	@Tags			Providers
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id			path		string					true	"Provider id"
//	@Param			Provider	body		model.ReqPatchProvider	true	"Provider data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200			{integer}	uint					"The number of updated rows"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/providers/{id} [patch]
func updateAProvider(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqPatchProvider{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("providers").Where("id = ?", id).Updates(req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Delete a provider godoc
//
//	@Summary		Delete a provider [OK]
//	@Description	Returns deleted rows
//	@Tags			Providers
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Provider id"
//	@Success		200	{integer}	uint	"The number of deleted rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/providers/{id} [delete]
func deleteAProvider(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		result := db.Where("id = ?", id).Delete(&database.Provider{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}