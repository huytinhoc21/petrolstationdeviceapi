package api

import (
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/util"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Get shifts godoc
//
//	@Summary		Get a list of shifts [OK]
//	@Description	Return a list of shifts by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			Shifts
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResShift
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/shifts [get]
func getShifts(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {

		rowData := database.Shift{}
		result := db.Table("shifts")
		result = util.GetFilteredDB(result, c, rowData)

		var shifts []model.ResShift
		// Get all records
		deleted := c.Query("deleted")
		if deleted == "true" {
			result = result.Unscoped().Where("deleted_at is not null").Find(&shifts)
		} else {
			result = result.Find(&shifts)
		}

		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, shifts)

	})
}

// Get a shift godoc
//
//	@Summary		Get a shift [OK]
//	@Description	Return a shift if successful
//	@Tags			Shifts
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Shift id"
//	@Success		200	{object}	model.ResShift
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/shifts/{id} [get]
func getAShift(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:id", func(c *gin.Context) {

		id := c.Param("id")
		var shift model.ResShift
		// Get all records
		result := db.Table("shifts").First(&shift, "id = ?", id)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, shift)

	})
}

// Post a shift godoc
//
//	@Summary		Post a shift to database [OK]
//	@Description	Return a id if successful
//	@Tags			Shifts
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			Shift	body		model.ReqShift	true	"New shift data"
//	@Success		200		{string}	string			"Inserted Unit ID"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/shifts [post]
func postAShift(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {
		req := model.ReqShift{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		result := db.Table("shifts").Create(&req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, req.ID)

	})
}

// Update a shift godoc
//
//	@Summary		Update a shift [OK]
//	@Description	Returns updated rows
//	@Tags			Shifts
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id		path		string				true	"Shift id"
//	@Param			Shift	body		model.ReqPatchShift	true	"Shift data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200		{integer}	uint				"The number of updated rows"
//	@Failure		400		{object}	model.HTTPError400
//	@Router			/shifts/{id} [patch]
func updateAShift(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqPatchShift{}
		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("shifts").Where("id = ?", id).Updates(req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Delete a shift godoc
//
//	@Summary		Delete a shift [OK]
//	@Description	Returns deleted rows
//	@Tags			Shifts
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"Shift id"
//	@Success		200	{integer}	uint	"The number of deleted rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/shifts/{id} [delete]
func deleteAShift(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		result := db.Where("id = ?", id).Delete(&database.Shift{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}
