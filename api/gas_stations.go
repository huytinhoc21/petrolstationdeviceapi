package api

import (
	"petrol-station/database"
	"petrol-station/model"
	"petrol-station/util"

	"gorm.io/gorm"

	ginJWT "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
)

// Get gas_stations godoc
//
//	@Summary		Get a list of gas_stations [OK]
//	@Description	Return a list of gas_stations by giving a filter. A filter can be formed based on the data columns of a table. For example ?num_staff=10&name="Trạm cân 10"&deleted=true. You can also search for more values for a field, for example ?num_staff=10&num_staff=9.
//	@Tags			GasStations
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResGasStation
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/gas_stations [get]
func getGasStations(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("", func(c *gin.Context) {
		tmp, _ := c.Get("JWT_PAYLOAD")
		payload := tmp.(ginJWT.MapClaims)
		id := payload["ID"]

		rowData := database.GasStation{}
		result := db.Table("gas_stations")
		result = util.GetFilteredDB(result, c, rowData)

		var gas_stations []model.ResGasStation
		// Get all records
		deleted := c.Query("deleted")
		if deleted == "true" {
			result = result.Unscoped().
				Joins("join user_gas_stations on user_gas_stations.user_id = ? and user_gas_stations.gas_station_id = gas_stations.id", id).
				Where("deleted_at is not null").Find(&gas_stations)
		} else {
			result = result.
				Joins("join user_gas_stations on user_gas_stations.user_id = ? and user_gas_stations.gas_station_id = gas_stations.id", id).
				Find(&gas_stations)
		}
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, gas_stations)

	})
}

// Get a gas_station godoc
//
//	@Summary		Get a gas_station [OK]
//	@Description	Return a gas_station if successful
//	@Tags			GasStations
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"GasStation id"
//	@Success		200	{object}	model.ResGasStation
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/gas_stations/{id} [get]
func getAGasStation(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/:id", func(c *gin.Context) {
		tmp, _ := c.Get("JWT_PAYLOAD")
		payload := tmp.(ginJWT.MapClaims)
		userID := payload["ID"]

		id := c.Param("id")
		var gas_station model.ResGasStation
		// Get all records
		result := db.Table("gas_stations").
			Joins("join user_gas_stations on user_gas_stations.user_id = ? and user_gas_stations.gas_station_id = gas_stations.id", userID).
			First(&gas_station, "id = ?", id)
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, gas_station)

	})
}

// Post a gas_station godoc
//
//	@Summary		Post a gas_station to database [OK]
//	@Description	Return a id if successful
//	@Tags			GasStations
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			GasStation	body		model.ReqGasStation	true	"New gas_station data"
//	@Success		200			{string}	string				"Inserted Unit ID"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/gas_stations [post]
func postAGasStation(router *gin.RouterGroup, db *gorm.DB) {
	router.POST("", func(c *gin.Context) {
		req := model.ReqGasStation{}

		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}

		result := db.Table("gas_stations").Create(&req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, req.ID)

	})
}

// Update a gas_station godoc
//
//	@Summary		Update a gas_station [OK]
//	@Description	Returns updated rows
//	@Tags			GasStations
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id			path		string						true	"GasStation id"
//	@Param			GasStation	body		model.ReqPatchGasStation	true	"GasStation data to update. We can update all fields and omit fields that don't need to be updated, the API only updates those fields present in the object."
//	@Success		200			{integer}	uint						"The number of updated rows"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/gas_stations/{id} [patch]
func updateAGasStation(router *gin.RouterGroup, db *gorm.DB) {
	router.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")

		req := model.ReqPatchGasStation{}
		c.Bind(&req)

		if err := CheckInputError(req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Table("gas_stations").Where("id = ?", id).Updates(req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Delete a gas_station godoc
//
//	@Summary		Delete a gas_station [OK]
//	@Description	Returns deleted rows
//	@Tags			GasStations
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			id	path		string	true	"GasStation id"
//	@Success		200	{integer}	uint	"The number of deleted rows"
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/gas_stations/{id} [delete]
func deleteAGasStation(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		result := db.Where("id = ?", id).Delete(&database.GasStation{})
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}

// Get gas_station_general_information godoc
//
//	@Summary		Get general information about gas stations  [OK]
//	@Description	Return a json data.
//	@Tags			GasStations
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}		model.ResGasStationInformation
//	@Failure		400	{object}	model.HTTPError400
//	@Router			/gas_stations/general-info [get]
func getGasStationInformation(router *gin.RouterGroup, db *gorm.DB) {
	router.GET("/general-info", func(c *gin.Context) {

		rowData := database.GasStation{}
		result := db.Table("gas_stations")
		result = util.GetFilteredDB(result, c, rowData)

		var gas_stations []model.ResGasStation
		// Get all records
		deleted := c.Query("deleted")
		if deleted == "true" {
			result = result.Unscoped().Where("deleted_at is not null").Find(&gas_stations)
		} else {
			result = result.Find(&gas_stations)
		}
		// SELECT * FROM users;

		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}

		c.JSON(200, gas_stations)

	})
}

// Delete gas stations godoc
//
//	@Summary		Delete gas stations [OK]
//	@Description	Returns deleted rows
//	@Tags			GasStations
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Param			GasStation	body		[]model.ReqDeleteMany	true	"List of IDs to be deleted"
//	@Success		200			{integer}	uint					"The number of deleted rows"
//	@Failure		400			{object}	model.HTTPError400
//	@Router			/gas_stations [delete]
func deleteGasStations(router *gin.RouterGroup, db *gorm.DB) {
	router.DELETE("", func(c *gin.Context) {

		req := []database.GasStation{}
		if err := c.Bind(&req); err != nil {
			returnError(c, 400, err.Error())
			return
		}
		result := db.Delete(&req)
		if result.Error != nil {
			returnError(c, 400, result.Error.Error())
			return
		}
		c.JSON(200, result.RowsAffected)
	})
}
